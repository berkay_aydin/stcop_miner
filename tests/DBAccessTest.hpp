/*
* DBAccessTest.hpp
*
*  Created on: June 7, 2014
* Author: Dustin Kempton
*/

#ifndef DBACCESSTEST_HPP_
#define DBACCESSTEST_HPP_

#include "db/DBAccess.hpp"
#include "db/ChebyshevIdxFactory.hpp"
#include "db/NoIdxFactory.hpp"
#include "db/SETIIdxFactory.hpp"
#include "Instance.h"


class DBAccessTest {
	IDBAccess* dbAcc;
	IIndexFactory* factory;
public:
	DBAccessTest() {
		string fileLoc = "./";

		//this->factory = new ChebyshevIdxFactory(fileLoc, 5, 1, 24, 0.25);
		//this->factory = new NoIdxFactory(fileLoc,5);
		this->factory = new SETIIdxFactory(fileLoc,5);
		this->dbAcc = new DBAccess(this->factory);


	}

	~DBAccessTest() {
		delete dbAcc;
		delete factory;
	}

	/************************************************************************/
	/* testing join on instance tables*/
	/************************************************************************/
	void testInstJoin() {

		InputReader *ir = new InputReader("../featuredata", this->dbAcc);

		vector<Feature> vf = ir->readFeatures();
		ir->readInstances(vf);

		JoinTableResults<Instance, IInstIndex>* results = this->dbAcc->spatiotemporalJoin("1", "2");
		//        std::cout << results->

		//        string fileName = "file"; 
		//        string fileName2 = "file2";
		//
		//
		//        string s0("POLYGON((49644.48 62093.48, 49540.69 62066.97, 49470.13 62060.86, 49494.99 61937.82, 49606.25 61910.4, 49649.17 61960.44, 49644.48 62093.48))");
		//        string s1("POLYGON((49670.61 62116.12, 49559.25 62087.67, 49483.55 62081.11, 49510.22 61949.1, 49629.58 61919.68, 49675.64 61973.37, 49670.61 62116.12))");
		//        string s2("POLYGON((49706.93 62146.17, 49587.45 62115.65, 49506.23 62108.62, 49534.85 61966.98, 49662.91 61935.41, 49712.32 61993.02, 49706.93 62146.17))");
		//        //string s3("POLYGON((49753.47 62183.69, 49625.28 62150.95, 49538.14 62143.4, 49568.85 61991.44, 49706.25 61957.57, 49759.26 62019.37, 49753.47 62183.69))");
		//        //string s4("POLYGON((49810.27 62228.73, 49672.73 62193.59, 49579.24 62185.5, 49612.18 62022.46, 49759.6 61986.12, 49816.47 62052.42, 49810.27 62228.73))");
		//        //string s5("POLYGON((49877.34 62281.32, 49729.77 62243.62, 49629.47 62234.94, 49664.81 62060.01, 49822.97 62021.02, 49884.0 62092.16, 49877.34 62281.32))");
		//
		//        list<string> wkts;
		//        wkts.push_back(s0);
		//        wkts.push_back(s1);
		//        wkts.push_back(s2);
		//
		//        int num = 5;
		//        for (int i = 0; i < num; i++) {
		//            Instance *inst1 = new Instance(i, 0, 0, 2, wkts);
		//            this->dbAcc->insert(inst1, fileName);
		//        }
		//
		//        for (int i = 0; i < num; i++) {
		//            Instance *inst1 = new Instance(i, 0, 0, 2, wkts);
		//            this->dbAcc->insert(inst1, fileName2);
		//        }
		//
		//  JoinTableResults<Instance, IInstIndex>* results = this->dbAcc->spatiotemporalJoin(fileName, fileName2);

		int count = 0;
		for (JoinTableResults<Instance, IInstIndex>::Iterator iter = results->
			begin();
			iter != results->end();
		iter++) {
			count++;
			pair<Instance*, Instance*> resultPair = iter.operator *();
			//            delete resultPair.first;
			//            delete resultPair.second;
		}

		std::cout << "Spatio Temporal Join Count: " << count << std::endl;
		//
		//        assert(count == (num*num - num));
	}


	/************************************************************************/
	/* Testing join on pattern instance tables*/
	/************************************************************************/
	void testPatInstJoin() {
		string fileName = "file3";
		string fileName2 = "file4";

		string s0("POLYGON((49644.48 62093.48, 49540.69 62066.97, 49470.13 62060.86, 49494.99 61937.82, 49606.25 61910.4, 49649.17 61960.44, 49644.48 62093.48))");
		string s1("POLYGON((49670.61 62116.12, 49559.25 62087.67, 49483.55 62081.11, 49510.22 61949.1, 49629.58 61919.68, 49675.64 61973.37, 49670.61 62116.12))");
		string s2("POLYGON((49706.93 62146.17, 49587.45 62115.65, 49506.23 62108.62, 49534.85 61966.98, 49662.91 61935.41, 49712.32 61993.02, 49706.93 62146.17))");
		//string s3("POLYGON((49753.47 62183.69, 49625.28 62150.95, 49538.14 62143.4, 49568.85 61991.44, 49706.25 61957.57, 49759.26 62019.37, 49753.47 62183.69))");
		//string s4("POLYGON((49810.27 62228.73, 49672.73 62193.59, 49579.24 62185.5, 49612.18 62022.46, 49759.6 61986.12, 49816.47 62052.42, 49810.27 62228.73))");
		//string s5("POLYGON((49877.34 62281.32, 49729.77 62243.62, 49629.47 62234.94, 49664.81 62060.01, 49822.97 62021.02, 49884.0 62092.16, 49877.34 62281.32))");

		list<string> wkts;
		wkts.push_back(s0);
		wkts.push_back(s1);
		wkts.push_back(s2);

		int num = 5;
		for (int i = 0; i < num; i++) {
			Instance *inst1 = new Instance(i, 1, 0, 2, wkts);
			Instance *inst2 = new Instance(i + num, 2, 0, 2, wkts);
			PatternInstance *pat_inst = new PatternInstance(inst1, inst2);
			this->dbAcc->insert(pat_inst, fileName);
		}

		for (int i = 0; i < num; i++) {
			Instance *inst1 = new Instance(i, 1, 0, 2, wkts);
			Instance *inst2 = new Instance(i + num, 2, 0, 2, wkts);
			PatternInstance *pat_inst = new PatternInstance(inst1, inst2);
			this->dbAcc->insert(pat_inst, fileName2);
		}

		JoinTableResults<PatternInstance, IPatInstIndex>* results = this->dbAcc->p_spatiotemporalJoin(fileName, fileName2);

		int count = 0;
		for (JoinTableResults<PatternInstance, IPatInstIndex>::Iterator iter = results->begin(); iter != results->end(); iter++) {
			count++;
			pair<PatternInstance*, PatternInstance*> resultPair = iter.operator *();
			delete resultPair.first;
			delete resultPair.second;
		}

		assert(count == (num*num - num));

	}
};

#endif
