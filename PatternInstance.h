/*
 * PatternInstance.h
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#ifndef PATTERNINSTANCE_H_
#define PATTERNINSTANCE_H_


#include "Instance.h"
#include <list>
#include <vector>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

#include <boost/geometry/io/wkt/wkt.hpp>
#include <boost/foreach.hpp>

//typedef boost::geometry::model::point<int, 2, boost::geometry::cs::cartesian> point;
//typedef boost::geometry::model::polygon<point, false, false > Polygon;
typedef boost::geometry::model::multi_polygon<Polygon > Multipolygon;

using namespace std;

class PatternInstance {

public:

    PatternInstance(Instance* i1, Instance* i2);	//constructor 1
    PatternInstance(PatternInstance* patI1, PatternInstance* patI2);	//constructor 2
    PatternInstance(vector<vector<Polygon> > intersectionPolys, vector<vector<Polygon> >unionPolys,
                    int patID, long startTime, long endTime, double intersectVol, double unionVol, double maxVol); //constructor 3 used when reading back from a file.
    ~PatternInstance(){
    	//delete unionGeometries;
    	//delete intersectionGeometries;
    	//delete participatingInstances;
    };

    list<int> participatingInstances;			//instance ids of participating instances
    vector< vector <Polygon> > unionGeometries;			//union geometries through all timestamps
    vector<vector <Polygon> > intersectionGeometries;	//intersection geometries through all timestamps

    void calculateGeometries(Instance* i1, Instance* i2);	//calculate geometries and scalar volume values
    void calculateGeometries(PatternInstance* patI1, PatternInstance* patI2);	//calculate geometries and scalar volume values

    int getPatternInstanceId();			//get pattern instance id
    double getIntersectionVolume();		//get the intersection volume
    double getUnionVolume();			//get the union volume
    double getMaxVolume();				//get maximum volume
    long getStartTime();				//get start time of pattern instance
    long getEndTime();					//get end time of pattern instance

    void printPatternInstance();						//prints the pattern instance


private:
    int pi_id;					//pattern instance identifier
    long startTime;				//pattern instance's start time
    long endTime;				//pattern instance's end time
    double intersectionVolume;	//scalar intersection volume for the pattern instance
    double unionVolume;			//scalar union volume for the pattern instance
    double maxVolume;			//maximum volume for OMAX measure
};



#endif /* PATTERNINSTANCE_H_ */
