/*
 * IFileAccess.hpp
 *
 *  Created on: May 20, 2014
 * Author: Dustin
 */

#ifndef MOCKFILEACCESS_CPP
#define MOCKFILEACCESS_CPP

#include <vector>
//#include "IInstFileAccess.hpp"
#include "Instance.h"
#include "PatternInstance.h"

using namespace std;

/**
 * Important naming convention:::
 *
 * if file name that we are going to be reading starts with an underscore('_')
 * than we will assume that it is a PATTERN INSTANCE
 *
 * if not then it is a direct instance
 *
 */

class MockFileAccess  {
private:
    vector<Instance*>* instanceVect;
    vector<PatternInstance*>* patternInstanceVect;
    long sequentialReads = 0;
    long randomReads = 0;

public:

    MockFileAccess() {
        instanceVect = new vector<Instance*>();
        patternInstanceVect = new vector<PatternInstance*>();
    }

    ~MockFileAccess() {
        delete instanceVect;
        delete patternInstanceVect;
    }

    long writeInstanceToFile(Instance* instance) {
        this->instanceVect->push_back(instance);
        return (this->instanceVect->size() - 1);
    }

    long writePatternInstanceToFile(PatternInstance* patternInstance) {
        this->patternInstanceVect->push_back(patternInstance);
        return (this->patternInstanceVect->size() - 1);
    }

    Instance* getInstanceFromFile(long loc) {
        Instance* inst = this->instanceVect->at(loc);
        randomReads++;
        sequentialReads++;
        //sequentialReads += I need the size of the polygon vector for this
        //but when we read from file this would be available.
        //so it really isn't needed in the production system
        return inst;
    }
    PatternInstance* getPatternInstanceFromFile(long loc) {
        PatternInstance* patInst = this->patternInstanceVect->at(loc);
        randomReads++;
        sequentialReads++;
        //sequentialReads += I need the size of the polygon vector for this
        //but when we read from file this would be available.
        //so it really isn't needed in the production system
        return patInst;
    }

    virtual int getNumSequentialReads() {
        return sequentialReads;
    }

    virtual int getNumRamdomReads() {
        return randomReads;
    }

    list<Instance*> searchST_Intersection(Instance *query, string table) {

        //TODO simply implement the following SQL statement
        /**
         * SELECT *
         * FROM table
         * WHERE ST_Intersects(query.getPolygon(time_i), polygon(time_i) )
         * 			AND query.getTimestamp() = time
         */

        //FOR NESTED LOOP JOIN VERSION

        // this means,
        //	for each timestamp that query instance covers
        //		T_intersection_list <- search temporally intersecting instances
        // 		for each instance I in T_intersection_list
        //			if I.getPolygon(i) intersects with query.getPolygon(i)
        //				then add the instance to result, and remove it from T_intersection_list
        //			else
        //				continue
        //	return all results

        Instance *i;
        list<Instance *> result;
        result.push_back(i);
        return result;
    }

    list<PatternInstance*> searchST_Intersection(PatternInstance *query, string table) {

        //TODO same thing but this time you have both intersection and union geometries
        //TODO simply implement the following SQL statement
        /**
         * SELECT *
         * FROM table
         * WHERE ST_Intersects(query.getPolygon(time_i), polygon(time_i) )
         * 			AND query.getTimestamp() = time
         */

        //FOR NESTED LOOP JOIN VERSION

        // this means,
        //	for each timestamp that query instance covers
        //		T_intersection_list <- search temporally intersecting instances
        // 		for each instance I in T_intersection_list
        //			if I.getPolygon(i) intersects with query.getPolygon(i)
        //				then add the instance to result, and remove it from T_intersection_list
        //			else
        //				continue
        //	return all results

        PatternInstance *i;
        list<PatternInstance *> result;
        result.push_back(i);
        return result;
    }


    vector<pair<Instance*, Instance*> > spatiotemporalJoin(string table1, string table2) {

        vector<pair<Instance*, Instance*> > joinResult;

        /* this is the part I am not sure how to do
         *
         * I am still thinking on the accessing of the data files
         * I think I didn't quite get the file accessing with a long integer
         * but the algorithm is as follows
        */
        int N_1 = 10; //size of table1
        //int N_2 = 10; //size of table2

        bool is_t1_patternInstance = table1.at(0) == '_';
        bool is_t2_patternInstance = table2.at(0) == '_';

        if(is_t1_patternInstance || is_t2_patternInstance) {
            cout << "You are doing something completely wrong, tables should be instance tables" << endl;
        } else {
            //now we have both instance tables
            pair<Instance*, Instance*> tempPair;

            for(int i = 0; i < N_1; i++) {
                Instance *i1 = getInstanceFromFile( long(i) ); // TODO this is a little tricky
                // DON'T GET HOW TO DO THAT IN FILE TOO
                list<Instance*> searchResult = searchST_Intersection(i1, table2);

                for(list<Instance*>::iterator it = searchResult.begin(); it != searchResult.end(); it++) {
                    // *it is the Instance*
                    tempPair = make_pair(i1, *it);
                    joinResult.push_back(tempPair);
                }
            }
        }
        return joinResult;
    }

    vector<pair<PatternInstance*, PatternInstance*> > p_spatiotemporalJoin(string table1, string table2) {

        vector<pair<PatternInstance*, PatternInstance*> > joinResult;

        /* this is the part I am not sure how to do
         *
         * I am still thinking on the accessing of the data files
         * I think I didn't quite get the file accessing with a long integer
         * but the algorithm is as follows
        */
        int N_1 = 10; //size of table1
        //int N_2 = 10; //size of table2

        bool is_t1_patternInstance = table1.at(0) == '_';
        bool is_t2_patternInstance = table2.at(0) == '_';

        if( !(is_t1_patternInstance && is_t2_patternInstance)) {
            cout << "You are doing something completely wrong, tables should be instance tables" << endl;
        } else {
            //now we have both instance tables
            pair<PatternInstance*, PatternInstance*> tempPair;

            for(int i = 0; i < N_1; i++) {
                PatternInstance *i1 = getPatternInstanceFromFile( long(i) ); // TODO this is a little tricky
                // DON'T GET HOW TO DO THAT IN FILE TOO
                list<PatternInstance*> searchResult = searchST_Intersection(i1, table2);

                for(list<PatternInstance*>::iterator it = searchResult.begin(); it != searchResult.end(); it++) {
                    // *it is the Instance*
                    tempPair = make_pair(i1, *it);
                    joinResult.push_back(tempPair);
                }
            }
        }
        return joinResult;
    }


};

#endif
