/*
 * Miner.cpp
 *
 *  Created on: May 27, 2014
 *      Author: berkay
 */

#include "Miner.h"
#include <algorithm>
#include "db/interface/IDBAccess.hpp"



Miner::Miner( double cce_th, double pi_th, IDBAccess* idb_access, Reader* file_reader) {
	insertElapsed = 0.0;
	joinElapsed = 0.0;

	cce_threshold = cce_th;
	pi_threshold = pi_th;

	IDB_accessor = idb_access;

	ir = file_reader;

	//initialize the instance files, read the instances
	int k = 1;
	generateCandidatePatterns(k);
	cout << "...Size 1 candidates generated" << endl;


	while ((unsigned int)k < allFeatures.size()) {
		cout << "Started generating size" << (k + 1) << " candidates ..." << endl;
		generateCandidatePatterns(k + 1);
		cout << "Generating candidate pattern instances for size: " << k + 1 << " candidate patterns..." << endl;
		generateCandidatePatternInstances(k + 1);

		filterCandidataPatternInstances_OMAX();
		refineCandidatePatterns_OMAX();

		filterCandidataPatternInstances_Jaccard();
		refineCandidatePatterns_Jaccard();

		k++;

		cout << "...Size " << k << " candidates, refined" << endl;
		if (allPatterns.at(k - 2).size() == 0) {
			cout << "But they are empty, this is the end my friend, TERMINATING" << endl;
			break;
		}
		else {
			for (unsigned int i = 0; i < allPatterns.at(k - 2).size(); i++) {
				cout << "\t" << allPatterns.at(k - 2).at(i).getPatternId();
			}
			cout << endl;
		}
	}


}

void Miner::generateCandidatePatterns(int cardinality) {

	if (cardinality < 1) {
		cout << "Cardinality cannot be less than 1" << endl;
	}
	else if (cardinality == 1) {
		exploreFeatures();
	}
	else if (cardinality == 2) {
		vector<Pattern> size2_candidates;
		for (unsigned int i = 0; i < allFeatures.size(); i++) {
			for (unsigned int j = i + 1; j < allFeatures.size(); j++) {
				Pattern p(2);
				p.addFeature(allFeatures.at(i).featureId);
				p.addFeature(allFeatures.at(j).featureId);
				size2_candidates.push_back(p);
				//p.printPattern();
			}
		}
		allPatterns.push_back(size2_candidates);
		cout << "After apriori number of size-2 candidate patterns generated: " << size2_candidates.size() << endl;
	}
	else {
		vector<Pattern> sizek_candidates;
		vector<Pattern> earlyCandidates = allPatterns.at(cardinality - 3); //we access (cardinality-3)_th index, as the 0th position holds
		//size-2 candidate patterns, and for size-k patterns, we get size-k-1 candidates.

		for (unsigned int i = 0; i < earlyCandidates.size(); i++) {

			for (unsigned int j = i + 1; j < earlyCandidates.size(); j++) {

				//if first n-2 features of (size_k-1) earlyCandidates[i] and earlyCandidates[j] are same
				//then we have a possible size-k candidate pattern

				vector<int> f1 = earlyCandidates.at(i).getFeatureIds();
				vector<int> f2 = earlyCandidates.at(j).getFeatureIds();

				bool possibleCandidate = true;
				for (unsigned int t = 0; t < f1.size() - 1; t++) {
					//index is at t
					if (f1.at(t) != f2.at(t)) {
						possibleCandidate = false;
						break;
					}
				} //check if first n-2 features are same, if yes it is a possible candidate pattern

				Pattern possiblePattern(cardinality); //create the pattern objects
				if (possibleCandidate) {
					for (unsigned int t = 0; t < f1.size(); t++) {
						//index is at t
						possiblePattern.addFeature(f1.at(t));
					}
					possiblePattern.addFeature(f2.at(f2.size() - 1)); //fill the features of two earlier patterns

					/*cout << "For possible pattern: " ;
					//possiblePattern.printPattern();
					cout << "\tpattern ids to be checked:::\n"; */

					//now we need to check if other subsets are also present
					for (int u = 0; u < cardinality - 2; u++) { //each iteration we generate a different subset,
						//and see if it is in earlier patterns
						vector<int> fids = possiblePattern.getFeatureIds();
						fids.erase(fids.begin() + u); //create a subset by removing one feature

						string pid = "";
						for (vector<int>::iterator it = fids.begin(); it != fids.end(); it++) {
							pid.append("_");
							pid.append(boost::lexical_cast<std::string>(*it));
						} //create a fake pattern id

						bool subsetPatternExists = false;
						for (unsigned int q = j + 1; q < earlyCandidates.size(); q++) { //check if it exists in earlier candidates
							if (earlyCandidates.at(q).getPatternId() == pid) { //stop if it exists
								subsetPatternExists = true;
								//cout << pid << " is found " <<  "\n";
								break;
							}
						} //end of checking earlier candidates

						if (!subsetPatternExists) { // if subset pattern does not exist, then it is not a possible candidate
							// break the loop here
							possibleCandidate = false;
							//cout << pid << " is not there, then "<< possiblePattern.getPatternId() << " not possible candidate" << "\n";
							break;
						}
					}
					//cout << endl;
				}

				if (possibleCandidate) {	//if every subset is found, we do not update possibleCandidate as false,
					//insert it to sizek_candidates vector
					sizek_candidates.push_back(possiblePattern);
				}
			}
		}
		allPatterns.push_back(sizek_candidates);
	}

}

void Miner::generateCandidatePatternInstances(int cardinality) {

	//for each "candidate pattern" CPat generated using generateCandidatePatterns()
	//		explore the possible instances of CPat using (if cardinality=2) instance or (cardinality>2)pattern instance file
	//		filter and prune with OMAX
	//		filter and prune with Jaccard
	//		report them back and write them

	//TODO delete stopper for debugging
	int stopper = 0;
	vector<Pattern> sizek_candidates = allPatterns.at(cardinality - 2);
	cout << "Patterns to explore:\n";
	for (unsigned int i = 0; i < sizek_candidates.size(); i++){
		sizek_candidates.at(i).printPattern();
	}

	vector<PatternInstance> patternInstaces;

	if (cardinality == 2) {//then we will use instance files

		for (unsigned int i = 0; i < sizek_candidates.size(); i++) {

			Pattern candPat = sizek_candidates.at(i); //get the candidate pattern
			vector<int> candPat_fids = candPat.getFeatureIds(); //get the candidate pattern's features
			if (candPat_fids.size() != 2) {
				cout << "Cardinality is 2, feature ids must only have two elements" << endl;
			}
			else {
				int fid1 = candPat_fids.at(0);
				int fid2 = candPat_fids.at(1); // fid1 and fid2 are the tables to search for ST_Join

				//keep track of join time
				cpu_timer timer;
				cpu_times startTime(timer.elapsed());

				JoinTableResults<Instance, IInstIndex>* joinQueryResult = IDB_accessor->spatiotemporalJoin(to_string(fid1), to_string(fid2));

				cpu_times endTime(timer.elapsed());
				nanosecond_type elapsedAtStart(startTime.system + startTime.user);
				nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
				joinElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);

				vector <PatternInstance*> candTableInst; //temporary list of pattern instances ( table instance)

				cout << "Checking instances from tables: " << fid1 << " and " << fid2 << endl;
				cout << "\tApplying OMAX filter " << endl;
				for (JoinTableResults<Instance, IInstIndex>::Iterator iter = joinQueryResult->begin();
					iter != joinQueryResult->end();) { //in this loop create the pattern instance objects

					//then filter with OMAX upon creation
					pair<Instance*, Instance*> valuePair = iter.operator *();
					Instance* inst1Ptr = valuePair.first;
					Instance* inst2Ptr = valuePair.second;
					cout << "\t\tInstances:::::" << inst1Ptr->getInstanceId() << " and " << inst2Ptr->getInstanceId();

					PatternInstance *cpInst = new PatternInstance(inst1Ptr, inst2Ptr); //create the pattern instance object
					//cpInst->printPatternInstance();
					//here is OMAX filter
					double omax = (cpInst->getIntersectionVolume() / cpInst->getMaxVolume());
					cout << " with cce OMAX: " << omax;
					if (cce_threshold <= omax) { // then it stays
						cout << " -> PASSED" << endl;
						candTableInst.push_back(cpInst);
					}
					else {

						//delete the instances as we will not be using them any longer
						delete cpInst;
						cout << endl;
						//candidate pattern instance is removed // this is for debug purposes
					}

					//keep track of join time
					cpu_timer timer;
					cpu_times startTime(timer.elapsed());
					iter++;
					cpu_times endTime(timer.elapsed());
					nanosecond_type elapsedAtStart(startTime.system + startTime.user);
					nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
					joinElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);

				} //end of pat instance creation and OMAX filtering
				cout << "\tEnd of OMAX filter " << endl;
				delete joinQueryResult;

				cout << "\tStart OMAX refinement" << endl;
				//calculate participation ratio
				int min = -1;
				for (unsigned int i = 0; i < allFeatures.size(); i++){
					if (fid1 == allFeatures.at(i).featureId){
						min = featureCounts.at(i);
						break;
					}
				}
				for (unsigned int i = 0; i < allFeatures.size(); i++){
					if (min == -1){
						cout << "min cannot be -1" << endl;
					}
					if (fid2 == allFeatures.at(i).featureId){
						if (min > featureCounts.at(i)){
							min = featureCounts.at(i);
						}
						break;
					}
				}

				cout << candTableInst.size() << endl;
				double pr_OMAX = (double)candTableInst.size() / (double)min;
				cout << "\t>My pr after OMAX is " << pr_OMAX << endl;
				cout << "\tEnd of OMAX refinement" << endl;
				if (pi_threshold <= pr_OMAX) { // then this pattern is ready for Jaccard measure
					cout << "\tThis pattern is eligible after OMAX filter" << endl;
					cout << "\tApplying Jaccard filter " << endl;
					for (unsigned int i = 0; i < candTableInst.size(); i++){
						double jaccard = candTableInst.at(i)->getIntersectionVolume() / candTableInst.at(i)->getUnionVolume();
						cout << "\t\tPattern Instance:::::" << candTableInst.at(i)->getPatternInstanceId() << "with cce J:" << jaccard;
						if (cce_threshold <= jaccard) {// then it stays
							cout << "->PASSED" << endl;
						}
						else {//then we remove it from candidate table instance

							PatternInstance* tmp = candTableInst.at(i);
							candTableInst.erase(candTableInst.begin() + i);
							delete tmp;
							cout << " -> NOT PASSED" << endl;
							continue;
						}
					}
					cout << "\tEnd of Jaccard filter " << endl;

					//here is the place we go for jaccard measure
					double pr_Jaccard = (double)candTableInst.size() / (double)min;
					cout << "\tStart Jaccard refinement" << endl;
					if (pi_threshold <= pr_Jaccard) { // then candidate pattern stays, and we need to write pattern instances back
						cout << "\tThis pattern passed Jaccard refinement, adding the instances to index" << endl;
						string patternTableName = candPat.getPatternId();
						cout << "\tNumber of instances to write to index: " << candTableInst.size() << endl;
						for (unsigned int i = 0; i < candTableInst.size(); i++){
							//cout << "Writing pattern instance to disk" << endl;
							PatternInstance* ptr_pInst = candTableInst.at(i);
							//ptr_pInst->printPatternInstance();
							//cout << "size of uni geom before inserting"<< ptr_pInst->unionGeometries.size() << endl;
							cpu_timer timer;
							cpu_times startTime(timer.elapsed());
							IDB_accessor->insert(ptr_pInst, patternTableName);
							cpu_times endTime(timer.elapsed());
							nanosecond_type elapsedAtStart(startTime.system + startTime.user);
							nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
							insertElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);
						}
					}
					else { //remove the pattern from allPatterns list
						cout << "\tThis pattern couldn't pass Jaccard refinement, deleting it from patterns" << endl;
						allPatterns.at(cardinality - 2).erase(allPatterns.at(cardinality - 2).begin() + i);
						sizek_candidates.erase(sizek_candidates.begin() + i);

						for (unsigned int i = 0; i < candTableInst.size(); i++){
							//clean up pointers
							PatternInstance* tmp = candTableInst.at(i);
							delete tmp;
						}
						i--;
					}

				}
				else { //remove the pattern from allPatterns list
					cout << "\tThis pattern couldn't pass OMAX refinement, deleting it from patterns" << endl;
					allPatterns.at(cardinality - 2).erase(allPatterns.at(cardinality - 2).begin() + i);
					sizek_candidates.erase(sizek_candidates.begin() + i);

					for (unsigned int i = 0; i < candTableInst.size(); i++){
						//clean up pointers
						PatternInstance* tmp = candTableInst.at(i);
						delete tmp;
					}

					i--;
				}

				//cin >> stopper;
			}
		}
		//cin >> stopper;
	}
	else if (cardinality > 2) { //then pattern instance files will be used

		for (unsigned int i = 0; i < sizek_candidates.size(); i++) {

			Pattern candPat = sizek_candidates.at(i); //get the candidate pattern
			vector<int> candPat_fids = candPat.getFeatureIds(); //get the candidate pattern's features
			if (candPat_fids.size() != cardinality) {
				cout << "Cardinality is " << cardinality << ", feature ids must only have " << cardinality << " elements" << endl;
			}
			else {

				//TODO this can be done more efficiently but this is for now
				string patId1 = "";
				string patId2 = "";
				for (unsigned int pInd = 0; pInd < candPat_fids.size() - 1; pInd++){
					patId1.append("_");
					patId1.append(to_string(candPat_fids.at(pInd)));
				}
				for (unsigned int pInd = 1; pInd < candPat_fids.size(); pInd++){
					patId2.append("_");
					patId2.append(to_string(candPat_fids.at(pInd)));
				}

				cpu_timer timer;
				cpu_times startTime(timer.elapsed());

				JoinTableResults<PatternInstance, IPatInstIndex>* joinQueryResult = IDB_accessor->p_spatiotemporalJoin(patId1, patId2);

				cpu_times endTime(timer.elapsed());
				nanosecond_type elapsedAtStart(startTime.system + startTime.user);
				nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
				joinElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);

				vector <PatternInstance* > candTableInst; //temporary list of pattern instances ( table instance)
				/*				if( candPat.getPatternId() == "_1_3_5" ){
									cout << "what is going on" << endl;
									}*/
				cout << "Checking pattern instances from tables: " << patId1 << " and " << patId2 << endl;
				cout << "\tApplying OMAX filter " << endl;
				for (JoinTableResults<PatternInstance, IPatInstIndex>::Iterator iter = joinQueryResult->begin();
					iter != joinQueryResult->end();) { //in this loop create the pattern instance objects

					//cout << "are we coming here at all??" << endl;
					//TODO HERE IS THE PROBLEM!!!!!!!!


					//then filter with OMAX upon creation
					pair<PatternInstance*, PatternInstance*> valuePair = iter.operator *();
					//TODO valuePair returns nothing!
					//cout << "what about here??" << endl;

					PatternInstance* inst1Ptr = valuePair.first;
					PatternInstance* inst2Ptr = valuePair.second;
					bool isInstanceNull = inst1Ptr == NULL || inst2Ptr == NULL;
					if (!isInstanceNull && inst1Ptr->getPatternInstanceId() != inst2Ptr->getPatternInstanceId()){
						cout << "\t\tPattern Instances:::::" << inst1Ptr->getPatternInstanceId() << " and " << inst2Ptr->getPatternInstanceId();

						//inst1->printPatternInstance();
						//inst2->printPatternInstance();

						PatternInstance *cpInst = new PatternInstance(inst1Ptr, inst2Ptr); //create the pattern instance object

						if (cpInst->getPatternInstanceId() != -1){ //check if it is valid or not
							//cout << "\t\t\t\tPAtInst + PATINST intersection result!!!!!!" << endl;
							//cpInst->printPatternInstance();
							//cout << "\t\t\t\tThis is the END OF the pattern instance created fellas!!!!!!" << endl;

							//here is OMAX filter
							double omax = (cpInst->getIntersectionVolume() / cpInst->getMaxVolume());
							cout << " with cce OMAX: " << omax;
							if (cce_threshold <= omax) { // then it stays
								cout << " -> PASSED" << endl;
								candTableInst.push_back(cpInst);
							}
							else {
								cout << " -> NOT PASSED" << endl;
								delete cpInst;
								//candidate pattern instance is removed // this is for debug purposes
							}
						}
						else{
							cout << " ->> INVALID PatInstance" << endl;
							delete cpInst;
						}

					}
					else{
						delete inst1Ptr;
						delete inst2Ptr;
					}

					cpu_timer timer;
					cpu_times startTime(timer.elapsed());
					iter++;
					cpu_times endTime(timer.elapsed());
					nanosecond_type elapsedAtStart(startTime.system + startTime.user);
					nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
					joinElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);

				} //end of pat instance creation and OMAX filtering
				cout << "\tEnd of OMAX filter " << endl;
				delete joinQueryResult;


				cout << "\tStart OMAX refinement" << endl;
				//calculate participation ratio

				int min = -1;
				for (unsigned int ii = 0; ii < allFeatures.size(); ii++){
					if (candPat_fids.at(0) == allFeatures.at(ii).featureId){
						min = featureCounts.at(ii);
						break;
					}
				}
				for (unsigned int i = 1; i < candPat_fids.size(); i++){
					for (unsigned int j = 0; j < allFeatures.size(); j++){
						if (candPat_fids.at(i) == allFeatures.at(j).featureId){
							if (min > featureCounts.at(j)){
								min = featureCounts.at(j);
							}
							break;
						}
					}
				}

				cout << candTableInst.size() << endl;
				double pr_OMAX = (double)candTableInst.size() / (double)min;
				cout << "\t>My pr after OMAX is " << pr_OMAX << endl;
				cout << "\tEnd of OMAX refinement" << endl;
				if (pi_threshold <= pr_OMAX) { // then this pattern is ready for Jaccard measure
					cout << "\tThis pattern is eligible after OMAX filter" << endl;
					cout << "\tApplying Jaccard filter " << endl;
					for (unsigned int i = 0; i < candTableInst.size(); i++){
						double jaccard = candTableInst.at(i)->getIntersectionVolume() / candTableInst.at(i)->getUnionVolume();
						cout << "\t\tPattern Instance:::::" << candTableInst.at(i)->getPatternInstanceId() << "with cce J:" << jaccard;
						if (cce_threshold <= jaccard) {// then it stays
							cout << "->PASSED" << endl;
						}
						else {//then we remove it from candidate table instance
							PatternInstance* tmp = candTableInst.at(i);
							delete tmp;
							candTableInst.erase(candTableInst.begin() + i);
							cout << endl;
							continue;
						}
					}
					cout << "\tEnd of Jaccard filter " << endl;

					//here is the place we go for jaccard measure
					double pr_Jaccard = (double)candTableInst.size() / (double)min;
					cout << "\tStart Jaccard refinement" << endl;
					if (pi_threshold <= pr_Jaccard) { // then candidate pattern stays, and we need to write pattern instances back
						cout << "\tThis pattern passed Jaccard refinement, adding the instances to index" << endl;
						string patternTableName = candPat.getPatternId();
						cout << "\tNumber of instances to write to index: " << candTableInst.size() << endl;
						for (unsigned int i = 0; i < candTableInst.size(); i++){
							//cout << "Writing pattern instance to disk" << endl;
							PatternInstance* ptr_pInst = candTableInst.at(i);
							//ptr_pInst->printPatternInstance();
							//cout << "size of uni geom before inserting"<< ptr_pInst->unionGeometries.size() << endl;
							cpu_timer timer;
							cpu_times startTime(timer.elapsed());
							IDB_accessor->insert(ptr_pInst, patternTableName);
							cpu_times endTime(timer.elapsed());
							nanosecond_type elapsedAtStart(startTime.system + startTime.user);
							nanosecond_type elapsedAtEnd(endTime.system + endTime.user);
							insertElapsed += nanosecond_type(elapsedAtEnd - elapsedAtStart) / nanosecond_type(1000000LL);
						}
					}
					else { //remove the pattern from allPatterns list
						cout << "\tThis pattern couldn't pass Jaccard refinement, deleting it from patterns" << endl;
						allPatterns.at(cardinality - 2).erase(allPatterns.at(cardinality - 2).begin() + i);
						sizek_candidates.erase(sizek_candidates.begin() + i);

						for (unsigned int i = 0; i < candTableInst.size(); i++){
							//clean up pointers
							PatternInstance* tmp = candTableInst.at(i);
							delete tmp;
						}

						i--;
					}

				}
				else { //remove the pattern from allPatterns list
					cout << "\tThis pattern couldn't pass OMAX refinement, deleting it from patterns" << endl;
					allPatterns.at(cardinality - 2).erase(allPatterns.at(cardinality - 2).begin() + i);
					sizek_candidates.erase(sizek_candidates.begin() + i);

                    for (unsigned int i = 0; i < candTableInst.size(); i++) {
						//clean up pointers
						PatternInstance* tmp = candTableInst.at(i);
						delete tmp;
					}

					i--;
				} //end of OMAX and J refinement

				//cin >> stopper;
			}//end of if&else -  cardinality check
		} //end of for - iterating all the sizek_candidates
	}

	cout << "Insert time " << insertElapsed / 1000 << endl;
	cout << "Join time " << joinElapsed / 1000 << endl;
}

void Miner::filterCandidataPatternInstances_OMAX() {

}

void Miner::filterCandidataPatternInstances_Jaccard() {

}

void Miner::refineCandidatePatterns_OMAX() {

}

void Miner::refineCandidatePatterns_Jaccard() {

}

void Miner::exploreFeatures() {

	//for each file in the features directory called 'datasetDir'
	//read the names of the features and give them id numbers, when done use allFeatures
	//then start inputting the instances into directories.

	cout << "Reading features ... " << endl;
	allFeatures = ir->readFeatures();
	for (unsigned int i = 0; i < allFeatures.size(); i++){
		cout << "FeatureId: " << allFeatures.at(i).featureId << "\t" << "FeatureName: " << allFeatures.at(i).featureName << endl;
	}

	cout << "Reading instances ..." << endl;
	ir->readInstances(allFeatures);
	featureCounts = ir->getFeatureCounts();
	int i = 0;
	if (featureCounts.size() != allFeatures.size()){
		cout << "Man we have a problem here!" << featureCounts.size() << " - " << allFeatures.size() << endl;
		cin >> i;
	}
	else{
		for (unsigned int i = 0; i < allFeatures.size(); i++){
			cout << "FeatureId: " << allFeatures.at(i).featureId << "\t" << "FeatureName: " << allFeatures.at(i).featureName
				<< "\tCount: " << featureCounts.at(i) << endl;
		}
	}
}
