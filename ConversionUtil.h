/*
 * ConversionUtil.h
 *
 *  Created on: Jun 24, 2014
 *      Author: vijay.akkineni
 */

#ifndef CONVERSIONUTIL_H_
#define CONVERSIONUTIL_H_

#include <list>
#include <vector>
#include <string>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

typedef boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian> pointsInFloat;


class ConversionUtil {
public:
    ConversionUtil();
    virtual ~ConversionUtil();

    pointsInFloat convertHGSToPixXY(pointsInFloat pointIn);
	pointsInFloat convertHPCToPixXY(pointsInFloat pointIn);
    pointsInFloat convert_hg_hcc(double hglon_deg, double hglat_deg);
    pointsInFloat convert_hcc_hpc(double x, double y);
    double rad2deg(double val);
    double deg2rad(double val);
    pointsInFloat convert_hpc_hcc(double x, double y);
    pointsInFloat convert_hcc_hg(double x, double y);
};


#endif /* CONVERSIONUTIL_H_ */
