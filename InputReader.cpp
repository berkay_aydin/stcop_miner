/*
 * InputReader.cpp
 *
 *  Created on: Jun 7, 2014
 *      Author: Vijay Akkineni
 */

#include "InputReader.h"

//string binaryFileName = "instanceFile";
//string binaryPatternFileName = "patternInstanceFile";

InputReader::InputReader(string dir, IDBAccess* _idb_access) {
    cout << "Constructor invocation" << endl;
    datasetDirectory = dir;
    idb_access = _idb_access;
    featureIDCounter = 0;
    //vector<Feature> featureVector = readFeatures();
    //readInstances(featureVector);
}

vector<Feature> InputReader::readFeatures() {

    boost::regex re;

    vector<Feature> featureVector;
    path datasetDirectoryPath = system_complete(path(datasetDirectory));

    cout << "read Features" << endl;

    if (!is_directory(datasetDirectoryPath)) {
        cout << datasetDirectoryPath << " is not a directory!" << endl;
    } else {
        directory_iterator end;
        for (directory_iterator it(datasetDirectoryPath); it != end; ++it) {
            if (is_regular_file(*it)) {
                path fileName = it->path().filename();
                path filePath = it->path();
                if (!(fileName.string() == "metadata")) {
                    featureIDCounter++;
                    Feature feature;
                    feature.featureId = featureIDCounter;
                    feature.featureName = fileName.string();
                    //feature.printFeature();
                    featureVector.push_back(feature);
                }
            }
        }
    }

    return featureVector;
}

void InputReader::readInstances(vector<Feature> features) {

    boost::char_separator<char> sep("-;|\f\n\r\t\v");

    for (unsigned int i = 0; i < features.size(); i++) {
        Feature feature = features.at(i);
        stream<mapped_file_source> featureFile;
        string line;
        path featureFilePath = system_complete(path(datasetDirectory + "/" + feature.featureName));
        featureFile.open(mapped_file_source(featureFilePath.string()));
        int count = 0;
        if (featureFile.is_open()) {

            int previousInstanceID = -1;
            long startTime = 0;
            long endTime = 0;
            string wkt;
            list<string> wkts;
            long currentTime = 0;

            bool firstRun = true;

            while (getline(featureFile, line)) {
                int index = 0;
                tokenizer tokens(line, sep);
                //Loop for reading a line
                int currentInstanceId = -1;
                for (tokenizer::iterator tok_iter = tokens.begin();
                        tok_iter != tokens.end(); ++tok_iter) {
                    //Reading instance ID
                    if (index == 0) {
                        currentInstanceId = boost::lexical_cast<int>(*tok_iter);
                    }
                    //Reading time
                    if (index == 1) {
                        currentTime = boost::lexical_cast<long>(*tok_iter);
                    }
                    //Reading wkt
                    if (index == 2) {
                        wkt = *tok_iter;
                    }
                    index++;
                }

                if (previousInstanceID == currentInstanceId) {
                    wkts.push_back(wkt);
                    endTime = currentTime;
                } else if (firstRun) {
                    firstRun = false;
                    previousInstanceID = currentInstanceId;
                    wkts.push_back(wkt);
                    startTime = currentTime;
                    endTime = currentTime;
                } else {
                    Instance *inst = new Instance(previousInstanceID, feature.featureId, startTime, endTime, wkts);
                    wkts.clear();
                    wkts.push_back(wkt);
                    startTime = currentTime;
                    endTime = currentTime;
                    previousInstanceID = currentInstanceId;
                    //inst->printInstance();
                    if(inst->getInstanceId() != -1) {
                        idb_access->insert(inst, to_string(feature.featureId));
                        count++;
                    }
                }

            }
            Instance *inst = new Instance(previousInstanceID, feature.featureId,
                                          startTime, endTime, wkts);
            count++;
            this->idb_access->insert(inst, to_string(feature.featureId));
            featureCounts.push_back(count);
            featureFile.close();
        }
    }
}

vector<int> InputReader::getFeatureCounts() {
    return featureCounts;
}

