/*
 * Miner.h
 *
 *  Created on: May 27, 2014
 *      Author: berkay
 */

#ifndef MINER_H_
#define MINER_H_

#include <vector>


#include "Pattern.h"
#include "Feature.h"
#include "db/interface/IDBAccess.hpp"
#include "Reader.h"
#include <boost/timer/timer.hpp>

using boost::timer::cpu_timer;
using boost::timer::cpu_times;
using boost::timer::nanosecond_type;

class Miner {

public:

    Miner(double cce_th, double pi_th, IDBAccess* idb_access, Reader* file_reader);

    void generateCandidatePatterns(int cardinality);
    void generateCandidatePatternInstances(int cardinality);

    void filterCandidataPatternInstances_OMAX();
    void refineCandidatePatterns_OMAX();
    void filterCandidataPatternInstances_Jaccard();
    void refineCandidatePatterns_Jaccard();

private:
    vector<Feature> allFeatures;
    vector<int> featureCounts;
    vector<vector<Pattern> > allPatterns;
    double cce_threshold;
    double pi_threshold;

    cpu_timer timer;
    double insertElapsed;
    double joinElapsed;

    IDBAccess* IDB_accessor;
    Reader* ir;

    void exploreFeatures();


};



#endif /* MINER_H_ */
