/*
* InputReader.cpp
*
*  Created on: Jun 7, 2014
*      Author: berkay
*/

#include "RealLifeDataInputReader.h"

using namespace boost::posix_time;
typedef boost::geometry::model::point<int, 2, boost::geometry::cs::cartesian> point;
typedef boost::geometry::model::polygon<point, false, false > Polygon;

typedef boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian> point2d;
typedef boost::geometry::model::polygon<point2d, false, false > Polygon2d;

//string binaryFileName = "instanceFile";
//string binaryPatternFileName = "patternInstanceFile";

RealLifeDataInputReader::RealLifeDataInputReader(string dir, IDBAccess* _idb_access, string _dataSetBaseTime, int _cadenceInMinutes) {
	cout << "Constructor invocation" << endl;
	datasetDirectory = dir;
	idb_access = _idb_access;
	dataSetBaseTime = _dataSetBaseTime;
	cadenceInMinutes = _cadenceInMinutes;
	featureIDCounter = 0;
	//vector<Feature> featureVector = readFeatures();
	//readInstances(featureVector);
}

vector<Feature> RealLifeDataInputReader::readFeatures() {

	boost::regex re;

	vector<Feature> featureVector;
	path datasetDirectoryPath = system_complete(path(datasetDirectory));

	cout << "read Features" << endl;

	if (!is_directory(datasetDirectoryPath)) {
		cout << datasetDirectoryPath << " is not a directory!" << endl;
	}
	else {
		directory_iterator end;
		for (directory_iterator it(datasetDirectoryPath); it != end; ++it) {
			if (is_regular_file(*it)) {
				path fileName = it->path().filename();
				path filePath = it->path();
				if (!(fileName.string() == "metadata")) {
					featureIDCounter++;
					Feature feature;
					feature.featureId = featureIDCounter;
					feature.featureName = fileName.string();
					feature.printFeature();
					featureVector.push_back(feature);
				}
			}
		}
	}

	return featureVector;
}

vector<int> RealLifeDataInputReader::getFeatureCounts() {
	return featureCounts;
}

Instance* RealLifeDataInputReader::createInstance(int previousInstanceID, const Feature& feature, long  startTime, long  endTime, list<string>& wkts) {
	vector<Polygon> geometries;

	float volume = 0;
	int instId = -1;

	if ((long)wkts.size() != (1 + endTime - startTime)) {
		cout << "The timestamps do not overlap, there is a problem" << wkts.size() << " vs " << (1 + endTime - startTime) << endl;
	}
	else {
		Polygon2d poly2d;
		Polygon polygon;

		for (list<string>::iterator it = wkts.
			begin();
			it != wkts.end();
		it++) {

			try {
				read_wkt(*it, poly2d);
			}
			catch (std::exception const&  ex) {
				instId = -1;
				break;
			}

			correct(poly2d);

			for (unsigned int i = 0; i < poly2d.outer().size(); i++) {

				point2d pointInXYPSpace = convUtil.convertHPCToPixXY(poly2d.outer().at(i));

				float point_x = pointInXYPSpace.get<0>();
				float point_y = pointInXYPSpace.get<1>();

				point point_i((int)point_x, (int)point_y);
				polygon.outer().push_back(point_i);
			}
			correct(polygon);
			//cout << "I am trying the integer polygons here:" << dsv(polygon) << std::endl;
			//int stopper = 0;
			//cin >> stopper;

			//cout << area(polygon) << endl;
			volume += area(polygon);
			//cout << volume << endl;
			geometries.push_back(polygon);
			polygon.clear();
			poly2d.clear();

		}
	}

	Instance* inst = new Instance(previousInstanceID, feature.featureId, startTime, endTime, geometries, volume);
	return inst;
}

void  RealLifeDataInputReader::readInstances(vector<Feature> features) {

	boost::char_separator<char> sep("\t");

	for (unsigned int i = 0; i < features.size(); i++) {
		Feature feature = features.at(i);
		stream<mapped_file_source> featureFile;
		string line;
		path featureFilePath = system_complete(path(datasetDirectory + "/" + feature.featureName));
		featureFile.open(mapped_file_source(featureFilePath.string()));
		int count = 0;

		ptime instanceBenchTime = time_from_string(dataSetBaseTime);

		if (featureFile.is_open()) {

			int previousInstanceID = -1;
			long startTime = 0;
			long endTime = 0;
			string wkt;
			list<string> wkts;

			bool firstLine = true;

			while (getline(featureFile, line)) {
				int index = 0;
				ptime currentTime;
				tokenizer tokens(line, sep);
				//Loop for reading a line
				int currentInstanceId = -1;
				for (tokenizer::iterator tok_iter = tokens.begin();
					tok_iter != tokens.end(); ++tok_iter) {
					//Reading instance ID
					if (index == 0) {
						currentInstanceId = boost::lexical_cast<int>(*tok_iter);
					}
					//Reading time
					if (index == 1) {
						string tempTime = boost::lexical_cast<string>(*tok_iter);
						//cout<< tempTime << endl;
						currentTime = time_from_string(tempTime);

					}
					//Reading wkt
					if (index == 2) {
						wkt = *tok_iter;
						//cout << wkt << endl;
					}
					index++;
				}

				if (firstLine) {
					firstLine = false;
					previousInstanceID = currentInstanceId;
					wkts.push_back(wkt);
					time_duration diff = currentTime - instanceBenchTime;
					startTime = diff.total_seconds() / (cadenceInMinutes * 60);
					endTime = diff.total_seconds() / (cadenceInMinutes * 60);
				}
				else if (previousInstanceID == currentInstanceId) {
					wkts.push_back(wkt);
					time_duration diff = currentTime - instanceBenchTime;
					long tmpEndTime = diff.total_seconds() / (cadenceInMinutes * 60);

					if (endTime + 1 < tmpEndTime)
					{
						cout << "Time is off! @" << tmpEndTime << " and ID: " << previousInstanceID << endl;
						throw std::bad_exception();
					}
					else{
						endTime = tmpEndTime;
					}
				}
				else {
					Instance* inst = createInstance(previousInstanceID, feature, startTime, endTime, wkts);
					inst->printInstance();

					wkts.clear();
					wkts.push_back(wkt);

					time_duration diff = currentTime - instanceBenchTime;
					startTime = diff.total_seconds() / (cadenceInMinutes * 60);
					endTime = diff.total_seconds() / (cadenceInMinutes * 60);
					previousInstanceID = currentInstanceId;

					this->idb_access->insert(inst, to_string(feature.featureId));
					count++;
				}

			}
			Instance *inst = new Instance(previousInstanceID, feature.featureId,
				startTime, endTime, wkts);
			count++;
			this->idb_access->insert(inst, to_string(feature.featureId));
			featureCounts.push_back(count);
			featureFile.close();
		}
	}
}

