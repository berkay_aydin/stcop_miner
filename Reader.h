/*
 * Reader.h
 *
 *  Created on: Jun 23, 2014
 *      Author: vijay.akkineni
 */

#ifndef READER_H_
#define READER_H_

#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <vector>
#include <iostream>

#include "Feature.h"
#include "db/interface/IDBAccess.hpp"


using namespace std;
using namespace boost::filesystem;
using namespace boost::iostreams;

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

class Reader {
public:
    Reader();
    virtual ~Reader();
    virtual vector<Feature> readFeatures() = 0;
    virtual void readInstances(vector<Feature> features) = 0;
    virtual vector<int> getFeatureCounts()  = 0;
};


#endif /* READER_H_ */
