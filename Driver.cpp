/*
 * Driver.cpp
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#include "Miner.h"
#include "Pattern.h"
#include "Instance.h"
#include "PatternInstance.h"
#include "InputReader.h"
#include "db/interface/IDBAccess.hpp"
#include "db/DBAccess.hpp"
#include "db/ChebyshevIdxFactory.hpp"
#include "db/NoIdxFactory.hpp"
#include "db/SETIIdxFactory.hpp"
#include "db/index/interface/IInstIndex.hpp"
#include "db/index/interface/IPatInstIndex.hpp"
#include "tests/DBAccessTest.hpp"
#include "RealLifeDataInputReader.h"
#include <string>

//#include "tests/DBAccessTest.hpp"

int main() {
	string inputDir = "/netdrive/files/SDBData/30days";
	//string inputDir = "/netdrive/files/SDBData/6K_instances";
	string outputDir = "/data_hdd/STCopsProject/";
	/*DBAccessTest* dbaTest = new DBAccessTest();
	dbaTest->testInstJoin();

	dbaTest->testPatInstJoin();*/
	//delete dbaTest;
	//IIndexFactory* index= new SETIIdxFactory(outputDir, 5);
	//IIndexFactory* index = new NoIdxFactory(outputDir, 5);
	IIndexFactory* index = new ChebyshevIdxFactory(outputDir, 5, 1, 24, 0.25);

	IDBAccess *dbAcess = new DBAccess(index);

	Reader* ir;
	//ir = new InputReader(inputDir, dbAcess);
	ir = new RealLifeDataInputReader(inputDir, dbAcess, "2012-06-01 00:00:00", 15);

	Miner m(0.05, 0.02, dbAcess, ir);
	cout << "Finished";
	return 1;





};

