/*
 * Pattern.cpp
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#include "Pattern.h"
#include <iostream>
#include <string>

using namespace std;

Pattern::Pattern(int card) {

    //TODO check if cardinality is larger than number of events
    if(card < 1) {
        cout << "Cardinality cannot be less than 1" << endl;
        card = 0;
    } else {
        cardinality = card;
    }
}

int Pattern::addFeature(int fId) {

    if(featureList.size() != (unsigned int)cardinality) {
        featureList.push_back(fId);
        featureList.sort();


        string pid = "";
        for(list<int>::iterator it = featureList.begin(); it != featureList.end(); it++) {
            pid.append("_");
            pid.append( boost::lexical_cast<std::string>( *it) );
        }
        patternId = pid;

        return 1;
    } else {
        cout << "You cannot add more features. Cardinality of this pattern is " << cardinality << endl;
        return -1;
    }


}

vector<int> Pattern::getFeatureIds() {
    vector<int> f;
    f.assign(featureList.begin(), featureList.end());
    return f;
}

string Pattern::getPatternId() {

    return patternId;

}

void Pattern::printPattern() {

    cout << patternId << endl;

}
