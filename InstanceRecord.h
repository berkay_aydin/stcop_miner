/*
 * InstanceRecord.h
 *
 *  Created on: Jun 24, 2014
 *      Author: vijay.akkineni
 */

#ifndef INSTANCERECORD_H_
#define INSTANCERECORD_H_

#include <list>
#include <vector>
#include <string>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

typedef boost::geometry::model::point<int, 2, boost::geometry::cs::cartesian> point;
typedef boost::geometry::model::polygon<point, false, false > Polygon;

typedef boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian> point2d;
typedef boost::geometry::model::polygon<point2d, false, false > Polygon2d;


using namespace std;
using namespace boost::geometry;

class InstanceRecord {
public:
    InstanceRecord();
    virtual ~InstanceRecord();

    int instanceId;
    int featureId;
    long time;
    double area;

    Polygon polygon;
};


#endif /* INSTANCERECORD_H_ */
