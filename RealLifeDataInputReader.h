/*
* InputReader.h
*
*  Created on: Jun 7, 2014
*      Author: berkay
*/

#ifndef REALLIFEDATAINPUTREADER_H_
#define REALLIFEDATAINPUTREADER_H_

#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <vector>
#include <iostream>
#include "Reader.h"
#include "Feature.h"
#include "db/interface/IDBAccess.hpp"
#include "ConversionUtil.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost::iostreams;

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

class RealLifeDataInputReader : public Reader {

public:
	RealLifeDataInputReader(string dir, IDBAccess* idb_access, string dataSetBaseTime, int cadenceInMinutes);

	vector<Feature> readFeatures();
	void readInstances(vector<Feature> features);
	vector<int> getFeatureCounts();

	Instance* createInstance(int previousInstanceID, const Feature& feature, long  startTime, long  endTime, list<string>& wkts);

private:
	string datasetDirectory;
	IDBAccess* idb_access;
	vector<int> featureCounts;
	string dataSetBaseTime;
	int cadenceInMinutes;
	int featureIDCounter;
	ConversionUtil convUtil;
};

#endif /* INPUTREADER_H_ */
