/*
 * Pattern.h
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#ifndef PATTERN_H_
#define PATTERN_H_

#include "PatternInstance.h"
#include "Feature.h"
#include <iostream>
#include <list>

using namespace std;

class Pattern {

public:
    Pattern(int cardinality);
    int addFeature(int featureId);
    int getCardinality();
    string getPatternId();
    vector<int> getFeatureIds();
    void printPattern();

    void findPatternInstances();
    void calculateCCE();
    void calculatePI();

    list <PatternInstance> patInstanceList; //table instance
private:
    string patternId;
    list <int> featureList;

    int cardinality;
    double cce;
    double pi;

};



#endif /* PATTERN_H_ */
