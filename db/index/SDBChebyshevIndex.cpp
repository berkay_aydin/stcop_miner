//#ifndef CHEBSHEVINDEX_CPP
//#define	CHEBSHEVINDEX_CPP
//
//#include <boost/geometry.hpp>
//#include <boost/geometry/geometries/point.hpp>
//#include <boost/geometry/geometries/box.hpp>
//#include <boost/geometry/geometries/polygon.hpp>
//#include <boost/geometry/algorithms/intersects.hpp>
//#include "boost/date_time/posix_time/posix_time.hpp"
//
//#include <boost/geometry/index/rtree.hpp>
//
//#include <vector>
//#include <tuple/tuple.hpp>
//#include <math.h>
//
//#include <iostream>
//#include <boost/foreach.hpp>
//#include <boost/shared_ptr.hpp>
//#include "../include/IEvent.hpp"
//#include "../include/ITrack.hpp"
//#include "../include/ChebShevPolyGen.hpp"
//#include "opencv2/core/core.hpp"
//
//
//using namespace std;
//
//namespace bg = boost::geometry;
//namespace bt = boost::posix_time;
//namespace bgi = boost::geometry::index;
//
///*namespace boost {
//	namespace geometry {
//	namespace index {
//
//	template <typename ChebPoly>
//	struct indexable< boost::shared_ptr<ChebPoly> >
//	{
//	typedef boost::shared_ptr<ChebPoly> V;
//
//	typedef ChebPoly const& result_type;
//	result_type operator()(V const& v) const { return *v; }
//	};
//
//	}
//	}
//	}*/ // namespace boost::geometry::index
//
//class SDBChebyshevIndex
//{
//
//	struct lowOrdrPolynomials{
//	public:
//		float x0, x1, y0, y1, h0, h1, w0, w1, dx, dy, dh, dw;
//	};
//	typedef bg::model::point<float, 2, bg::cs::cartesian> point;
//	typedef bg::model::box<point> box;
//	typedef bg::model::polygon<point, false, false> pgon;
//
//	typedef boost::tuple<box, lowOrdrPolynomials, ChebShevPoly*, ITrack*> value;
//
//
//	vector<bgi::rtree< value, bgi::rstar<16, 4> >*>* trees;
//	bt::time_period* globalPeriod = NULL;
//	bt::time_duration treeSpan;
//	ChebShevPolyGen* generator;
//	long queryCount, resultPreRefine, resulPostRefine;
//public:
//	SDBChebyshevIndex(boost::posix_time::time_duration treeSpan, ChebShevPolyGen* generator){
//
//		this->treeSpan = treeSpan;
//		this->generator = generator;
//		queryCount = 0;
//		resultPreRefine = 0;
//		resulPostRefine = 0;
//	}
//
//	~SDBChebyshevIndex(){
//
//		for (int i = 0; i < this->trees->size(); i++){
//
//			bgi::rtree< value, bgi::rstar<16, 4> >* tree = this->trees->at(i);
//			delete tree;
//		}
//
//		delete trees;
//		delete globalPeriod;
//	}
//	void insert(ITrack* track){
//
//		bt::time_period trackPeriod(track->getStartTime(), track->getEndTime());
//
//		//get the trees that this track fall into
//		vector<int> idxs = this->getIdx(trackPeriod);
//
//		IEvent* currentEvent = track->getFirst();
//		//get the polynomial at each interval and insert into the proper tree
//		for (int i = 0; i < idxs.size(); i++){
//			int indexNumber = idxs.at(i);
//			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*indexNumber));
//			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));
//
//			while (!currentEvent->getTimePeriod().intersects(indexPeriod))currentEvent = currentEvent->getNext();
//
//			ChebShevPoly* chebpolynomial = this->generator->getPoly(currentEvent, indexPeriod, trackPeriod);
//			if (chebpolynomial->isValid()){
//				/*cout << "working so far:" << i << endl;*/
//				lowOrdrPolynomials polys;
//				polys.x0 = chebpolynomial->getXCoff()->at(0);
//				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
//				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
//				polys.w0 = chebpolynomial->getWidthCoff()->at(0);
//
//				polys.dx = chebpolynomial->getXDev();
//				polys.dy = chebpolynomial->getYDev();
//				polys.dh = chebpolynomial->getHeightDev();
//				polys.dw = chebpolynomial->getWidthDev();
//
//				//if we have more than one coefficient use the first two
//				if (chebpolynomial->getXCoff()->size() > 1){
//
//					polys.x1 = chebpolynomial->getXCoff()->at(1);
//					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
//					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
//					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
//				}
//				else{
//					//otherwise we have a constant valued function and the
//					//second coefficient is zero
//					polys.x1 = 0;
//					polys.y1 = 0;
//					polys.h1 = 0;
//					polys.w1 = 0;
//				}
//
//
//
//				pgon p = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);
//
//				box b = bg::return_envelope<box>(p);
//				this->trees->at(idxs.at(i))->insert(boost::make_tuple(b, polys, chebpolynomial, track));
//			}
//			else{
//				cout << "somenting is wrong" << endl;
//				throw std::bad_exception();
//			}
//		}
//
//
//	}
//
//	void insert(list<ITrack*>* tracks){
//
//		for (list<ITrack*>::iterator iter = tracks->begin(); iter != tracks->end(); iter++){
//			ITrack* currentTrack = iter.operator*();
//			this->insert(currentTrack);
//		}
//	}
//
//
//	vector<ITrack*> query(ITrack* track){
//		std::vector<ITrack*> result_n;
//
//
//
//		bt::time_period trackPeriod(track->getStartTime(), track->getEndTime());
//		bt::time_period intersectPeriod = this->globalPeriod->intersection(trackPeriod);
//
//		bt::time_duration diff = intersectPeriod.begin() - this->globalPeriod->begin();
//		int beginIdx = (int)diff.total_seconds() / this->treeSpan.total_seconds();
//		int endIdx = beginIdx + (int)intersectPeriod.length().total_seconds() / this->treeSpan.total_seconds();
//
//		//box b = bg::return_envelope<box>(ply);
//		IEvent* currentEvent = track->getFirst();
//		for (int i = beginIdx; i <= endIdx; i++){
//			//cout << "Idx: " << i << endl;
//			std::vector<value>result_tmp;
//
//			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*i));
//			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));
//
//			while (!currentEvent->getTimePeriod().intersects(indexPeriod))currentEvent = currentEvent->getNext();
//
//			ChebShevPoly* chebpolynomial = this->generator->getPoly(currentEvent, indexPeriod, trackPeriod);
//
//			if (chebpolynomial->isValid()){
//				/*cout << "working so far:" << i << endl;*/
//				lowOrdrPolynomials polys;
//				polys.x0 = chebpolynomial->getXCoff()->at(0);
//				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
//				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
//				polys.w0 = chebpolynomial->getWidthCoff()->at(0);
//
//				polys.dx = chebpolynomial->getXDev();
//				polys.dy = chebpolynomial->getYDev();
//				polys.dh = chebpolynomial->getHeightDev();
//				polys.dw = chebpolynomial->getWidthDev();
//
//				//if we have more than one coefficient use the first two
//				if (chebpolynomial->getXCoff()->size() > 1){
//
//					polys.x1 = chebpolynomial->getXCoff()->at(1);
//					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
//					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
//					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
//				}
//				else{
//					//otherwise we have a constant valued function and the
//					//second coefficient is zero
//					polys.x1 = 0;
//					polys.y1 = 0;
//					polys.h1 = 0;
//					polys.w1 = 0;
//				}
//
//
//
//				pgon searchPly = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);
//
//				//get a bounding box and insert the tuple into the tree
//				//box b = bg::return_envelope<box>(p);
//
//				this->trees->at(i)->query(bgi::intersects(searchPly), std::back_inserter(result_tmp));
//
//				BOOST_FOREACH(value res, result_tmp){
//
//					ChebShevPoly* resultCheb = get<2>(res);
//					float timeSlice = fabs((chebpolynomial->getValidIntervalEnd() - chebpolynomial->getValidIntervalBegin()) / 24.0);
//					float sliceStart = chebpolynomial->getValidIntervalBegin();
//					for (int j = 0; j < 24; j++){
//
//						float sliceEnd = sliceStart + timeSlice;
//						if (sliceStart >= resultCheb->getValidIntervalBegin()){
//
//							if (sliceEnd > resultCheb->getValidIntervalEnd() && sliceStart < resultCheb->getValidIntervalEnd()){
//								sliceEnd = resultCheb->getValidIntervalEnd();
//							}
//							else if (sliceStart >= resultCheb->getValidIntervalEnd())break;
//
//							bg::model::polygon<point, false, false> resultPoly = this->getFullPGone(resultCheb, sliceStart, sliceEnd, true);
//							bg::model::polygon<point, false, false> queryPoly = this->getFullPGone(chebpolynomial, sliceStart, sliceEnd, true);
//
//							if (bg::intersects(queryPoly, resultPoly)){
//								result_n.push_back(get<3>(res));
//								break;
//							}
//						}
//						sliceStart = sliceEnd;
//					}
//				}
//
//			}
//			else{
//				cout << "Somenting is wrong" << endl;
//				throw std::bad_exception();
//			}
//
//
//		}
//		std::sort(result_n.begin(), result_n.end());
//		std::vector<ITrack*>::iterator it = std::unique(result_n.begin(), result_n.end());
//		result_n.resize(std::distance(result_n.begin(), it));
//
//		//cout << "result size before Refine:" << result_n.size() << endl;
//		this->resultPreRefine += result_n.size();
//
//		list<IEvent*>* events = track->getEvents();
//		vector<ITrack*> finalResultsVect;
//
//		for (vector<ITrack*>::iterator it = result_n.begin(); it != result_n.end(); it++){
//			ITrack* tr = it.operator *();
//			bool isIntersecting = false;
//			IEvent* ev = tr->getFirst();
//			//check all events in the returned track
//			while (ev != NULL){
//				boost::posix_time::time_period per = ev->getTimePeriod();
//				if (per.intersects(trackPeriod)){
//					//check all events in the track we are querying against
//					for (list<IEvent*>::iterator eit = events->begin(); eit != events->end(); eit++){
//						IEvent* trEv = eit.operator *();
//						if (trEv->getTimePeriod().intersects(per)){
//							vector<cv::Point2i>* trEvShape = trEv->getShape();
//							vector<cv::Point2i>* evShape = ev->getShape();
//							if (polysIntersect(evShape, trEvShape))
//							{
//								isIntersecting = true;
//								break;
//							}
//						}
//					}
//				}
//				if (isIntersecting){
//					finalResultsVect.push_back(tr);
//					break;
//				}
//				ev = ev->getNext();
//			}
//		}
//
//		//cout << "result size:" << finalResultsVect.size() << endl;
//		this->resulPostRefine += finalResultsVect.size();
//		this->queryCount++;
//		return finalResultsVect;
//	}
//
//	void disp(){
//		double count = 0;
//		for (int i = 0; i < this->trees->size(); i++){
//			bgi::rtree< value, bgi::rstar<16, 4> >* tree = this->trees->at(i);
//			int sz = tree->size();
//			//cout << "Tree at " << i << " size: " << sz << endl;
//			count += sz;
//		}
//		double avg = count / this->trees->size();
//		cout << "Total Items in Index: " << count << endl;
//		cout << "Avg Tree Size:" << avg << endl;
//
//		double avgRef = (this->resultPreRefine - this->resulPostRefine);
//		avgRef = avgRef / queryCount;
//		double avgResult = this->resulPostRefine / (double)this->queryCount;
//		cout << "Average removed in Refine step: " << avgRef << endl;
//		cout << "Average query result: " << avgResult << endl;
//	}
//
//private:
//
//	//needed because can't test two polygon intersections with vectors of points
//	bool polysIntersect(vector<cv::Point2i>* left, vector<cv::Point2i>* right){
//		pgon lp, rp;
//		for (vector<cv::Point2i>::iterator it = left->begin(); it != left->end(); it++){
//			cv::Point2i pt = it.operator *();
//			lp.outer().push_back(point(pt.x, pt.y));
//		}
//
//		for (vector<cv::Point2i>::iterator it = right->begin(); it != right->end(); it++){
//
//			cv::Point2i pt = it.operator *();
//			rp.outer().push_back(point(pt.x, pt.y));
//		}
//
//		return bg::intersects(lp, rp);
//	}
//
//	pgon getFullPGone(ChebShevPoly* polynomial, float begin, float end, bool withDev){
//
//		if (withDev){
//			float minX = this->functAtT(polynomial->getXCoff(), begin) - polynomial->getXDev() -
//				polynomial->getWidthDev() - (functAtT(polynomial->getWidthCoff(), begin) / 2.0);
//
//			float yBegin = this->functAtT(polynomial->getYCoff(), begin);
//			float hBegin = (functAtT(polynomial->getHeightCoff(), begin) / 2.0);
//
//			float minYBegin = yBegin - polynomial->getYDev() - polynomial->getHeightDev() - hBegin;
//			float maxYBegin = yBegin + polynomial->getYDev() + polynomial->getHeightDev() + hBegin;
//
//
//			point ll(minX, minYBegin);
//			point ul(minX, maxYBegin);
//
//			float maxX = this->functAtT(polynomial->getXCoff(), end) + polynomial->getXDev() +
//				polynomial->getWidthDev() + (functAtT(polynomial->getWidthCoff(), end) / 2.0);
//
//			float yEnd = this->functAtT(polynomial->getYCoff(), end);
//			float hEnd = (functAtT(polynomial->getHeightCoff(), end) / 2.0);
//
//			float minYEnd = yEnd - polynomial->getYDev() - polynomial->getHeightDev() - hEnd;
//			float maxYEnd = yEnd + polynomial->getYDev() + polynomial->getHeightDev() + hEnd;
//
//			point lr(maxX, minYEnd);
//			point ur(maxX, maxYEnd);
//
//			pgon p;
//			p.outer().push_back(point(minX, maxYBegin));
//			p.outer().push_back(point(minX, minYBegin));
//			p.outer().push_back(point(maxX, minYEnd));
//			p.outer().push_back(point(maxX, maxYEnd));
//
//			return p;
//		}
//		else{
//			float minX = this->functAtT(polynomial->getXCoff(), begin) - (functAtT(polynomial->getWidthCoff(), begin) / 2.0);
//
//			float yBegin = this->functAtT(polynomial->getYCoff(), begin);
//			float hBegin = (functAtT(polynomial->getHeightCoff(), begin) / 2.0);
//
//			float minYBegin = yBegin - hBegin;
//			float maxYBegin = yBegin + hBegin;
//
//
//			point ll(minX, minYBegin);
//			point ul(minX, maxYBegin);
//
//			float maxX = this->functAtT(polynomial->getXCoff(), end) + (functAtT(polynomial->getWidthCoff(), end) / 2.0);
//
//			float yEnd = this->functAtT(polynomial->getYCoff(), end);
//			float hEnd = (functAtT(polynomial->getHeightCoff(), end) / 2.0);
//
//			float minYEnd = yEnd - hEnd;
//			float maxYEnd = yEnd + hEnd;
//
//			point lr(maxX, minYEnd);
//			point ur(maxX, maxYEnd);
//
//			pgon p;
//			p.outer().push_back(point(minX, maxYBegin));
//			p.outer().push_back(point(minX, minYBegin));
//			p.outer().push_back(point(maxX, minYEnd));
//			p.outer().push_back(point(maxX, maxYEnd));
//
//			return p;
//		}
//
//	}
//
//	float functAtT(vector<float>* coefficients, float t){
//		float val = 0.0;
//		float tm_2 = 1;
//		float tm_1 = t;
//
//		val = coefficients->at(0);
//		if (coefficients->size() > 1){
//			val = t*coefficients->at(0);
//		}
//
//		if (coefficients->size() < 3){
//			return val;
//		}
//		for (int i = 2; i < coefficients->size(); i++){
//			float tm = 2.0*t*tm_1 - tm_2;
//			val += coefficients->at(i)*tm;
//
//			tm_2 = tm_1;
//			tm_1 = tm;
//		}
//		return val;
//	}
//
//	pgon getLinearPGone(lowOrdrPolynomials polys, float begin, float end, bool withDev){
//
//		if (withDev){
//			float minX = polys.x0 + (polys.x1*begin) - polys.dx - polys.dw - ((polys.w0 + (polys.w1*begin)) / 2.0);
//			float maxX = polys.x0 + (polys.x1*end) + polys.dx + polys.dw + ((polys.w0 + (polys.w1*end)) / 2.0);
//
//			float minY = polys.y0 + (polys.y1*begin);
//			float minH = (polys.h0 + (polys.h1*begin)) / 2.0;
//			float ulY = minY + polys.dy + polys.dh + minH;
//			float llY = minY - polys.dy - polys.dh - minH;
//
//
//			float maxY = polys.y0 + (polys.y1*end);
//			float maxH = (polys.h0 + (polys.h1*end)) / 2.0;
//			float urY = maxY + polys.dy + polys.dh + maxH;
//			float lrY = maxY - polys.dy - polys.dh - maxH;
//
//			pgon p;
//			p.outer().push_back(point(minX, ulY));
//			p.outer().push_back(point(minX, llY));
//			p.outer().push_back(point(maxX, lrY));
//			p.outer().push_back(point(maxX, urY));
//
//			return p;
//		}
//		else{
//			float minX = polys.x0 + (polys.x1*begin) - ((polys.w0 + (polys.w1*begin)) / 2.0);
//			float maxX = polys.x0 + (polys.x1*end) + ((polys.w0 + (polys.w1*end)) / 2.0);
//
//			float minY = polys.y0 + (polys.y1*begin);
//			float minH = (polys.h0 + (polys.h1*begin)) / 2.0;
//			float ulY = minY + minH;
//			float llY = minY - minH;
//
//
//			float maxY = polys.y0 + (polys.y1*end);
//			float maxH = (polys.h0 + (polys.h1*end)) / 2.0;
//			float urY = maxY + maxH;
//			float lrY = maxY - maxH;
//
//			pgon p;
//			p.outer().push_back(point(minX, ulY));
//			p.outer().push_back(point(minX, llY));
//			p.outer().push_back(point(maxX, lrY));
//			p.outer().push_back(point(maxX, urY));
//
//			return p;
//		}
//	}
//
//	vector<int> getIdx(bt::time_period period){
//
//		//if not init then do so
//		if (this->globalPeriod == NULL){
//			int length = (int)period.length().total_seconds() / this->treeSpan.total_seconds();
//			bt::ptime end = period.begin() + this->treeSpan * length + this->treeSpan;
//			this->globalPeriod = new bt::time_period(period.begin(), end);
//			//this->trees = new vector<PATree>();
//			this->trees = new vector<bgi::rtree< value, bgi::rstar<16, 4> >*>();
//			this->addTrees(this->globalPeriod->begin(), length + 2);
//		}
//
//		// if this period starts before our current time period we will need to widen the period
//		if (this->globalPeriod->begin() > period.begin()){
//
//			//get the duration of the difference between the original start period and the new one
//			//that is needed to accomidate this new time period as well.
//			bt::time_duration diff = this->globalPeriod->begin() - period.begin();
//
//			//find the the number of periods this difference covers and then expand our global period by that + 1
//			int length = (int)diff.total_seconds() / this->treeSpan.total_seconds();
//			bt::ptime start = this->globalPeriod->begin() - bt::seconds(this->treeSpan.total_seconds()*length) - bt::seconds(this->treeSpan.total_seconds());
//
//			bt::time_period* oldPeriod = this->globalPeriod;
//			this->globalPeriod = new bt::time_period(start, oldPeriod->end());
//			delete oldPeriod;
//
//			this->addTrees(this->globalPeriod->begin(), length + 1, true);
//		}
//
//		//if this period ends after our current time period we will need to widen the period
//		if (this->globalPeriod->end() < period.end()){
//
//			//get the duration of the difference between the original end period and the new one
//			//that is needed to accomidate this new time period as well.
//			bt::time_duration diff = period.end() - this->globalPeriod->end();
//
//			int length = (int)diff.total_seconds() / this->treeSpan.total_seconds();
//			bt::ptime end = this->globalPeriod->end() + bt::seconds(this->treeSpan.total_seconds() * length) + bt::seconds(this->treeSpan.total_seconds());
//
//			bt::time_period* oldPeriod = this->globalPeriod;
//			this->globalPeriod = new bt::time_period(oldPeriod->begin(), end);
//			this->addTrees(oldPeriod->end(), length + 1);
//			delete oldPeriod;
//		}
//
//
//		vector<int> returnVect;
//		bt::time_duration diff = period.begin() - this->globalPeriod->begin();
//		int beginIdx = (int)diff.total_seconds() / this->treeSpan.total_seconds();
//		int endIdx = beginIdx + (int)period.length().total_seconds() / this->treeSpan.total_seconds();
//		for (int i = beginIdx; i <= endIdx; i++){
//			returnVect.push_back(i);
//		}
//
//		return returnVect;
//	}
//
//	/*
//	This method assumes that this->trees is already created
//	do not call it until the vector is created
//	*/
//	void addTrees(bt::ptime start, int num, bool front = false){
//
//		if (front){
//			//vector<PATree>* oldTrees = this->trees;
//			vector<bgi::rtree< value, bgi::rstar<16, 4> >*>* oldTrees = this->trees;
//
//			//this->trees = new vector<PATree>();
//			this->trees = new vector<bgi::rtree< value, bgi::rstar<16, 4> >*>();
//			bt::ptime end = start + this->treeSpan;
//			for (int i = 0; i < num; i++){
//				//PATree pa;
//				//pa.period = new bt::time_period(start, end);
//				//pa.tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
//				bgi::rtree< value, bgi::rstar<16, 4> >* tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
//				start = end;
//				end += this->treeSpan;
//				this->trees->push_back(tree);
//			}
//
//			for (int i = 0; i < oldTrees->size(); i++){
//				this->trees->push_back(oldTrees->at(i));
//			}
//			delete oldTrees;
//		}
//		else{
//			//bt::ptime end = start + this->treeSpan;
//			for (int i = 0; i < num; i++){
//				//PATree pa;
//				//pa.period = new bt::time_period(start, end);
//				//pa.tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
//				bgi::rtree< value, bgi::rstar<16, 4> >* tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
//				//start = end;
//				//end += this->treeSpan;
//				this->trees->push_back(tree);
//			}
//		}
//	}
//
//};
//#endif
