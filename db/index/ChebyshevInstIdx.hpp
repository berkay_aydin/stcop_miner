/*
* ChebShevInstIndex.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef CHEBYSHEVINSTINDEX_HPP_
#define CHEBYSHEVINSTINDEX_HPP_

#include <vector>
#include <tuple>
#include <math.h>

#include <boost/foreach.hpp>
#include <boost/geometry/algorithms/intersects.hpp>

#include "db/index/interface/IInstIndex.hpp"
#include "db/io/interface/IInstFileAccess.hpp"
#include "db/index/include/ChebyshevPolyGen.hpp"
#include "db/index/include/ChebyshevIndexBase.hpp"


using namespace std;

namespace bg = boost::geometry;
namespace bt = boost::posix_time;
namespace bgi = boost::geometry::index;

class ChebyshevInstIndex : public IInstIndex, private ChebyshevIndexBase {
private:

	ChebyshevPolyGen* generator;
	long queryCount, resultPreRefine, resulPostRefine, cadencePeroid;
	IInstFileAccess* fileAccess;

public:

	ChebyshevInstIndex(IInstFileAccess* fileAccess, ChebyshevPolyGen* generator, boost::posix_time::time_duration treeSpan, long cadencePeroid) :ChebyshevIndexBase(treeSpan) {

		this->fileAccess = fileAccess;
		this->generator = generator;
		this->cadencePeroid = cadencePeroid;
		queryCount = 0;
		resultPreRefine = 0;
		resulPostRefine = 0;
	}

	~ChebyshevInstIndex() {
		delete this->fileAccess;
	}


	/************************************************************************/
	/* insert		:Method that inserts instances into the index and stores them into a file

	@param inst		:The instance that is to be stored in a file and inserted into the index*/
	/************************************************************************/
	void insert(Instance* inst) {

		//get the period of the instnce we shall insert into the index
		bt::time_period trackPeriod(bt::from_time_t(inst->getStartTime()), bt::from_time_t(inst->getEndTime()));

		//get the trees that this instance falls into
		vector<int> idxs = this->getIdx(trackPeriod);

		//we need to get the geometries before we write the object to the file as that destroys the object
		vector<Polygon> instPolys = inst->getPolygons();

		//write the object to file and get the location back for later access
		bt::time_period currPeriod(bt::from_time_t(inst->getStartTime()), seconds(this->cadencePeroid));
		long instFileLoc = this->fileAccess->writeInstanceToFile(inst);
		delete inst;

		//get the polynomial at each interval and insert into the proper tree
		vector<Polygon>::iterator instPolysIter = instPolys.begin();
		for (unsigned int i = 0; i < idxs.size(); i++) {

			//get the index of the current tree for insertion
			int indexNumber = idxs.at(i);

			//get the time period of the current tree as we need it to calculate the chebyshev in and subsequent polygons for indexing
			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*indexNumber));
			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));

			//go the the first polygon that intersects the period of the current tree
			while (!currPeriod.intersects(indexPeriod)) {
				currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
				instPolysIter++;
			}

			//calculate the chepyshev polynomial for the portion of the instance that falls into the time interval of the current tree
			ChebyshevPoly* chebpolynomial = this->generator->getPoly(instPolysIter, instPolys.end(), indexPeriod, trackPeriod);

			//now get a linear approximating coefficients of this portion of the instance
			if (chebpolynomial->isValid()) {
				/*cout << "working so far:" << i << endl;*/
				lowOrdrPolynomials polys;
				polys.x0 = chebpolynomial->getXCoff()->at(0);
				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
				polys.w0 = chebpolynomial->getWidthCoff()->at(0);

				polys.dx = chebpolynomial->getXDev();
				polys.dy = chebpolynomial->getYDev();
				polys.dh = chebpolynomial->getHeightDev();
				polys.dw = chebpolynomial->getWidthDev();

				//if we have more than one coefficient use the first two
				if (chebpolynomial->getXCoff()->size() > 1) {

					polys.x1 = chebpolynomial->getXCoff()->at(1);
					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
				}
				else {
					//otherwise we have a constant valued function and the
					//second coefficient is zero
					polys.x1 = 0;
					polys.y1 = 0;
					polys.h1 = 0;
					polys.w1 = 0;
				}

				//use the linear approximation to get a polygon that represents it
				Polygon p = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);

				//get the envelope of that polygon, and insert the information into the current tree
				box b = bg::return_envelope<box>(p);
				this->trees->at(idxs.at(i))->insert(boost::make_tuple(b, chebpolynomial, instFileLoc));
			}
			else {
				//the next line says it all
				cout << "Somenting is wrong with insert Inst Idx" << endl;
				cout << "ID: " << inst->getFeatureId() << endl;
				cout << "Index Period begin: "<<indexPeriod.begin()<<endl;
				cout << "Index Period end: " << indexPeriod.end() << endl;
				cout << "Track Period begin: " << trackPeriod.begin() << endl;
				cout << "Track Period end: " << trackPeriod.end() << endl;
				throw std::bad_exception();
			}
		}
	}

	/************************************************************************/
	/* searchST_Intersection	:Method for finding instances that intersect with a given Instance

	@param inst					:The instance we seek to find instances intersecting with
	@return						:Returns the instances in the table that intersect with the one passed in*/
	/************************************************************************/
	vector<Instance*>* searchST_Intersection(Instance* inst) {


		//get the period for the search instance so we can generate a chebyshev polynomial for it
		bt::time_period trackPeriod(bt::from_time_t(inst->getStartTime()), bt::from_time_t(inst->getEndTime()));

		//get the time period that the search instance period intersects with data in the index
		bt::time_period intersectPeriod = this->globalPeriod->intersection(trackPeriod);

		//get the index of the trees that this instance period falls into
		bt::time_duration diff = intersectPeriod.begin() - this->globalPeriod->begin();
		int beginIdx = (int)diff.total_seconds() / this->treeSpan.total_seconds();
		int endIdx = beginIdx + (int)intersectPeriod.length().total_seconds() / this->treeSpan.total_seconds();

		//get the polygons, an iterator for them and the period of the first polygon
		vector<Polygon> instPolys = inst->getPolygons();
		vector<Polygon>::iterator instPolysIter = instPolys.begin();
		bt::time_period currPeriod(bt::from_time_t(inst->getStartTime()), seconds(this->cadencePeroid));

		//for all the trees that this instance period falls into search the tree for candidates
		vector<long> filter_results;
		for (int i = beginIdx; i <= endIdx; i++) {

			std::vector<value>result_tmp;

			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*i));
			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));

			while (!currPeriod.intersects(indexPeriod)) {
				currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
				instPolysIter++;
			}

			ChebyshevPoly* chebpolynomial = this->generator->getPoly(instPolysIter, instPolys.end(), indexPeriod, trackPeriod);


			if (chebpolynomial->isValid()) {
				/*cout << "working so far:" << i << endl;*/
				lowOrdrPolynomials polys;
				polys.x0 = chebpolynomial->getXCoff()->at(0);
				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
				polys.w0 = chebpolynomial->getWidthCoff()->at(0);

				polys.dx = chebpolynomial->getXDev();
				polys.dy = chebpolynomial->getYDev();
				polys.dh = chebpolynomial->getHeightDev();
				polys.dw = chebpolynomial->getWidthDev();

				//if we have more than one coefficient use the first two
				if (chebpolynomial->getXCoff()->size() > 1) {

					polys.x1 = chebpolynomial->getXCoff()->at(1);
					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
				}
				else {
					//otherwise we have a constant valued function and the
					//second coefficient is zero
					polys.x1 = 0;
					polys.y1 = 0;
					polys.h1 = 0;
					polys.w1 = 0;
				}



				Polygon searchPly = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);

				this->trees->at(i)->query(bgi::intersects(searchPly), std::back_inserter(result_tmp));


				BOOST_FOREACH(value res, result_tmp) {

					ChebyshevPoly* resultCheb = res.get<1>();
					float timeSlice = fabs((chebpolynomial->getValidIntervalEnd() - chebpolynomial->getValidIntervalBegin()) / 24.0);
					float sliceStart = chebpolynomial->getValidIntervalBegin();
					for (int j = 0; j < 24; j++) {

						float sliceEnd = sliceStart + timeSlice;
						if (sliceStart >= resultCheb->getValidIntervalBegin()) {

							if (sliceEnd > resultCheb->getValidIntervalEnd() && sliceStart < resultCheb->getValidIntervalEnd()) {
								sliceEnd = resultCheb->getValidIntervalEnd();
							}
							else if (sliceStart >= resultCheb->getValidIntervalEnd())
								break;

							Polygon resultPoly = this->getFullPGone(resultCheb, sliceStart, sliceEnd, true);
							Polygon queryPoly = this->getFullPGone(chebpolynomial, sliceStart, sliceEnd, true);

							if (bg::intersects(queryPoly, resultPoly)) {
								filter_results.push_back(res.get<2>());
								break;
							}
						}
						sliceStart = sliceEnd;
					}
				}

				delete chebpolynomial;
			}
			else {
				cout << "You tried to query with an object that falls outside the valid time period for this index!" << endl;
				delete chebpolynomial;
				//  throw std::bad_exception();
			}


		}

		//clear out duplicate results from the vector of candidates
		std::sort(filter_results.begin(), filter_results.end());
		std::vector<long>::iterator it = std::unique(filter_results.begin(), filter_results.end());
		filter_results.resize(std::distance(filter_results.begin(), it));

		//bookkeeping, keeping track of how many filter results were returned
		this->resultPreRefine += filter_results.size();


		vector<Instance* >* finalResultsVect = new vector<Instance* >();
		//look at all the candidate results and determine if they are true results or a false return
		for (vector<long>::iterator it = filter_results.begin(); it != filter_results.end(); it++) {
			long instIdx = it.operator *();
			Instance* instCandidate = this->fileAccess->getInstanceFromFile(instIdx);
			bool isIntersecting = false;
			vector<Polygon> candidatePolys = instCandidate->getPolygons();

			//check intersection of the returned instance against the query instance
			currPeriod = bt::time_period(bt::from_time_t(inst->getStartTime()), seconds(this->cadencePeroid));
			bt::time_period candidateCurrPeriod(bt::from_time_t(instCandidate->getStartTime()), seconds(this->cadencePeroid));
			instPolysIter = instPolys.begin();
			vector<Polygon>::iterator candidatePolysIter = candidatePolys.begin();

			//for each polygon in the search instance, see if there is an actuall intersection with the candidate
			//once one is found, there is no need to continue processing this candidate as it is a true result
			//and can be returned.
			for (; instPolysIter != instPolys.end(); instPolysIter++) {

				//if the current period is before the candidate there is no need to process, just continue.
				if (currPeriod < candidateCurrPeriod) {
					currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
					continue;
				}

				//if the candidate period is before the current of the query instance we need to iterate to the point they potentially
				//intersect and then check for intersection.
				while (candidateCurrPeriod < currPeriod && (candidatePolysIter != candidatePolys.end())) {
					candidateCurrPeriod = bt::time_period(candidateCurrPeriod.end(), seconds(this->cadencePeroid));
					candidatePolysIter++;
				}

				//if we have reached the end of polygons representing the candidate then we cannot check for intersection any longer
				if (candidatePolysIter == candidatePolys.end())break;

				Polygon candidatePoly = candidatePolysIter.operator *();
				Polygon queryInstPoly = instPolysIter.operator *();

				if (bg::intersects(queryInstPoly, candidatePoly)) {
					finalResultsVect->push_back(instCandidate);
					isIntersecting = true;
					break;
				}

				currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
			}

			if (!isIntersecting)
				delete instCandidate;
		}

		//bookkeeping, keeping track of the number of true results were returned and how many queries we have had
		this->resulPostRefine += finalResultsVect->size();
		this->queryCount++;

		//return the intersecting results
		return finalResultsVect;

	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
	return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this->fileAccess->begin());
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
	return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		return Iterator(this->fileAccess->end());
	}


};
#endif /* CHEBYSHEVINSTINDEX_HPP_ */
