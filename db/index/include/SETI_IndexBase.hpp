/*
 * SETI_IndexBase.hpp
 *
 *  Created on: Jun 9, 2014
 *      Author: berkay
 */

#ifndef SETI_INDEXBASE_HPP_
#define SETI_INDEXBASE_HPP_


#include <boost/geometry.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"

#include "db/index/include/SETI_FrontLine.hpp"
#include "db/index/include/SETI_Grid.hpp"


using namespace std;

namespace bg = boost::geometry;
namespace bt = boost::posix_time;
namespace bgi = boost::geometry::index;

typedef bg::model::point<float, 2, bg::cs::cartesian> point;
typedef bg::model::box<point> box;
typedef boost::geometry::model::polygon<point, false, false > Polygon;


class SETI_IndexBase {
public:


    //typedef boost::tuple<box, lowOrdrPolynomials, ChebyshevPoly*, long> value;


    //SETI_Grid grid();

    bt::time_period* globalPeriod = NULL;
    bt::time_duration treeSpan;



    SETI_IndexBase() {

    }

    ~SETI_IndexBase() {
        //delete all the trees of the index as we are killing it

    }
};



#endif /* SETI_INDEXBASE_HPP_ */
