/*
* ChebShevPatInstIndex.hpp
*
*  Created on: May 30, 2014
* Author: Dustin Kempton
*/

#ifndef CHEBYSHEVINDEXBASE_HPP_
#define CHEBYSHEVINDEXBASE_HPP_

#include <boost/geometry.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include "db/index/include/ChebyshevPoly.hpp"

using namespace std;

namespace bg = boost::geometry;
namespace bt = boost::posix_time;
namespace bgi = boost::geometry::index;

typedef bg::model::point<int, 2, bg::cs::cartesian> point;
typedef bg::model::box<point> box;
typedef boost::geometry::model::polygon<point, false, false > Polygon;


class ChebyshevIndexBase {
protected:
	struct lowOrdrPolynomials {
	public:
		float x0, x1, y0, y1, h0, h1, w0, w1, dx, dy, dh, dw;
	};

	typedef boost::tuple<box, ChebyshevPoly*, long> value;


	vector<bgi::rtree< value, bgi::rstar<16, 4> >*>* trees;
	bt::time_period* globalPeriod = NULL;
	bt::time_duration treeSpan;

public:

	ChebyshevIndexBase(boost::posix_time::time_duration treeSpan) {
		this->treeSpan = treeSpan;
	}

	~ChebyshevIndexBase() {
		//delete all the trees of the index as we are killing it
		if (globalPeriod != NULL) {
			for (unsigned int i = 0; i < this->trees->size(); i++) {
				bgi::rtree< value, bgi::rstar<16, 4> >* tree = this->trees->at(i);
				delete tree;
			}

			delete trees;
			delete globalPeriod;
		}
	}

	Polygon getFullPGone(ChebyshevPoly* polynomial, float begin, float end, bool withDev) {

		if (withDev) {
			float xBegin = this->functAtT(polynomial->getXCoff(), begin);
			float xEnd = this->functAtT(polynomial->getXCoff(), end);
			float yBegin = this->functAtT(polynomial->getYCoff(), begin);
			float yEnd = this->functAtT(polynomial->getYCoff(), end);
			float wBegin = (functAtT(polynomial->getWidthCoff(), begin) / 2.0);
			float wEnd = (functAtT(polynomial->getWidthCoff(), end) / 2.0);
			float hBegin = (functAtT(polynomial->getHeightCoff(), begin) / 2.0);
			float hEnd = (functAtT(polynomial->getHeightCoff(), end) / 2.0);

			float beginX = xBegin - polynomial->getXDev() - polynomial->getWidthDev() - wBegin;
			float minYBegin = yBegin - polynomial->getYDev() - polynomial->getHeightDev() - hBegin;
			float maxYBegin = yBegin + polynomial->getYDev() + polynomial->getHeightDev() + hBegin;


			point ll(beginX, minYBegin);
			point ul(beginX, maxYBegin);

			float endX = xEnd + polynomial->getXDev() + polynomial->getWidthDev() + wEnd;
			float minYEnd = yEnd - polynomial->getYDev() - polynomial->getHeightDev() - hEnd;
			float maxYEnd = yEnd + polynomial->getYDev() + polynomial->getHeightDev() + hEnd;

			point lr(endX, minYEnd);
			point ur(endX, maxYEnd);

			Polygon p;
			p.outer().push_back(point(beginX, maxYBegin));
			p.outer().push_back(point(beginX, minYBegin));
			p.outer().push_back(point(endX, minYEnd));
			p.outer().push_back(point(endX, maxYEnd));

			return p;
		}
		else {
			float xBegin = this->functAtT(polynomial->getXCoff(), begin);
			float xEnd = this->functAtT(polynomial->getXCoff(), end);
			float yBegin = this->functAtT(polynomial->getYCoff(), begin);
			float yEnd = this->functAtT(polynomial->getYCoff(), end);
			float wBegin = (functAtT(polynomial->getWidthCoff(), begin) / 2.0);
			float wEnd = (functAtT(polynomial->getWidthCoff(), end) / 2.0);
			float hBegin = (functAtT(polynomial->getHeightCoff(), begin) / 2.0);
			float hEnd = (functAtT(polynomial->getHeightCoff(), end) / 2.0);

			float beginX = xBegin - wBegin;
			float minYBegin = yBegin - hBegin;
			float maxYBegin = yBegin + hBegin;


			point ll(beginX, minYBegin);
			point ul(beginX, maxYBegin);

			float endX = xEnd + wEnd;
			float minYEnd = yEnd - hEnd;
			float maxYEnd = yEnd + hEnd;

			point lr(endX, minYEnd);
			point ur(endX, maxYEnd);

			Polygon p;
			p.outer().push_back(point(beginX, maxYBegin));
			p.outer().push_back(point(beginX, minYBegin));
			p.outer().push_back(point(endX, minYEnd));
			p.outer().push_back(point(endX, maxYEnd));

			return p;
		}

	}

	float functAtT(vector<float>* coefficients, float t) {
		float val = 0.0;
		float tm_2 = 1;
		float tm_1 = t;

		val = coefficients->at(0);
		if (coefficients->size() >= 1) {
			val = coefficients->at(0);
		}

		if (coefficients->size() >= 2){
			val += t*coefficients->at(1);
		}

		if (coefficients->size() < 3) {
			return val;
		}
		for (unsigned int i = 2; i < coefficients->size(); i++) {
			float tm = 2.0*t*tm_1 - tm_2;
			val += coefficients->at(i)*tm;

			tm_2 = tm_1;
			tm_1 = tm;
		}
		return val;
	}

	Polygon getLinearPGone(lowOrdrPolynomials polys, float begin, float end, bool withDev) {

		if (withDev) {
			float minX = polys.x0 + std::min(polys.x1*begin, polys.x1*end) - polys.dx - polys.dw - ((polys.w0 + std::min(polys.w1*begin, polys.w1*end)) / 2.0);
			float maxX = polys.x0 + std::max(polys.x1*end, polys.x1*begin) + polys.dx + polys.dw + ((polys.w0 + std::max(polys.w1*end, polys.w1*begin)) / 2.0);

			float minY = polys.y0 + std::min(polys.y1*begin, polys.y1*end);
			float minH = (polys.h0 + std::min(polys.h1*begin, polys.h1*end)) / 2.0;
			float ulY = minY + polys.dy + polys.dh + minH;
			float llY = minY - polys.dy - polys.dh - minH;


			float maxY = polys.y0 + std::max(polys.y1*end, polys.y1*begin);
			float maxH = (polys.h0 + std::max(polys.h1*end, polys.h1*begin)) / 2.0;
			float urY = maxY + polys.dy + polys.dh + maxH;
			float lrY = maxY - polys.dy - polys.dh - maxH;

			Polygon p;
			p.outer().push_back(point(minX, ulY));
			p.outer().push_back(point(minX, llY));
			p.outer().push_back(point(maxX, lrY));
			p.outer().push_back(point(maxX, urY));

			return p;
		}
		else {
			float minX = polys.x0 + std::min(polys.x1*begin, polys.x1*end) - ((polys.w0 + std::min(polys.w1*begin, polys.w1*end)) / 2.0);
			float maxX = polys.x0 + std::max(polys.x1*end, polys.x1*begin) + ((polys.w0 + std::max(polys.w1*end, polys.w1*begin)) / 2.0);

			float minY = polys.y0 + std::min(polys.y1*begin, polys.y1*end);
			float minH = (polys.h0 + std::min(polys.h1*begin, polys.h1*end)) / 2.0;
			float ulY = minY + minH;
			float llY = minY - minH;


			float maxY = polys.y0 + std::max(polys.y1*end, polys.y1*begin);
			float maxH = (polys.h0 + std::max(polys.h1*end, polys.h1*begin)) / 2.0;
			float urY = maxY + maxH;
			float lrY = maxY - maxH;

			Polygon p;
			p.outer().push_back(point(minX, ulY));
			p.outer().push_back(point(minX, llY));
			p.outer().push_back(point(maxX, lrY));
			p.outer().push_back(point(maxX, urY));

			return p;
		}
	}

	vector<int> getIdx(bt::time_period period) {

		//if not init then do so
		if (this->globalPeriod == NULL) {
			int length = (int)period.length().total_seconds() / this->treeSpan.total_seconds();
			bt::ptime end = period.begin() + this->treeSpan * length + this->treeSpan;
			this->globalPeriod = new bt::time_period(period.begin(), end);

			this->trees = new vector<bgi::rtree< value, bgi::rstar<16, 4> >*>();
			this->addTrees(length + 2);
		}

		// if this period starts before our current time period we will need to widen the period
		if (this->globalPeriod->begin() > period.begin()) {

			//get the duration of the difference between the original start period and the new one
			//that is needed to accomidate this new time period as well.
			bt::time_duration diff = this->globalPeriod->begin() - period.begin();

			//find the the number of periods this difference covers and then expand our global period by that + 1
			int length = (int)diff.total_seconds() / this->treeSpan.total_seconds();
			bt::ptime start = this->globalPeriod->begin() - bt::seconds(this->treeSpan.total_seconds()*length) - bt::seconds(this->treeSpan.total_seconds());

			bt::time_period* oldPeriod = this->globalPeriod;
			this->globalPeriod = new bt::time_period(start, oldPeriod->end());
			delete oldPeriod;

			this->addTrees(length + 1, true);
		}

		//if this period ends after our current time period we will need to widen the period
		if (this->globalPeriod->end() < period.end()) {

			//get the duration of the difference between the original end period and the new one
			//that is needed to accomidate this new time period as well.
			bt::time_duration diff = period.end() - this->globalPeriod->end();

			int length = (int)diff.total_seconds() / this->treeSpan.total_seconds();
			bt::ptime end = this->globalPeriod->end() + bt::seconds(this->treeSpan.total_seconds() * length) + bt::seconds(this->treeSpan.total_seconds());

			bt::time_period* oldPeriod = this->globalPeriod;
			this->globalPeriod = new bt::time_period(oldPeriod->begin(), end);
			this->addTrees(length + 1);
			delete oldPeriod;
		}


		vector<int> returnVect;
		bt::time_duration diff = period.begin() - this->globalPeriod->begin() + bt::seconds(1);
		int beginIdx = (int)diff.total_seconds() / this->treeSpan.total_seconds();
		int endIdx = beginIdx + (int)period.length().total_seconds() / this->treeSpan.total_seconds();
		for (int i = beginIdx; i <= endIdx; i++) {
			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*i));
			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));
			if (period.intersects(indexPeriod)){
				returnVect.push_back(i);
			}
		}

		return returnVect;
	}

	/*
	This method assumes that this->trees is already created
	do not call it until the vector is created
	*/
	void addTrees(int num, bool front = false) {

		if (front) {

			vector<bgi::rtree< value, bgi::rstar<16, 4> >*>* oldTrees = this->trees;

			this->trees = new vector<bgi::rtree< value, bgi::rstar<16, 4> >*>();
			//bt::ptime end = start + this->treeSpan;
			for (int i = 0; i < num; i++) {
				//PATree pa;
				//pa.period = new bt::time_period(start, end);
				//pa.tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
				bgi::rtree< value, bgi::rstar<16, 4> >* tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
				//start = end;
				//end += this->treeSpan;
				this->trees->push_back(tree);
			}

			for (unsigned int i = 0; i < oldTrees->size(); i++) {
				this->trees->push_back(oldTrees->at(i));
			}
			delete oldTrees;
		}
		else {
			//bt::ptime end = start + this->treeSpan;
			for (int i = 0; i < num; i++) {
				//PATree pa;
				//pa.period = new bt::time_period(start, end);
				//pa.tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
				bgi::rtree< value, bgi::rstar<16, 4> >* tree = new bgi::rtree< value, bgi::rstar<16, 4> >();
				//start = end;
				//end += this->treeSpan;
				this->trees->push_back(tree);
			}
		}
	}
};
#endif /*CHEBYSHEVINDEXBASE_HPP_*/
