
#ifndef CHEBYSHEVPOLY_HPP
#define	CHEBYSHEVPOLY_HPP

#include <vector>

class ChebyshevPoly {
	std::vector<float>* xCoff = NULL;
	std::vector<float>* yCoff = NULL;
	float xDev;
	float yDev;

	std::vector<float>* heightCoff = NULL;
	std::vector<float>* widthCoff = NULL;
	float heightDev;
	float widthDev;
	float validIntervalBegin;
	float validIntervalEnd;
	bool valid;

public:
	ChebyshevPoly(std::vector<float>* _xCoff, std::vector<float>* _yCoff, std::vector<float>* _heightCoff, std::vector<float>* _widthCoff,
		float _xDev, float _yDev, float _heightDev, float _widthDev, float _validIntervalBegin, float _validIntervalEnd, bool _valid) {
		xCoff = _xCoff;
		yCoff = _yCoff;
		heightCoff = _heightCoff;
		widthCoff = _widthCoff;
		xDev = _xDev;
		yDev = _yDev;
		heightDev = _heightDev;
		widthDev = _widthDev;
		validIntervalBegin = _validIntervalBegin;
		validIntervalEnd = _validIntervalEnd;
		valid = _valid;
	};

	ChebyshevPoly(bool _valid) {
		valid = _valid;
	};

	~ChebyshevPoly() {
		if (!(this->xCoff == NULL)) {
			delete this->xCoff;
		}

		if (!(this->yCoff == NULL)) {
			delete this->yCoff;
		}

		if (!(this->heightCoff == NULL)) {
			delete this->heightCoff;
		}

		if (!(this->widthCoff == NULL)) {
			delete this->widthCoff;
		}
	};

	bool isValid() {
		return valid;
	}

	std::vector<float>* getXCoff() {
		return xCoff;
	};
	std::vector<float>* getYCoff() {
		return yCoff;
	};
	std::vector<float>* getHeightCoff() {
		return heightCoff;
	};
	std::vector<float>* getWidthCoff() {
		return widthCoff;
	};
	float getXDev() {
		return xDev;
	};
	float getYDev() {
		return yDev;
	};
	float getHeightDev() {
		return heightDev;
	};
	float getWidthDev() {
		return widthDev;
	};
	float getValidIntervalBegin() {
		return validIntervalBegin;
	};
	float getValidIntervalEnd() {
		return validIntervalEnd;
	};
};
#endif
