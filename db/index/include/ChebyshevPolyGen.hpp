#ifndef CHEBSHEVPOLYGEN_HPP
#define	CHEBSHEVPOLYGEN_HPP

#include <vector>
#include <math.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include "db/index/include/ChebyshevPoly.hpp"



using namespace boost::posix_time;
namespace bg = boost::geometry;

typedef bg::model::point<int, 2, bg::cs::cartesian> point;
typedef bg::model::polygon<point, false, false > Polygon;
typedef bg::model::box<point> box;

class ChebyshevPolyGen {
	double fraction;
	long cadencePeriod;

	float calculatePofT(int i, float t){
		if (i == 0){
			//printf("1 \n");
			return 1.0f;
		}
		else if (i == 1){
			//printf("%f \n",t);
			return t;
		}
		else{
			float temp1 = 1.0f;
			float temp2 = t;
			for (int j = 1; j < i; j++){
				float temp = (2 * t*temp2) - (temp1);
				temp1 = temp2;
				temp2 = temp;
			}
			//printf("%f \n",temp2);
			return temp2;
		}
	}

public:
	ChebyshevPolyGen(double fraction, long cadencePeriod) {
		this->fraction = fraction;
		this->cadencePeriod = cadencePeriod;
	}

	/* headEv - ist instance of the event that falls in the time period(not the head of the event)
	** timePreiod - period over which we are computing the chebyShev polynomial
	** iTrackTimePeriod - time period of the track
	** fraction - fraction of the total number of points used as degree
	*/
	//check to see if the ChebShevCoff is valid by using the isValid() function.
	ChebyshevPoly* getPoly(vector<Polygon>::iterator headEvItr, vector<Polygon>::iterator endOfVect, boost::posix_time::time_period timePeriod, boost::posix_time::time_period iTrackTimePeriod) {

		time_duration length = timePeriod.length();
		long total_seconds = length.total_seconds();
		bool intersection = timePeriod.intersects(iTrackTimePeriod);
		time_period validTimePeriod = timePeriod.intersection(iTrackTimePeriod);


		vector<EventValues> eventValues;//contains entire set of instances
		vector<EventValues> eventValues2;//contains set of instances used for computing coefficients


		long validTimePeriodBegin, validTimePeriodEnd;
		//if period of track passed in and index period intersect
		if (intersection) {

			//if the intersection period and the index period do not start at the same time
			if (!(time_period(timePeriod.begin(), validTimePeriod.begin())).is_null()) {
				//number of seconds from beginning of index time period to the beginning of the intersecting time period.
				validTimePeriodBegin = (time_period(timePeriod.begin(), validTimePeriod.begin())).length().total_seconds();
			}
			else {
				validTimePeriodBegin = 0;//have the same begin
			}

			//number of seconds from beginning of index time period to the end of the intersect time period.
			validTimePeriodEnd = (time_period(timePeriod.begin(), validTimePeriod.end())).length().total_seconds();

			/*calculating the normalized values of the valid inteval*/
			double normalValidBegin = (((double)(2.0 * (validTimePeriodBegin - 0))) / total_seconds) - 1;
			double normalValidEnd = (((double)(2.0 * (validTimePeriodEnd - 0))) / total_seconds) - 1;
			long totalSeconds;
			int eventLength = 0;

			/*calculating t and f(t) for the event within the time period*/
			time_period currentEventTimePeriod(validTimePeriod.begin(), validTimePeriod.begin() + seconds(cadencePeriod));
			for (int i = 0; (headEvItr != endOfVect) && currentEventTimePeriod.intersects(timePeriod);) {

				if (i == 0) {
					if (!(time_period(timePeriod.begin(), currentEventTimePeriod.begin()).is_null())) {
						totalSeconds = (time_period(timePeriod.begin(), (currentEventTimePeriod).begin())).length().total_seconds();
					}
					else {
						totalSeconds = 0;
					}
				}
				else {
					totalSeconds = (time_period(timePeriod.begin(), (currentEventTimePeriod).begin())).length().total_seconds();
				}
				Polygon p = headEvItr.operator *();

				//if the polygon is empty we cannot calculate anyting on it
				if (!p.outer().size()){
					//cout << "empty" << endl;
					headEvItr++;
					currentEventTimePeriod = time_period(currentEventTimePeriod.end(), seconds(cadencePeriod));
					i++;
					continue;
				}
				box b = bg::return_envelope<box>(p);

				point minCorner = b.min_corner();
				point maxCorner = b.max_corner();

				int width = (int)(maxCorner.get<0>() - minCorner.get<0>());
				int height = (int)(maxCorner.get<1>() - minCorner.get<1>());
				float midx = minCorner.get<0>() + (width / 2.0);
				float midy = minCorner.get<1>() + (height / 2.0);

				float t = (((float)(2.0 * (totalSeconds - 0))) / total_seconds) - 1;

				eventValues.push_back(EventValues(midx, midy, height, width, t));
				headEvItr++;
				i++;
				eventLength++;
				currentEventTimePeriod = time_period(currentEventTimePeriod.end(), seconds(cadencePeriod));
			}

			/*Computing events or instances used for calculating coefficients*/
			int degree = (int)((double)eventLength * fraction);

			double degree2 = ((double)eventLength * fraction);
			//printf(" degree: %d degree2: %f ", degree, degree2);
			int increment;

			int noOfElementsNeeded = (degree + 2) - 2;

			if (noOfElementsNeeded > 0){
				increment = ((eventLength - 2) / (noOfElementsNeeded));
			}
			else{
				increment = 0;
			}

	
			// if the multi-polygons were empty!!
			if (!eventValues.size()) return new ChebyshevPoly(false);
			
			//else we may proceed
			eventValues2.push_back(eventValues[0]);
			//printf("selected: %d ", 0 );

			for (int i = 0, j = increment; (i < noOfElementsNeeded - 1) && (j < eventLength); i++, j = j + increment){
				eventValues2.push_back(eventValues[j]);
				//printf("%d ", j);
			}

			if (eventLength > 1){
				if (degree < 1){
					degree = 1;
				}
				eventValues2.push_back(eventValues[eventLength - 1]);
				//printf("%d ",eventLength - 1 );
			}
			//printf("\n");

			/*calculating coefficients*/
			vector<float>* coeffX = new vector<float>();//for mid-point x

			for (int i = 0; i <= degree; i++) {
				float tempCoef = 0;

				for (int j = 1; j <= (degree + 1); j++) {
					if (i == 0) {
						tempCoef = tempCoef + eventValues2[j - 1].midX;
					}
					else {
						//tempCoef = tempCoef + eventValues2[j - 1].midX * cos((j - 1)*acos(eventValues2[j - 1].t));
						tempCoef = tempCoef + eventValues2[j - 1].midX * calculatePofT(i, eventValues2[j - 1].t);
					}
				}

				if (i == 0) {
					tempCoef = tempCoef / (degree + 1);
					coeffX->push_back(tempCoef);
				}
				else {
					tempCoef = (tempCoef * 2) / (degree + 1);
					coeffX->push_back(tempCoef);
				}
			}

			/*calculating deviance for mid-point x*/
			float maxDevX = 0;

			for (int i = 0; i < eventLength; i++) {
				float tempVal = 0;
				for (int j = 0; j <= degree; j++) {
					//tempVal = tempVal + coeffX->at(j) * cos((j)*acos(eventValues[i].t));
					tempVal = tempVal + coeffX->at(j) * calculatePofT(j, eventValues[i].t);
				}
				float diff = eventValues[i].midX - tempVal;
				if (diff < 0) {
					diff = diff *(-1);
				}
				if (diff > maxDevX) {
					maxDevX = diff;
				}
			}


			/*calculating coefficients*/
			vector<float>* coeffY = new vector<float>();//for mid-point y

			for (int i = 0; i <= degree; i++) {
				float tempCoef = 0;

				for (int j = 1; j <= (degree + 1); j++) {
					if (i == 0) {
						tempCoef = tempCoef + eventValues2[j - 1].midY;
					}
					else {
						//tempCoef = tempCoef + eventValues2[j - 1].midY * cos((j - 1)*acos(eventValues2[j - 1].t));
						tempCoef = tempCoef + eventValues2[j - 1].midY * calculatePofT(i, eventValues2[j - 1].t);
					}
				}

				if (i == 0) {
					tempCoef = tempCoef / (degree + 1);
					coeffY->push_back(tempCoef);
				}
				else {
					tempCoef = (tempCoef * 2) / (degree + 1);
					coeffY->push_back(tempCoef);
				}
			}

			/*calculating deviance for mid-point y*/
			float maxDevY = 0;

			for (int i = 0; i < eventLength; i++) {
				float tempVal = 0;
				for (int j = 0; j <= degree; j++) {
					//tempVal = tempVal + coeffY->at(j) * cos((j)*acos(eventValues[i].t));
					tempVal = tempVal + coeffY->at(j) * calculatePofT(j, eventValues[i].t);
				}
				float diff = eventValues[i].midY - tempVal;
				if (diff < 0) {
					diff = diff *(-1);
				}
				if (diff > maxDevY) {
					maxDevY = diff;
				}
			}

			/*calculating coefficients for height*/
			vector<float>* coeffH = new vector<float>();//for height

			for (int i = 0; i <= degree; i++) {
				float tempCoef = 0;

				for (int j = 1; j <= (degree + 1); j++) {
					if (i == 0) {
						tempCoef = tempCoef + eventValues2[j - 1].height;
					}
					else {
						//tempCoef = tempCoef + eventValues2[j - 1].height * cos((j - 1)*acos(eventValues2[j - 1].t));
						tempCoef = tempCoef + eventValues2[j - 1].height * calculatePofT(i, eventValues2[j - 1].t);
					}
				}

				if (i == 0) {
					tempCoef = tempCoef / (degree + 1);
					coeffH->push_back(tempCoef);
				}
				else {
					tempCoef = (tempCoef * 2) / (degree + 1);
					coeffH->push_back(tempCoef);
				}
			}

			/*calculating deviance for height*/
			float maxDevH = 0;

			for (int i = 0; i < eventLength; i++) {
				float tempVal = 0;
				for (int j = 0; j <= degree; j++) {
					//tempVal = tempVal + coeffH->at(j) * cos((j)*acos(eventValues[i].t));
					tempVal = tempVal + coeffH->at(j) * calculatePofT(j, eventValues[i].t);
				}
				float diff = eventValues[i].height - tempVal;
				if (diff < 0) {
					diff = diff *(-1);
				}
				if (diff > maxDevH) {
					maxDevH = diff;
				}
			}

			/*calculating coefficients for width*/
			vector<float>* coeffW = new vector<float>();//for width

			for (int i = 0; i <= degree; i++) {
				float tempCoef = 0;

				for (int j = 1; j <= (degree + 1); j++) {
					if (i == 0) {
						tempCoef = tempCoef + eventValues2[j - 1].width;
					}
					else {
						//tempCoef = tempCoef + eventValues2[j - 1].width * cos((j - 1)*acos(eventValues2[j - 1].t));
						tempCoef = tempCoef + eventValues2[j - 1].width * calculatePofT(i, eventValues2[j - 1].t);
					}
				}

				if (i == 0) {
					tempCoef = tempCoef / (degree + 1);
					coeffW->push_back(tempCoef);
				}
				else {
					tempCoef = (tempCoef * 2) / (degree + 1);
					coeffW->push_back(tempCoef);
				}
			}

			/*calculating deviance for width*/
			float maxDevW = 0;

			for (int i = 0; i < eventLength; i++) {
				float tempVal = 0;
				for (int j = 0; j <= degree; j++) {
					//tempVal = tempVal + coeffW->at(j) * cos((j)*acos(eventValues[i].t));
					tempVal = tempVal + coeffW->at(j) * calculatePofT(j, eventValues[i].t);
				}
				float diff = eventValues[i].width - tempVal;
				if (diff < 0) {
					diff = diff *(-1);
				}
				if (diff > maxDevW) {
					maxDevW = diff;
				}
			}

			return new ChebyshevPoly(coeffX, coeffY, coeffH, coeffW,
				maxDevX, maxDevY, maxDevH, maxDevW, normalValidBegin, normalValidEnd, true);

		}
		else {
			return new ChebyshevPoly(false);
		}
	}
	class EventValues {
	public:
		double midX;
		double midY;
		int height;
		int width;
		double t;
		EventValues(double _midX, double _midY, int _height, int _width, double _t) {
			midX = _midX;
			midY = _midY;
			height = _height;
			width = _width;
			t = _t;
		}
	};
};

#endif
