/*
 * SETI_Grid.hpp
 *
 *  Created on: Jun 9, 2014
 *      Author: berkay
 */

#ifndef SETI_GRID_HPP_
#define SETI_GRID_HPP_

#include <utility>
#include <vector>
#include <unordered_set>
#include <boost/geometry/index/parameters.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/unordered_map.hpp>


typedef bg::model::point<int, 2, bg::cs::cartesian> point;
typedef bg::model::point<long, 2, bg::cs::cartesian> time_point;
typedef bg::model::box<point> box;
typedef bg::model::box<time_point> time_box;

typedef boost::geometry::model::polygon<point, false, false > Polygon;
typedef std::pair<time_point, long> value;
typedef bgi::rtree< value, bgi::rstar<16> > seti_rtree;

namespace bgi = boost::geometry::index;
using namespace std;

class SETI_Grid {
public:

	SETI_Grid(double dimX, double dimY, int xCell, int yCell) {
		maxTime = 0;
		this->dimX = dimX;
		this->dimY = dimY;
		this->xCell = xCell;
		this->yCell = yCell;

		for(int i = 0; i < xCell; i++) {
			vector< seti_rtree* > temp;
			vector< box > temp2;
			grid_rtrees.push_back(temp);
			cells.push_back(temp2);
			for (int j = 0; j < yCell; j++) {
				seti_rtree* tmp_tree = new seti_rtree;
				grid_rtrees.at(i).push_back(tmp_tree);
				point upperLeft((dimX / xCell) * i, (dimY / yCell) * j);
				point lowerRight((dimX / xCell) * (i+1), (dimY / yCell) * (j+1));
				box cell_ij( upperLeft, lowerRight );
				cells.at(i).push_back(cell_ij);
			}
		}
	}

	~SETI_Grid() {
		for(unsigned int i = 0; i < grid_rtrees.size(); i++){
			for (unsigned int j = 0; j < grid_rtrees.at(i).size(); j++){
				delete grid_rtrees.at(i).at(j);
			}
		}
	}

	void p_insert(PatternInstance* pInst, long instanceLocation);
	vector<long>* p_ST_Intersection(PatternInstance* pInst);


	void insert(Instance* inst, long instanceLocation);
	vector<int>* findIntersectingCells(Polygon p);
	vector<int>* findIntersectingCells_MP(vector<Polygon> p);
    		vector<long>* ST_Intersection(Instance* inst);

private:
	double dimX;
	double dimY;
	int xCell;
	int yCell;

	long maxTime;

	vector< vector<box> > cells;
	vector< vector< seti_rtree* > > grid_rtrees;
	boost::unordered_map<int, vector<int> > frontLine; //hash for instanceID -> cell number in grid

};



inline vector<int>* SETI_Grid::findIntersectingCells_MP(vector<Polygon> polyVector) {
	vector<int>* intersectingCells = new vector<int>();
	unordered_set<int> intersectingCellSet;

	for(unsigned int i; i < polyVector.size(); i++){
		Polygon p = polyVector.at(i);
		box mbr;
		envelope(p, mbr);

		point maxcorner = mbr.max_corner();
		point mincorner = mbr.min_corner();

		float mincorner_x = mincorner.get<0>();
		float mincorner_y = mincorner.get<1>();
		float maxcorner_x = maxcorner.get<0>();
		float maxcorner_y = maxcorner.get<1>();

		if(mincorner_x == 0.0 && mincorner_y == 0.0 && maxcorner_x == 0.0 && maxcorner_y == 0.0 ){
			break;
		}

		double xlength = dimX / xCell;
		double ylength = dimY / yCell;

		int minBox_x = (int) (mincorner_x / xlength );
		int minBox_y = (int) (mincorner_y / ylength );
		int maxBox_x = (int) (maxcorner_x / xlength );
		int maxBox_y = (int) (maxcorner_y / ylength );

		for(int i = minBox_x; i <= maxBox_x; i++) {
			for(int j = minBox_y; j <= maxBox_y; j++) {
				if( boost::geometry::intersects (cells.at(i).at(j), p)  ) {
					int cell = xCell*j + i;
					intersectingCellSet.insert(cell);
				}
			}
		}
	}
	intersectingCells->assign(intersectingCellSet.begin(), intersectingCellSet.end());

	return intersectingCells;
}


inline void SETI_Grid::p_insert(PatternInstance* inst, long instanceLocation){
	int sTime = inst->getStartTime();
	int eTime = inst->getEndTime();
	if(eTime > maxTime) {
		maxTime = eTime;
	}
	vector<vector<Polygon> > geometries = inst->intersectionGeometries;
	vector<int>* prevIntersectingCells = findIntersectingCells_MP(geometries.at(0) );

	vector<std::pair<int, time_point> > frontLine; // int - cellId, time_point - traj duration

	time_point trajDuration(sTime, sTime);	//first trajectory duration is startTime startTime,
	// but we will change the endTime of traj later.

	//for each intersecting cell add the first trajectory segment
	for (unsigned int i = 0; i < prevIntersectingCells->size(); i++) {
		std::pair<int, time_point> entry;
		entry = std::make_pair(prevIntersectingCells->at(i), trajDuration);
		frontLine.push_back(entry);
		//frontLineTimeRanges.at(0).set<1>(eTime);
	}
	// iterate from the second geometry until last
	// for each geometry check if we need to include another geometry
	for (unsigned int i = 1; i < geometries.size(); i++) {

		//find the intersecting cells of current polygon
		vector<int>* currentIntersectingCells = findIntersectingCells_MP(geometries.at(i));

		std::sort(currentIntersectingCells->begin(), currentIntersectingCells->end());

		//check it with what we have on frontline
		for (unsigned int cellIndex = 0; cellIndex < currentIntersectingCells->size(); cellIndex++) {
			int cellId = currentIntersectingCells->at(cellIndex);
			bool cellIdFound = false;
			unsigned int fl_cellIndex = 0;

			for (; fl_cellIndex < frontLine.size(); fl_cellIndex++) {
				if (cellId == frontLine.at(fl_cellIndex).first) { // then change the trajectory
					//then we found our old cell id
					//we need to change the trajectory
					frontLine.at(fl_cellIndex).second.set<1>(sTime + i);
					cellIdFound = true;
					break;
				}
			}
			if (!cellIdFound) {
				//new cell id, new trajectory
				//create time duration and cellId, then add the entry to front line
				time_point newTrajDuration(sTime + i, sTime + i);
				std::pair<int, time_point> entry;
				entry = std::make_pair(currentIntersectingCells->at(cellIndex), newTrajDuration);
				frontLine.push_back(entry);

			} //else {// we have already done that above}
		}

		//check frontLine and add the ending trajectories to index
		for (unsigned int cellIndex = 0; cellIndex < frontLine.size(); cellIndex++) {
			int cellId = frontLine.at(cellIndex).first;
			bool cellIdPresent = false;
			unsigned int cur_cellIndex = 0;

			for (; cur_cellIndex < currentIntersectingCells->size(); cur_cellIndex++) {
				if (cellId == currentIntersectingCells->at(cur_cellIndex)) {
					//if cell id was already there, we have handled it earlier
					cellIdPresent = true;
					break;
				}
			}
			if (!cellIdPresent) {	// cell id is no longer in the current intersections
				// therefore we need to add it to index (rtrees)
				// then remove the trajectory segment from frontline

				//a) add trajectory segment to rtree index
				value trajSegment = std::make_pair(frontLine.at(cellIndex).second, instanceLocation);

				int cellCoordinate_x = cellId % xCell;
				int cellCoordinate_y = cellId / xCell;
				grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->insert(trajSegment);

				//b) remove the entry from segment, update cell index
				frontLine.erase (frontLine.begin()+cellIndex);
				cellIndex--;
			} //else {// we have already done that above}
		}
		delete currentIntersectingCells;
	}
	//in the end flush the front line and add everything to index
	value trajSegment;
	for(unsigned int i = 0; i < frontLine.size(); i++) {
		trajSegment = std::make_pair(frontLine.at(i).second, instanceLocation);
		int cellCoordinate_x = frontLine.at(i).first % xCell;
		int cellCoordinate_y = frontLine.at(i).first / xCell;
		grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->insert(trajSegment);
	}
	frontLine.clear();
	delete prevIntersectingCells;
}
inline vector<long>* SETI_Grid::p_ST_Intersection(PatternInstance* inst) {
	vector<long>* results = new vector<long>();

	std::unordered_set<long> resultSet;
	std::vector<value> tempResultVector;


	int sTime = inst->getStartTime();
	//int eTime = inst->getEndTime();

	vector<vector<Polygon> > geometries = inst->intersectionGeometries;
	vector<int>* prevIntersectingCells = findIntersectingCells_MP(geometries.at(0));

	vector<std::pair<int, time_point> > frontLine; // int - cellId, time_point - traj duration

	time_point trajDuration(sTime, sTime);	//first trajectory duration is startTime startTime,
	// but we will change the endTime of traj later.

	//for each intersecting cell add the first trajectory segment
	for (unsigned int i = 0; i < prevIntersectingCells->size(); i++) {
		std::pair<int, time_point> entry;
		entry = std::make_pair(prevIntersectingCells->at(i), trajDuration);
		frontLine.push_back(entry);
		//frontLineTimeRanges.at(0).set<1>(eTime);
	}
	//size_t gsize = geometries.size();
	// iterate from the second geometry until last
	// for each geometry check if we need to include another geometry
	for (unsigned int i = 1; i < geometries.size(); i++) {

		//find the intersecting cells of current polygon
		vector<int>* currentIntersectingCells = findIntersectingCells_MP(geometries.at(i));

		std::sort(currentIntersectingCells->begin(), currentIntersectingCells->end());

		//check it with what we have on frontline
		for (unsigned int cellIndex = 0; cellIndex < currentIntersectingCells->size(); cellIndex++) {
			int cellId = currentIntersectingCells->at(cellIndex);
			bool cellIdFound = false;
			unsigned int fl_cellIndex = 0;

			for (; fl_cellIndex < frontLine.size(); fl_cellIndex++) {
				if (cellId == frontLine.at(fl_cellIndex).first) { // then change the trajectory
					//then we found our old cell id
					//we need to change the trajectory
					frontLine.at(fl_cellIndex).second.set<1>(sTime + i);
					cellIdFound = true;
					break;
				}
			}
			if (!cellIdFound) {
				//new cell id, new trajectory
				//create time duration and cellId, then add the entry to front line
				time_point newTrajDuration(sTime + i, sTime + i);
				std::pair<int, time_point> entry;
				entry = std::make_pair(currentIntersectingCells->at(cellIndex), newTrajDuration);
				frontLine.push_back(entry);

			} //else {// we have already done that above}
		}
		//size_t fsize = frontLine.size();
		//check frontLine and add the ending trajectories to index
		for (unsigned int cellIndex = 0; cellIndex < frontLine.size(); cellIndex++) {
			int cellId = frontLine.at(cellIndex).first;
			bool cellIdPresent = false;
			unsigned int cur_cellIndex = 0;

			for (; cur_cellIndex < currentIntersectingCells->size(); cur_cellIndex++) {
				if (cellId == currentIntersectingCells->at(cur_cellIndex)) {
					//if cell id was already there, we have handled it earlier
					cellIdPresent = true;
					break;
				}
			}
			if (!cellIdPresent) {	// cell id is no longer in the current intersections
				// therefore we need to add it to index (rtrees)
				// then remove the trajectory segment from frontline

				int cellCoordinate_x = cellId % xCell;
				int cellCoordinate_y = cellId / xCell;
				time_point minc(0, frontLine.at(cellIndex).second.get<0>());
				time_point maxc(frontLine.at(cellIndex).second.get<1>(), maxTime);
				time_box queryTimeBox(minc, maxc);

				grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->query( bgi::intersects(queryTimeBox), std::back_inserter(tempResultVector));


				//b) remove the entry from segment, update cell index
				frontLine.erase (frontLine.begin()+cellIndex);
				cellIndex--;
			} //else {// we have already done that above}
		}
		delete currentIntersectingCells;
	}
	//in the end flush the front line and add everything to index
	value trajSegment;
	for(unsigned int i = 0; i < frontLine.size(); i++) {
		int cellCoordinate_x = frontLine.at(i).first % xCell;
		int cellCoordinate_y = frontLine.at(i).first / xCell;
		time_point minc(0, frontLine.at(i).second.get<0>());
		time_point maxc(frontLine.at(i).second.get<1>(), maxTime);
		time_box queryTimeBox(minc, maxc);

		grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->query( bgi::intersects(queryTimeBox), std::back_inserter(tempResultVector));
	}
	frontLine.clear();

	for(unsigned int i = 0; i < tempResultVector.size(); i++ ) {
		resultSet.insert( tempResultVector.at(i).second );
	}
	results->assign(resultSet.begin(), resultSet.end());

	return results;
}

inline vector<long>* SETI_Grid::ST_Intersection(Instance* inst) {
	vector<long>* results = new vector<long>();

	std::unordered_set<long> resultSet;
	std::vector<value> tempResultVector;


	int sTime = inst->getStartTime();
	//int eTime = inst->getEndTime();

	vector<Polygon> geometries = inst->getPolygons();
	vector<int>* prevIntersectingCells = findIntersectingCells(geometries.at(0));

	vector<std::pair<int, time_point> > frontLine; // int - cellId, time_point - traj duration

	time_point trajDuration(sTime, sTime);	//first trajectory duration is startTime startTime,
	// but we will change the endTime of traj later.

	//for each intersecting cell add the first trajectory segment
	for (unsigned int i = 0; i < prevIntersectingCells->size(); i++) {
		std::pair<int, time_point> entry;
		entry = std::make_pair(prevIntersectingCells->at(i), trajDuration);
		frontLine.push_back(entry);
		//frontLineTimeRanges.at(0).set<1>(eTime);
	}

	// iterate from the second geometry until last
	// for each geometry check if we need to include another geometry
	for (unsigned int i = 1; i < geometries.size(); i++) {

		//find the intersecting cells of current polygon
		vector<int>* currentIntersectingCells = findIntersectingCells(inst->getPolygon(i));

		std::sort(currentIntersectingCells->begin(), currentIntersectingCells->end());

		//check it with what we have on frontline
		for (unsigned int cellIndex = 0; cellIndex < currentIntersectingCells->size(); cellIndex++) {
			int cellId = currentIntersectingCells->at(cellIndex);
			bool cellIdFound = false;
			unsigned int fl_cellIndex = 0;

			for (; fl_cellIndex < frontLine.size(); fl_cellIndex++) {
				if (cellId == frontLine.at(fl_cellIndex).first) { // then change the trajectory
					//then we found our old cell id
					//we need to change the trajectory
					frontLine.at(fl_cellIndex).second.set<1>(sTime + i);
					cellIdFound = true;
					break;
				}
			}
			if (!cellIdFound) {
				//new cell id, new trajectory
				//create time duration and cellId, then add the entry to front line
				time_point newTrajDuration(sTime + i, sTime + i);
				std::pair<int, time_point> entry;
				entry = std::make_pair(currentIntersectingCells->at(cellIndex), newTrajDuration);
				frontLine.push_back(entry);

			} //else {// we have already done that above}
		}

		//check frontLine and add the ending trajectories to index
		for (unsigned int cellIndex = 0; cellIndex < frontLine.size(); cellIndex++) {
			int cellId = frontLine.at(cellIndex).first;
			bool cellIdPresent = false;
			unsigned int cur_cellIndex = 0;

			for (; cur_cellIndex < currentIntersectingCells->size(); cur_cellIndex++) {
				if (cellId == currentIntersectingCells->at(cur_cellIndex)) {
					//if cell id was already there, we have handled it earlier
					cellIdPresent = true;
					break;
				}
			}
			if (!cellIdPresent) {	// cell id is no longer in the current intersections
				// therefore we need to add it to index (rtrees)
				// then remove the trajectory segment from frontline

				int cellCoordinate_x = cellId % xCell;
				int cellCoordinate_y = cellId / xCell;
				time_point minc(0, frontLine.at(cellIndex).second.get<0>());
				time_point maxc(frontLine.at(cellIndex).second.get<1>(), maxTime);
				time_box queryTimeBox(minc, maxc);

				grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->query( bgi::intersects(queryTimeBox), std::back_inserter(tempResultVector));


				//b) remove the entry from segment, update cell index
				frontLine.erase (frontLine.begin()+cellIndex);
				cellIndex--;
			} //else {// we have already done that above}
		}
	}
	//in the end flush the front line and add everything to index
	value trajSegment;
	for(unsigned int i = 0; i < frontLine.size(); i++) {
		int cellCoordinate_x = frontLine.at(i).first % xCell;
		int cellCoordinate_y = frontLine.at(i).first / xCell;
		time_point minc(0, frontLine.at(i).second.get<0>());
		time_point maxc(frontLine.at(i).second.get<1>(), maxTime);
		time_box queryTimeBox(minc, maxc);

		grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->query( bgi::intersects(queryTimeBox), std::back_inserter(tempResultVector));
	}
	frontLine.clear();

	for(unsigned int i = 0; i < tempResultVector.size(); i++ ) {
		resultSet.insert( tempResultVector.at(i).second );
	}
	results->assign(resultSet.begin(), resultSet.end());

	return results;
}

/* The outline of an example grid and layout of cell numbering is as follows
									X
						0		1		2		4
					---------------------------------
 				0	|	0	|	1	|	2	|	3	|
  					|		|		|		|		|
					---------------------------------
  				1	|	4	|	5	|	6	|	7	|
  					|		|		|		|		|
					---------------------------------
 				2	|	8	|	9	|	10	|	11	|
  	y				|		|		|		|		|
					---------------------------------
  				4	|	12	|	13	|	14	|	15	|
  					|		|		|		|		|
					---------------------------------
 				5 	|	16	|	17	|	18	|	19	|
  					|		|		|		|		|
					---------------------------------
 */

inline vector<int>* SETI_Grid::findIntersectingCells(Polygon p) {
	vector<int>* intersectingCells = new vector<int>();
	box mbr;
	//cout << "find int cells: " << dsv(p) << endl;

	envelope(p, mbr);

	point maxcorner = mbr.max_corner();
	point mincorner = mbr.min_corner();

	float mincorner_x = mincorner.get<0>();
	float mincorner_y = mincorner.get<1>();
	float maxcorner_x = maxcorner.get<0>();
	float maxcorner_y = maxcorner.get<1>();

	double xlength = dimX / xCell;
	double ylength = dimY / yCell;

	int minBox_x = (int) (mincorner_x / xlength );
	int minBox_y = (int) (mincorner_y / ylength );
	int maxBox_x = (int) (maxcorner_x / xlength );
	int maxBox_y = (int) (maxcorner_y / ylength );


	try{
		for(int i = minBox_x; i <= maxBox_x; i++) {
			//cout << "Cell : " << i;
			for(int j = minBox_y; j <= maxBox_y; j++) {
				//cout << "\twith: " << j;
				if( boost::geometry::intersects (cells.at(i).at(j), p)  ) {
					int cell = xCell*j + i;
					intersectingCells->push_back(cell);
				}

			}
			//cout <<endl;
		}
	} catch(std::exception const&  ex){
		intersectingCells->clear();
		intersectingCells->push_back(-1);
	}
	return intersectingCells;
}

inline void SETI_Grid::insert(Instance* inst, long instanceLocation) {
	int sTime = inst->getStartTime();
	int eTime = inst->getEndTime();
	if(eTime > maxTime) {
		maxTime = eTime;
	}

	vector<Polygon> geometries = inst->getPolygons();
	//cout << "size of geometries " << dsv(geometries.at(0)) << endl;
	vector<int>* prevIntersectingCells = findIntersectingCells(geometries.at(0));

	/*for(unsigned int i = 0; i < prevIntersectingCells->size(); i++ ){
		cout <<"Prev: " <<  prevIntersectingCells->at(i) << endl;
	}*/

	vector<std::pair<int, time_point> > frontLine; // int - cellId, time_point - traj duration

	time_point trajDuration(sTime, sTime);	//first trajectory duration is startTime startTime,
	// but we will change the endTime of traj later.

	//for each intersecting cell add the first trajectory segment
	for (unsigned int i = 0; i < prevIntersectingCells->size(); i++) {
		std::pair<int, time_point> entry;
		if(prevIntersectingCells->at(i) != -1){
			entry = std::make_pair(prevIntersectingCells->at(i), trajDuration);
			frontLine.push_back(entry);
		}
		//frontLineTimeRanges.at(0).set<1>(eTime);
	}


	// iterate from the second geometry until last
	// for each geometry check if we need to include another geometry
	for (unsigned int i = 1; i < geometries.size(); i++) {

		//find the intersecting cells of current polygon
		vector<int>* currentIntersectingCells = findIntersectingCells( geometries.at(i));

		std::sort(currentIntersectingCells->begin(), currentIntersectingCells->end());



		//check it with what we have on frontline
		for (unsigned int cellIndex = 0; cellIndex < currentIntersectingCells->size(); cellIndex++) {
			int cellId = currentIntersectingCells->at(cellIndex);
			if(cellId == -1){
				//then this instance is out of the spatial framework
				break;
			}
			bool cellIdFound = false;
			unsigned int fl_cellIndex = 0;

			for (; fl_cellIndex < frontLine.size(); fl_cellIndex++) {
				if (cellId == frontLine.at(fl_cellIndex).first) { // then change the trajectory
					//then we found our old cell id
					//we need to change the trajectory
					frontLine.at(fl_cellIndex).second.set<1>(sTime + i);
					cellIdFound = true;
					break;
				}
			}
			if (!cellIdFound) {
				//new cell id, new trajectory
				//create time duration and cellId, then add the entry to front line
				time_point newTrajDuration(sTime + i, sTime + i);
				std::pair<int, time_point> entry;
				entry = std::make_pair(currentIntersectingCells->at(cellIndex), newTrajDuration);
				frontLine.push_back(entry);

			} //else {// we have already done that above}
		}

		//check frontLine and add the ending trajectories to index
		for (unsigned int cellIndex = 0; cellIndex < frontLine.size(); cellIndex++) {
			int cellId = frontLine.at(cellIndex).first;
			bool cellIdPresent = false;
			unsigned int cur_cellIndex = 0;

			for (; cur_cellIndex < currentIntersectingCells->size(); cur_cellIndex++) {
				//cout << "iterating current cell index  " << cur_cellIndex << endl;

				if (cellId == currentIntersectingCells->at(cur_cellIndex)) {
					//if cell id was already there, we have handled it earlier
					cellIdPresent = true;
					break;
				}
			}
			if (!cellIdPresent) {	// cell id is no longer in the current intersections
				// therefore we need to add it to index (rtrees)
				// then remove the trajectory segment from frontline

				//cout << "Do we come here at all??" << endl;

				//a) add trajectory segment to rtree index
				value trajSegment = std::make_pair(frontLine.at(cellIndex).second, instanceLocation);

				int cellCoordinate_x = cellId % xCell;
				int cellCoordinate_y = cellId / xCell;
				grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->insert(trajSegment);

				//b) remove the entry from segment, update cell index
				frontLine.erase (frontLine.begin()+cellIndex);
				cellIndex--;
			} //else {// we have already done that above}
		}
		delete currentIntersectingCells;
	}
	//in the end flush the front line and add everything to index
	value trajSegment;
	for(unsigned int i = 0; i < frontLine.size(); i++) {
		trajSegment = std::make_pair(frontLine.at(i).second, instanceLocation);
		int cellCoordinate_x = frontLine.at(i).first % xCell;
		int cellCoordinate_y = frontLine.at(i).first / xCell;
		grid_rtrees.at(cellCoordinate_x).at(cellCoordinate_y)->insert(trajSegment);
	}
	frontLine.clear();
	delete prevIntersectingCells;
	//try me
}

#endif /* SETI_GRID_HPP_ */
