/*
* ChebShevPatInstIndex.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef CHEBYSHEVPATINSTINDEX_HPP_
#define CHEBYSHEVPATINSTINDEX_HPP_

#include <vector>
#include <tuple>
#include <math.h>
#include <exception>

#include <boost/foreach.hpp>
#include <boost/geometry/multi/multi.hpp>
#include <boost/geometry/algorithms/intersects.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

#include "db/index/interface/IPatInstIndex.hpp"
#include "db/io/interface/IPatInstFileAccess.hpp"
#include "db/index/include/ChebyshevPolyGen.hpp"
#include "db/index/include/ChebyshevIndexBase.hpp"

using namespace std;

namespace bg = boost::geometry;
namespace bt = boost::posix_time;
namespace bgi = boost::geometry::index;

class ChebyshevPatInstIndex : public IPatInstIndex, private ChebyshevIndexBase {
private:

	ChebyshevPolyGen* generator;
	long queryCount, resultPreRefine, resulPostRefine, cadencePeroid;
	IPatInstFileAccess* fileAccess;

public:

	ChebyshevPatInstIndex(IPatInstFileAccess* fileAccess, ChebyshevPolyGen* generator, boost::posix_time::time_duration treeSpan, long cadencePeroid) :ChebyshevIndexBase(treeSpan) {
		this->fileAccess = fileAccess;
		this->generator = generator;
		this->cadencePeroid = cadencePeroid;
		queryCount = 0;
		resultPreRefine = 0;
		resulPostRefine = 0;
	}

	~ChebyshevPatInstIndex() {
		delete this->fileAccess;
	}

	/************************************************************************/
	/* insert		:Method that inserts pattern instances into the index and stores them into a file

	@param inst		:The pattern instance that is to be stored in a file and inserted into the index*/
	/************************************************************************/
	void insert(PatternInstance* patInst) {

		//get the period of the pattern instance we shall insert into the index
		bt::time_period trackPeriod(bt::from_time_t(patInst->getStartTime()), bt::from_time_t(patInst->getEndTime()));

		//get the trees that this instance falls into
		vector<int> idxs = this->getIdx(trackPeriod);

		//we need to get the geometries before we write the object to the file as that destroys the object
		vector<vector<Polygon>> patInstPolys = patInst->intersectionGeometries;

		//write the object to file and get the location back for later access
		bt::time_period currPeriod(bt::from_time_t(patInst->getStartTime()), seconds(this->cadencePeroid));
		long patInstFileLoc = this->fileAccess->writePatternInstanceToFile(patInst);
		delete patInst;

		//get the hull of the multi-polygons at each interval
		vector<Polygon> hulls;
		for (vector< vector<Polygon > >::iterator vecIter = patInstPolys.begin(); vecIter != patInstPolys.end(); vecIter++) {
			vector<Polygon> polysVect = vecIter.operator *();
			Multipolygon polygon_set;
			polygon_set.assign(polysVect.begin(), polysVect.end());
			Polygon conHull;
			bg::convex_hull(polygon_set, conHull);
			hulls.push_back(conHull);
		}

		//get the hull polynomial at each interval and insert into the proper tree
		vector<Polygon>::iterator instPolysIter = hulls.begin();
		for (unsigned int i = 0; i < idxs.size(); i++) {

			//get the index of the current tree for insertion
			int indexNumber = idxs.at(i);

			//get the time period of the current tree as we need it to calculate the chebyshev in and subsequent polygons for indexing
			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*indexNumber));
			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));

			//go the the first polygon that intersects the period of the current tree
			while (!currPeriod.intersects(indexPeriod)) {
				currPeriod = bt::time_period(currPeriod.end(), currPeriod.end() + seconds(this->cadencePeroid));
				instPolysIter++;
			}

			//calculate the chepyshev polynomial for the portion of the instance that falls into the time interval of the current tree
			ChebyshevPoly* chebpolynomial = this->generator->getPoly(instPolysIter, hulls.end(), indexPeriod, trackPeriod);

			//now get a linear approximating coefficients of this portion of the instance
			if (chebpolynomial->isValid()) {
				/*cout << "working so far:" << i << endl;*/
				lowOrdrPolynomials polys;
				polys.x0 = chebpolynomial->getXCoff()->at(0);
				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
				polys.w0 = chebpolynomial->getWidthCoff()->at(0);

				polys.dx = chebpolynomial->getXDev();
				polys.dy = chebpolynomial->getYDev();
				polys.dh = chebpolynomial->getHeightDev();
				polys.dw = chebpolynomial->getWidthDev();

				//if we have more than one coefficient use the first two
				if (chebpolynomial->getXCoff()->size() > 1) {

					polys.x1 = chebpolynomial->getXCoff()->at(1);
					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
				}
				else {
					//otherwise we have a constant valued function and the
					//second coefficient is zero
					polys.x1 = 0;
					polys.y1 = 0;
					polys.h1 = 0;
					polys.w1 = 0;
				}

				//use the linear approximation to get a polygon that represents it
				Polygon p = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);

				//get the envelope of that polygon, and insert the information into the current tree
				box b = bg::return_envelope<box>(p);
				this->trees->at(idxs.at(i))->insert(boost::make_tuple(b, chebpolynomial, patInstFileLoc));
			}
			else {
				//the next line says it all
				cout << "No insertion due to invalid or empty multi-polygons" << endl;
				//throw std::bad_exception();
			}
		}

	}

	vector<PatternInstance* >* searchST_Intersection(PatternInstance* patInst) {

		//get the period for the search instance so we can generate a chebyshev polynomial for it
		bt::time_period trackPeriod(bt::from_time_t(patInst->getStartTime()), bt::from_time_t(patInst->getEndTime()));

		//get the time period that the search instance period intersects with data in the index
		bt::time_period intersectPeriod = this->globalPeriod->intersection(trackPeriod);

		//get the index of the trees that this instance period falls into
		bt::time_duration diff = intersectPeriod.begin() - this->globalPeriod->begin();
		int beginIdx = (int)diff.total_seconds() / this->treeSpan.total_seconds();
		int endIdx = beginIdx + (int)intersectPeriod.length().total_seconds() / this->treeSpan.total_seconds();




		//get the hull of the multi-polygons at each interval
		vector<vector<Polygon>> patInstMultiPolys = patInst->intersectionGeometries;
		vector<Polygon> hulls;

		for (vector< vector<Polygon > >::iterator vecIter = patInstMultiPolys.begin(); vecIter != patInstMultiPolys.end(); vecIter++) {
			vector<Polygon> polysVect = vecIter.operator *();
			Multipolygon polygon_set;
			polygon_set.assign(polysVect.begin(), polysVect.end());
			Polygon conHull;
			bg::convex_hull(polygon_set, conHull);
			hulls.push_back(conHull);
		}



		//get the polygons, an iterator for them and the period of the first polygon
		vector<Polygon>::iterator instPolysIter = hulls.begin();
		bt::time_period currPeriod(bt::from_time_t(patInst->getStartTime()), seconds(this->cadencePeroid));

		//for all the trees that this instance period falls into search the tree for candidates
		vector<long> filter_results;
		for (int i = beginIdx; i <= endIdx; i++) {

			std::vector<value>result_tmp;

			bt::time_duration afterIndexStart(bt::seconds(this->treeSpan.total_seconds()*i));
			bt::time_period indexPeriod(this->globalPeriod->begin() + afterIndexStart, this->globalPeriod->begin() + (afterIndexStart + this->treeSpan));

			while (!currPeriod.intersects(indexPeriod)) {
				currPeriod = bt::time_period(currPeriod.end(), currPeriod.end() + seconds(this->cadencePeroid));
				instPolysIter++;
			}

			ChebyshevPoly* chebpolynomial = this->generator->getPoly(instPolysIter, hulls.end(), indexPeriod, trackPeriod);


			if (chebpolynomial->isValid()) {
				/*cout << "working so far:" << i << endl;*/
				lowOrdrPolynomials polys;
				polys.x0 = chebpolynomial->getXCoff()->at(0);
				polys.y0 = chebpolynomial->getWidthCoff()->at(0);
				polys.h0 = chebpolynomial->getHeightCoff()->at(0);
				polys.w0 = chebpolynomial->getWidthCoff()->at(0);

				polys.dx = chebpolynomial->getXDev();
				polys.dy = chebpolynomial->getYDev();
				polys.dh = chebpolynomial->getHeightDev();
				polys.dw = chebpolynomial->getWidthDev();

				//if we have more than one coefficient use the first two
				if (chebpolynomial->getXCoff()->size() > 1) {

					polys.x1 = chebpolynomial->getXCoff()->at(1);
					polys.y1 = chebpolynomial->getWidthCoff()->at(1);
					polys.h1 = chebpolynomial->getHeightCoff()->at(1);
					polys.w1 = chebpolynomial->getWidthCoff()->at(1);
				}
				else {
					//otherwise we have a constant valued function and the
					//second coefficient is zero
					polys.x1 = 0;
					polys.y1 = 0;
					polys.h1 = 0;
					polys.w1 = 0;
				}


				Polygon searchPly = this->getLinearPGone(polys, chebpolynomial->getValidIntervalBegin(), chebpolynomial->getValidIntervalEnd(), true);
				this->trees->at(i)->query(bgi::intersects(searchPly), std::back_inserter(result_tmp));

				BOOST_FOREACH(value res, result_tmp) {

					ChebyshevPoly* resultCheb = res.get<1>();
					double timeSlice = fabs((chebpolynomial->getValidIntervalEnd() - chebpolynomial->getValidIntervalBegin()) / 24.0);
					double sliceStart = chebpolynomial->getValidIntervalBegin();
					for (int j = 0; j < 24; j++) {

						float sliceEnd = sliceStart + timeSlice;
						if (sliceStart >= resultCheb->getValidIntervalBegin()) {

							if (sliceEnd > resultCheb->getValidIntervalEnd() && sliceStart < resultCheb->getValidIntervalEnd()) {
								sliceEnd = resultCheb->getValidIntervalEnd();
							}
							else if (sliceStart >= resultCheb->getValidIntervalEnd())
								break;


							Polygon resultPoly = this->getFullPGone(resultCheb, sliceStart, sliceEnd, true);
							Polygon queryPoly = this->getFullPGone(chebpolynomial, sliceStart, sliceEnd, true);

							if (bg::intersects(queryPoly, resultPoly)) {
								filter_results.push_back(res.get<2>());
								break;
							}

						}
						sliceStart = sliceEnd;
					}
				}

				delete chebpolynomial;
			}
			else {
				cout << "You tried to query with an object that falls outside the valid time period for this index!" << endl;
				delete chebpolynomial;
				//throw std::bad_exception();
			}


		}


		//clear out duplicate results from the vector of candidates
		std::sort(filter_results.begin(), filter_results.end());
		std::vector<long>::iterator it = std::unique(filter_results.begin(), filter_results.end());
		filter_results.resize(std::distance(filter_results.begin(), it));

		//bookkeeping, keeping track of how many filter results were returned
		this->resultPreRefine += filter_results.size();


		vector<PatternInstance* >* finalResultsVect = new vector<PatternInstance* >();
		//look at all the candidate results and determine if they are true results or a false return
		for (vector<long>::iterator it = filter_results.begin(); it != filter_results.end(); it++) {

			//get the pattern instance from the file
			long instIdx = it.operator *();
			PatternInstance* patInstCandidate = this->fileAccess->getPatternInstanceFromFile(instIdx);
			bool isIntersecting = false;

			//get the multi-polygons from the candidate so we can check them agains the query pattern instance
			vector< vector<Polygon> > candidateMultiPolys = patInstCandidate->intersectionGeometries;

			//get the time periods of the starting polygons for each of the returned pattern instance and the query pattern instance
			currPeriod = bt::time_period(bt::from_time_t(patInst->getStartTime()), seconds(this->cadencePeroid));
			bt::time_period candidateCurrPeriod(bt::from_time_t(patInstCandidate->getStartTime()), seconds(this->cadencePeroid));

			//for each multi-polygon in the search pattern instance, see if there is an actuall intersection with the candidate
			//once one is found, there is no need to continue processing this candidate as it is a true result
			//and can be returned.
			vector<vector<Polygon>>::iterator patInstMultiPolysIter = patInstMultiPolys.begin();
			vector<vector<Polygon>>::iterator candidateMultiPolysIter = candidateMultiPolys.begin();
			for (; patInstMultiPolysIter != patInstMultiPolys.end(); patInstMultiPolysIter++) {

				//if the current period is before the candidate there is no need to process, just continue.
				if (currPeriod < candidateCurrPeriod) {
					currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
					continue;
				}

				//if the candidate period is before the current of the query instance we need to iterate to the point they potentially
				//intersect and then check for intersection.
				while (candidateCurrPeriod < currPeriod && (candidateMultiPolysIter != candidateMultiPolys.end())) {
					candidateCurrPeriod = bt::time_period(candidateCurrPeriod.end(), seconds(this->cadencePeroid));
					candidateMultiPolysIter++;
				}

				//if we have reached the end of the polygons vector for the candidate then we can do no more comparisons to find intersection
				if (candidateMultiPolysIter == candidateMultiPolys.end())break;

				//loop through each polygon in the multi polygon of the candidate and see if it intersects with one of the
				//polygons from the multi-polygon of the query pattern instance.
				vector<Polygon> candidateMultiPoly = candidateMultiPolysIter.operator *();
				vector<Polygon> queryInstMultiPoly = patInstMultiPolysIter.operator *();
				for (vector<Polygon>::iterator candidatePolyIter = candidateMultiPoly.begin(); candidatePolyIter != candidateMultiPoly.end(); candidatePolyIter++) {
					Polygon candidatePoly = candidatePolyIter.operator *();
					for (vector<Polygon>::iterator queryInstMultiPolyIter = queryInstMultiPoly.begin(); queryInstMultiPolyIter != queryInstMultiPoly.end(); queryInstMultiPolyIter++) {
						Polygon queryInstPoly = queryInstMultiPolyIter.operator *();

						//if we have an intersection, add to results and break as we do not need to test any longer
						if (bg::intersects(queryInstPoly, candidatePoly)) {
							finalResultsVect->push_back(patInstCandidate);
							isIntersecting = true;
							break;
						}
					}
					if (isIntersecting)
						break;
				}

				if (isIntersecting)
					break;
				currPeriod = bt::time_period(currPeriod.end(), seconds(this->cadencePeroid));
			}

			if (!isIntersecting)
				delete patInstCandidate;
		}

		//bookkeeping, keeping track of the number of true results were returned and how many queries we have had
		this->resulPostRefine += finalResultsVect->size();
		this->queryCount++;

		//return the intersecting results
		return finalResultsVect;

	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
	return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this->fileAccess->begin());
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
	return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		return Iterator(this->fileAccess->end());
	}

};
#endif /* CHEBYSHEVPATINSTINDEX_HPP_ */
