/*
 * NoPatInstIndex.hpp
 *
 *  Created on: May 29, 2014
 * Author: Dustin Kempton
 */

#ifndef NOPATINSTINDEX_HPP_
#define NOPATINSTINDEX_HPP_

#include "db/index/interface/IPatInstIndex.hpp"
#include "db/io/interface/IPatInstFileAccess.hpp"

class NoPatInstIndex : public IPatInstIndex {
private:
	IPatInstFileAccess* fileAccess;
public:

	NoPatInstIndex(IPatInstFileAccess* fileAccess) {
		this->fileAccess = fileAccess;
	}

	~NoPatInstIndex() {
		delete this->fileAccess;

	}

	void insert(PatternInstance* patternInstance) {
		//cout << "We are about to write the pattern instance here with id::" << patternInstance->getPatternInstanceId() << endl;
		//cout<< "uni geom size at index insert" << patternInstance->unionGeometries.size() << endl;
		this->fileAccess->writePatternInstanceToFile(patternInstance);
		delete patternInstance;
		//cout << "We wrote it, right?" << endl;
	}

	vector<PatternInstance*>* searchST_Intersection(PatternInstance* inst) {
		vector<PatternInstance* >* finalResultsVect = new vector<PatternInstance* >();

		bt::time_period instTrackPeriod(bt::from_time_t(inst->getStartTime()),
			bt::from_time_t(inst->getEndTime()));

		Iterator it = this->fileAccess->begin();
		for (; it != this->fileAccess->end(); it++) {
			PatternInstance *tempPatternInstance = *it;
			bt::time_period tempInstanceTrackPeriod(
				bt::from_time_t(tempPatternInstance->getStartTime()),
				bt::from_time_t(tempPatternInstance->getEndTime()));

			if (tempInstanceTrackPeriod.intersects(instTrackPeriod)) {

				long intervalBegin = 0;
				long intervalEnd = 0;

				long delta = tempPatternInstance->getStartTime() - inst->getStartTime();
				long endDelta = tempPatternInstance->getEndTime() - inst->getEndTime();

				if (delta < 0) {
					intervalBegin = inst->getStartTime();
				}
				else {
					intervalBegin = tempPatternInstance->getStartTime();
				}

				if (endDelta < 0) {
					intervalEnd = tempPatternInstance->getEndTime();
				}
				else {
					intervalEnd = inst->getEndTime();
				}

				bool loopBreaker = false;
				for (long timestamp = intervalBegin; timestamp != intervalEnd; timestamp++) {
					unsigned int ii = (unsigned int)(timestamp - intervalBegin);
					for (unsigned int i = 0; i < tempPatternInstance->intersectionGeometries.at(ii).size(); i++) {

						Polygon tempPolygon = tempPatternInstance->intersectionGeometries.at(ii).at(i);

						for (unsigned int j = 0; j < inst->intersectionGeometries.at(ii).size(); j++) {
							Polygon instPolygon = inst->intersectionGeometries.at(ii).at(j);

							if (boost::geometry::intersects(instPolygon, tempPolygon)) {
								finalResultsVect->push_back(tempPatternInstance);
								loopBreaker = true;
								break;
							}
						}
						if (loopBreaker) {
							break;
						}
					}
					if (loopBreaker) {
						break;
					}
				}
				if (!loopBreaker)delete tempPatternInstance;
			}
			else{
				delete tempPatternInstance;
			}

		}

		return finalResultsVect;
	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
	 return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this->fileAccess->begin());
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
	 return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		return Iterator(this->fileAccess->end());
	}

};
#endif /* NOPATINSTINDEX_HPP_ */
