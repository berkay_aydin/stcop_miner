/*
 * SETIInstIndex.hpp
 *
 *  Created on: May 29, 2014
 * Author: Dustin Kempton
 */

#ifndef SETIINSTINDEX_HPP_
#define SETIINSTINDEX_HPP_

#include "db/index/interface/IInstIndex.hpp"
#include "db/io/interface/IInstFileAccess.hpp"
#include "db/index/include/SETI_Grid.hpp"


class SETIInstIndex : public IInstIndex {
private:
    IInstFileAccess* fileAccess;
    SETI_Grid *grid;
public:

    SETIInstIndex(IInstFileAccess* fileAccess, double dimX, double dimY, int xCell, int yCell) {
        this->fileAccess = fileAccess;
        grid = new SETI_Grid(dimX, dimY, xCell, yCell);
    }

    ~SETIInstIndex() {
        delete this->fileAccess;
    }

    void insert(Instance* inst) {
        long fileLoc = this->fileAccess->writeInstanceToFile(inst);
        grid->insert(inst, fileLoc);
        delete inst;
    }

    vector<Instance*>* searchST_Intersection(Instance* inst) {
        vector<Instance* >* results = new vector<Instance* >();
        vector<long>* instFileLocs = grid->ST_Intersection(inst); //file locations of instances
        for(unsigned int i=0; i < instFileLocs->size(); i++) {
            long fileLoc = instFileLocs->at(i);
            Instance* resultInstance = this->fileAccess->getInstanceFromFile(fileLoc);

            long intervalBegin = 0;
            long intervalEnd = 0;

            long delta = resultInstance->getStartTime()- inst->getStartTime();
            long endDelta = resultInstance->getEndTime() - inst->getEndTime();

            if (delta < 0) {
                intervalBegin = inst->getStartTime();
            } else {
                intervalBegin = resultInstance->getStartTime();
            }

            if (endDelta < 0) {
                intervalEnd = resultInstance->getEndTime();
            } else {
                intervalEnd = inst->getEndTime();
            }

            for (long timestamp = intervalBegin; timestamp <= intervalEnd; timestamp++) {
                if (boost::geometry::intersects(inst->getPolygon(timestamp),
                                                resultInstance->getPolygon(timestamp))) {
                    results->push_back(resultInstance);
                    break;
                }
            }
        }

        return results;
    }

    /************************************************************************/
    /* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
    /************************************************************************/
    Iterator begin() {
        return Iterator(this->fileAccess->begin());
    }

    /************************************************************************/
    /* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
    /************************************************************************/
    Iterator end() {
        return Iterator(this->fileAccess->end());
    }
};
#endif /* SETIINSTINDEX_HPP_ */
