/*
 * NoInstIndex.hpp
 *
 *  Created on: May 29, 2014
 * Author: Dustin Kempton
 */

#ifndef NOINSTINDEX_HPP_
#define NOINSTINDEX_HPP_

#include "db/index/interface/IInstIndex.hpp"
#include "db/io/interface/IInstFileAccess.hpp"
#include <boost/foreach.hpp>
#include <boost/geometry/algorithms/intersects.hpp>
#include <boost/chrono.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/conversion.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/unordered_map.hpp>

using namespace std;

namespace bg = boost::geometry;
namespace bt = boost::posix_time;
namespace bgi = boost::geometry::index;

class NoInstIndex : public IInstIndex {

private:
	IInstFileAccess* fileAccess;
	//boost::unordered_map<int, long> instFileLocMap;

public:

	NoInstIndex(IInstFileAccess* fileAccess) {
		this->fileAccess = fileAccess;
	}

	~NoInstIndex() {
		delete this->fileAccess;
	}

	void insert(Instance* inst) {
		this->fileAccess->writeInstanceToFile(inst);
		delete inst;
	}

	vector<Instance*>* searchST_Intersection(Instance* inst) {

		vector<Instance* >* finalResultsVect = new vector<Instance* >();

		bt::time_period instTrackPeriod(bt::from_time_t(inst->getStartTime()),
			bt::from_time_t(inst->getEndTime()));

		Iterator it = this->fileAccess->begin();
		for (; it != this->fileAccess->end(); it++) {
			Instance *tempInstance = *it;
			bt::time_period tempInstanceTrackPeriod(
				bt::from_time_t(tempInstance->getStartTime()),
				bt::from_time_t(tempInstance->getEndTime()));

			if (tempInstanceTrackPeriod.intersects(instTrackPeriod)) {

				long intervalBegin = 0;
				long intervalEnd = 0;

				long delta = tempInstance->getStartTime()
					- inst->getStartTime();
				long endDelta = tempInstance->getEndTime() - inst->getEndTime();

				if (delta < 0) {
					intervalBegin = inst->getStartTime();
				}
				else {
					intervalBegin = tempInstance->getStartTime();
				}

				if (endDelta < 0) {
					intervalEnd = tempInstance->getEndTime();
				}
				else {
					intervalEnd = inst->getEndTime();
				}
				bool inserted = false;
				for (long timestamp = intervalBegin; timestamp <= intervalEnd; timestamp++) {
					if (boost::geometry::intersects(inst->getPolygon(timestamp),
						tempInstance->getPolygon(timestamp))) {
						finalResultsVect->push_back(tempInstance);
						inserted = true;
						break;
					}
				}
				if (!inserted)delete tempInstance;
			}
			else{
				delete tempInstance;
			}

		}

		return finalResultsVect;

	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
	 return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this->fileAccess->begin());
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
	 return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		return Iterator(this->fileAccess->end());
	}
}
;
#endif /* NOINSTINDEX_HPP_ */
