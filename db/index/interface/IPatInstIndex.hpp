/*
* IPatInstIndex.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef IPATINSTINDEX_HPP_
#define IPATINSTINDEX_HPP_


#include <list>
#include <utility>
#include "Instance.h"
#include "PatternInstance.h"
#include "db/io/interface/IPatInstFileAccess.hpp"

class IPatInstIndex {
public:

    /************************************************************************/
    /* Iterator used to iterate through the pattern instance file*/
    /************************************************************************/
class Iterator : public std::iterator<std::forward_iterator_tag, PatternInstance* > {
    private:
        IPatInstFileAccess::Iterator iter;

    public:

        Iterator( const IPatInstFileAccess::Iterator instItr):iter(instItr) {}

        ~Iterator() {}

        bool operator==(const Iterator& other) {
            return(iter == other.iter);
        }

        bool operator!=(const Iterator& other) {
            return(iter != other.iter);
        }

        Iterator& operator++() {
            iter++;
            return *this;
        }

        Iterator& operator++(int) {
            ++(*this);
            return *this;
        }

        PatternInstance*& operator*() {
            return iter.operator *();
        }

        PatternInstance** operator->() {
            return iter.operator->();
        }
    };

    virtual void insert(PatternInstance* inst) = 0;

    // TODO delete it not used //virtual vector<PatternInstance* >* searchST_Intersection(Instance* inst) = 0;
    virtual vector<PatternInstance* >* searchST_Intersection(PatternInstance* patInst) = 0;

    /************************************************************************/
    /* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
    /************************************************************************/
    virtual Iterator begin() = 0;

    /************************************************************************/
    /* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
    /************************************************************************/
    virtual Iterator end() = 0;

};
#endif /* IPATINSTINDEX_HPP_ */
