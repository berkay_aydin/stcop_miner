/*
* IInstIndex.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef IINSTINDEX_HPP_
#define IINSTINDEX_HPP_


#include <list>
#include <iterator>
#include "Instance.h"
#include "PatternInstance.h"
#include "db/io/interface/IInstFileAccess.hpp"

class IInstIndex {
protected:


public:

    /************************************************************************/
    /* Iterator used to iterate through the instance file*/
    /************************************************************************/
class Iterator : public std::iterator<std::forward_iterator_tag, Instance* > {
    private:
        IInstFileAccess::Iterator iter;

    public:

        Iterator( const IInstFileAccess::Iterator instItr ):iter(instItr) {}

        ~Iterator() {}

        bool operator==(const Iterator& other) {
            return(iter == other.iter);
        }

        bool operator!=(const Iterator& other) {
            return(iter != other.iter);
        }

        Iterator& operator++() {
            iter++;
            return *this;
        }

        Iterator& operator++(int) {
            ++(*this);
            return *this;
        }

        Instance*& operator*() {
            return iter.operator *();
        }

        Instance** operator->() {
            return iter.operator->();
        }
    };

    virtual void insert(Instance* inst) = 0;

    virtual vector<Instance* >* searchST_Intersection(Instance* inst) = 0;

    /************************************************************************/
    /* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
    /************************************************************************/
    virtual Iterator begin() = 0;

    /************************************************************************/
    /* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
    /************************************************************************/
    virtual Iterator end() = 0;


};
#endif /* IINSTINDEX_HPP_ */
