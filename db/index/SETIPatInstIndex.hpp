/*
 * SETIPatInstIndex.hpp
 *
 *  Created on: May 29, 2014
 * Author: Dustin Kempton
 */

#ifndef SETIPATINSTINDEX_HPP_
#define SETIPATINSTINDEX_HPP_

#include "db/index/interface/IPatInstIndex.hpp"
#include "db/io/interface/IPatInstFileAccess.hpp"

class SETIPatInstIndex : public IPatInstIndex {
private:
	IPatInstFileAccess* fileAccess;
	SETI_Grid *grid;
public:

	SETIPatInstIndex(IPatInstFileAccess* fileAccess, double dimX, double dimY, int xCell, int yCell) {
		this->fileAccess = fileAccess;
		 grid = new SETI_Grid(dimX, dimY, xCell, yCell);
	}

	~SETIPatInstIndex() {
		delete this->fileAccess;

	}

	void insert(PatternInstance* pInst) {
		//get the file location
		long fileLoc = this->fileAccess->writePatternInstanceToFile(pInst);
		//insert it to the index
		this->grid->p_insert(pInst, fileLoc);
		delete pInst;
	}

	vector<PatternInstance* >* searchST_Intersection(PatternInstance* pInst) {
		vector<PatternInstance* >* results = new vector<PatternInstance* >();
		vector<long>* pInstFileLocs = grid->p_ST_Intersection(pInst);
		for(unsigned int i=0; i < pInstFileLocs->size(); i++) {
			long fileLoc = pInstFileLocs->at(i);
			PatternInstance* resultPatInst = this->fileAccess->getPatternInstanceFromFile(fileLoc);

			//cout <<"unisize" << resultPatInst->unionGeometries.size() << endl;
			long intervalBegin = 0;
			long intervalEnd = 0;

			long delta = resultPatInst->getStartTime()- pInst->getStartTime();
			long endDelta = resultPatInst->getEndTime() - pInst->getEndTime();

			if (delta < 0) {
				intervalBegin = pInst->getStartTime();
			} else {
				intervalBegin = resultPatInst->getStartTime();
			}

			if (endDelta < 0) {
				intervalEnd = resultPatInst->getEndTime();
			} else {
				intervalEnd = pInst->getEndTime();
			}
			bool loopBreaker = false;
			for (long timestamp = intervalBegin; timestamp <= intervalEnd; timestamp++) {
				unsigned int ii = (unsigned int)(timestamp - intervalBegin);
				for (unsigned int i = 0; i < pInst->intersectionGeometries.at(ii).size(); i++) {

					Polygon tempPolygon = pInst->intersectionGeometries.at(ii).at(i);

					for(unsigned int j=0; j < resultPatInst -> intersectionGeometries.at(ii).size();j++) {
						Polygon instPolygon = resultPatInst->intersectionGeometries.at(ii).at(j);

						if (boost::geometry::intersects(instPolygon,tempPolygon)) {
							//cout << "Adding Pattern instance: " << resultPatInst->getPatternInstanceId() << endl;
							results->push_back(resultPatInst);
							loopBreaker = true;
							break;
						}
					}
					if(loopBreaker){break;}
				}
				if(loopBreaker){break;}
			}
		}
		return results;
	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this->fileAccess->begin());
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		return Iterator(this->fileAccess->end());
	}

};
#endif /* SETIPATINSTINDEX_HPP_ */
