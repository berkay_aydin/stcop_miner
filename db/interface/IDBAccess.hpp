/*
* IDBAccess.hpp
*
*  Created on: May 27, 2014
* Author: Dustin
*/

#ifndef IDBACCESS_HPP_
#define IDBACCESS_HPP_


#include <list>
#include "Instance.h"
#include "PatternInstance.h"
#include <boost/function.hpp>
#include "db/index/interface/IInstIndex.hpp"
#include "db/index/interface/IPatInstIndex.hpp"

template<class T, class T2>
class JoinTableResults {
private:
	T2* indexType;
	string tableName;
	boost::function< vector<T*>* (T*, string) > getMethod;
public:

	JoinTableResults(T2* indexType, boost::function< vector<T*>* (T*, string) > getMethod, string tableName) {
		this->indexType = indexType;
		this->getMethod = getMethod;
		this->tableName = tableName;
	}

	/************************************************************************/
	/* Iterator used to iterate through the results of the join T is the type of
	the value returned T2 is the index type*/
	/************************************************************************/
	class Iterator : public std::iterator<std::forward_iterator_tag, std::pair<T*, T*> > {
	private:
		bool pairSet = false;
		string tableName;
		pair<T*, T*> currResult;
		typename T2::Iterator curItr;
		typename T2::Iterator endItr;
		vector<T*>* currResults = NULL;
		typename vector<T*>::iterator currPos;
		boost::function< vector<T*>* (T*, string tableName) > getMethod;

	public:

		Iterator(typename T2::Iterator  beginItr, typename T2::Iterator  endItr, boost::function< vector<T*>* (T*, string tableName) > getMethod, string tableName) :
			curItr(beginItr), endItr(endItr) {
			this->getMethod = getMethod;
			this->tableName = tableName;
			++(*this);
		}

		~Iterator() {
			if (currResults != NULL)
				delete currResults;
		}

		bool operator==(const Iterator& other) {
			if (currResults != NULL) {
				return(currPos == other.currPos && curItr == other.curItr);
			}
			else {
				return(curItr = other.curItr);
			}
		}

		bool operator!=(const Iterator& other) {
			if (currResults != NULL) {
				return(currPos != other.currPos && curItr != other.curItr);
			}
			else {
				return(curItr != other.curItr);
			}
		}

		Iterator& operator++() {

			if (currResults == NULL) {
				if (curItr != endItr) {
					T* tmp = curItr.operator*();
					currResults = this->getMethod(tmp, this->tableName);
					this->currPos = currResults->begin();
					delete tmp;

					if (currPos == currResults->end()){
						++(*this);
					}
				}
			}
			else if (currPos == currResults->end()) {

				//if the current vector of results is at its end then we check to see
				//if there are any more items in the first table to iterate through
				curItr++;
				if (curItr != endItr) {

					//delete the current vector of results (which could have been empty)
					delete currResults;
					T* tmp = curItr.operator*();
					currResults = this->getMethod(tmp, this->tableName);
					this->currPos = currResults->begin();
					delete tmp;

					if (currPos == currResults->end()){
						++(*this);
					}
				}

			}
			else {
				currPos++;
				if (currPos == currResults->end()) {
					++(*this);
				}
			}

			pairSet = false;
			return *this;
		}

		Iterator& operator++(int) {
			++(*this);
			return *this;
		}

		pair<T*, T*>& operator*() {
			
			//if attempting to access the element when the tables have reached the end
			if (curItr == endItr && currPos == currResults->end()){
				logic_error("Attempting to access past the end of the results");
			}

			if (!pairSet) {
				if (currResults == NULL) {
					++(*this);
				}

				T* left = curItr.operator*();
				T* right = currPos.operator *();
				currResult = std::make_pair(left, right);
				pairSet = true;
			}
			return currResult;

		}

		pair<T*, T*>* operator->() {

			//if attempting to access the element when the tables have reached the end
			if (curItr == endItr && currPos == currResults->end()){
				logic_error("Attempting to access past the end of the results");
			}

			if (!pairSet) {
				if (currResults == NULL) {
					++(*this);
				}
				T* left = curItr.operator*();
				T* right = currPos.operator *();
				currResult = std::make_pair(left, right);
				pairSet = true;
			}
			return &currResult;
		}
	};

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the table
	return		:Returns the iterator that points to the beginning of the table */
	/************************************************************************/
	Iterator begin() {
		return(Iterator(this->indexType->begin(), this->indexType->end(), this->getMethod, this->tableName));
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the table
	return	:Returns an iterator pointing to the end of the table*/
	/************************************************************************/
	Iterator end() {
		return(Iterator(this->indexType->end(), this->indexType->end(), this->getMethod, this->tableName));
	}
};

class IDBAccess {
public:


	virtual JoinTableResults<Instance, IInstIndex>* spatiotemporalJoin(string table1, string table2) = 0;
	virtual JoinTableResults<PatternInstance, IPatInstIndex>* p_spatiotemporalJoin(string table1, string table2) = 0;


	virtual vector<Instance*>* searchST_Intersection(Instance *i, string table) = 0;
	virtual vector<PatternInstance*>* searchST_Intersection(PatternInstance *pinst, string table) = 0;

	virtual void insert(Instance* inst, string tableName) = 0;
	virtual void insert(PatternInstance* patInst, string tableName) = 0;

};
#endif /* IDBACCESS_HPP_ */
