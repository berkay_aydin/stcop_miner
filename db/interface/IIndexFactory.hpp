/*
* IIndexFactory.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef IINDEXFACTORY_HPP_
#define IINDEXFACTORY_HPP_

#include "db/index/interface/IInstIndex.hpp"
#include "db/index/interface/IPatInstIndex.hpp"
class IIndexFactory {
public:
    virtual IInstIndex* getInstIdx(std::string tableName) = 0;
    virtual IPatInstIndex* getPatInstIdx(std::string tableName) = 0;
};
#endif /* IINDEXFACTORY_HPP_ */
