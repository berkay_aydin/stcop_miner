/*
* ChebShevIdxFactory.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef CHEBYSHEVINDEXFACTORY_HPP_
#define CHEBYSHEVINDEXFACTORY_HPP_

#include <boost/date_time/posix_time/posix_time.hpp>
#include "db/index/include/ChebyshevPolyGen.hpp"
#include "db/index/ChebyshevPatInstIdx.hpp"
#include "db/interface/IIndexFactory.hpp"
#include "db/index/ChebyshevInstIdx.hpp"
#include "db/io/PatInstFileAccess.hpp"
#include "db/io/InstFileAccess.hpp"

using namespace std;

class ChebyshevIdxFactory : public IIndexFactory {
    string filesLoc;
    int bytesPerRead;
    long cadencePeriod;
    double fractionOfPolys;
    ChebyshevPolyGen* generator;
    boost::posix_time::time_duration treeSpan;
public:

    ChebyshevIdxFactory(string storageFilesLocation, int bytesPerRead, long cadencePeriod, long treePeriod, double fractionOfPolys) {
        this->filesLoc = storageFilesLocation;
        this->bytesPerRead = bytesPerRead;
        this->fractionOfPolys = fractionOfPolys;
        this->cadencePeriod = cadencePeriod;
        this->generator = new ChebyshevPolyGen(this->fractionOfPolys, this->cadencePeriod);
        treeSpan = boost::posix_time::time_duration(seconds(treePeriod));
    }

    ~ChebyshevIdxFactory() {
        delete generator;
    }

    IInstIndex* getInstIdx(string tableName) {

        IInstFileAccess* fa = new InstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IInstIndex* idx = new ChebyshevInstIndex(fa, this->generator, this->treeSpan, this->cadencePeriod);

        return idx;
    }

    IPatInstIndex* getPatInstIdx(string tableName) {
        IPatInstFileAccess* fa = new PatInstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IPatInstIndex* idx = new ChebyshevPatInstIndex(fa, this->generator, this->treeSpan, this->cadencePeriod);

        return idx;
    }

};
#endif /* CHEBYSHEVINDEXFACTORY_HPP_ */
