/*
* NoIdxFactory.hpp
*
*  Created on: May 29, 2014
* Author: Dustin Kempton
*/

#ifndef NOINDEXFACTORY_HPP_
#define NOINDEXFACTORY_HPP_

#include "db/interface/IIndexFactory.hpp"
#include "db/index/NoPatInstIdx.hpp"
#include "db/index/NoInstIdx.hpp"
#include "db/io/PatInstFileAccess.hpp"
#include "db/io/InstFileAccess.hpp"

using namespace std;

class NoIdxFactory : public IIndexFactory {
    string filesLoc;
    int bytesPerRead;
public:

    NoIdxFactory(string storageFilesLocation, int bytesPerRead) {
        this->filesLoc = storageFilesLocation;
        this->bytesPerRead = bytesPerRead;
    }

    IInstIndex* getInstIdx(string tableName) {

        IInstFileAccess* fa = new InstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IInstIndex* idx = new NoInstIndex(fa);

        return idx;
    }

    IPatInstIndex* getPatInstIdx(string tableName) {
        IPatInstFileAccess* fa = new PatInstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IPatInstIndex* idx = new NoPatInstIndex(fa);

        return idx;
    }

};
#endif /* NOINDEXFACTORY_HPP_ */
