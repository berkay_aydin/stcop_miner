/*
* IFileAccess.hpp
*
*  Created on: May 20, 2014
* Author: Dustin
*/

#ifndef IPATINSTFILEACCESS_HPP_
#define IPATINSTFILEACCESS_HPP_


#include <list>
#include <utility>
#include "../../../Instance.h"
#include "../../../Pattern.h"
#include "../../../PatternInstance.h"

class IPatInstFileAccess {
public:

    /************************************************************************/
    /* Iterator used to iterate through the paterninstance file*/
    /************************************************************************/
class Iterator : public std::iterator<std::forward_iterator_tag, PatternInstance* > {
    private:
        bool pairSet;
        bool accessed;
        std::pair<int, PatternInstance*> currentInst;
        long nextIdx;
        IPatInstFileAccess* fileAccessPtr;

    public:

        Iterator(IPatInstFileAccess* fileAccess, long idx) {
            fileAccessPtr = fileAccess;
            nextIdx = idx;
            pairSet = false;
            accessed = true;
        }

        ~Iterator() {
            if (!accessed)
                delete currentInst.second;
        }

        bool operator==(const Iterator& other) {
            return(nextIdx == other.nextIdx);
        }

        bool operator!=(const Iterator& other) {
            return(nextIdx != other.nextIdx);
        }

        Iterator& operator++() {
            if (!pairSet) {
                currentInst = fileAccessPtr->getPatternInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = false;
            } else if (accessed) {
                currentInst = fileAccessPtr->getPatternInstancePairFromFile(nextIdx);
                nextIdx = currentInst.first;
                accessed = false;
            } else if (!accessed) {
                delete currentInst.second;
                currentInst = fileAccessPtr->getPatternInstancePairFromFile(nextIdx);
                nextIdx = currentInst.first;
            }


            return *this;
        }

        Iterator& operator++(int) {
            ++(*this);
            return *this;
        }

        PatternInstance*& operator*() {
            if (!pairSet || accessed) {
                currentInst = fileAccessPtr->getPatternInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = true;
                return currentInst.second;
            } else {
                accessed = true;
                return currentInst.second;
            }
        }

        PatternInstance** operator->() {
            if (!pairSet || accessed) {
                currentInst = fileAccessPtr->getPatternInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = true;
                return &currentInst.second;
            } else {
                accessed = true;
                return &currentInst.second;
            }
        }
    };

    /************************************************************************/
    /* writePatternInstanceToFile	:saves a Pattern Instance to a file and deletes the object from memory

    @param	instance				:the Pattern Instance to save to a file
    @return							:returns the location in the file that the object begins*/
    /************************************************************************/
    virtual long writePatternInstanceToFile(PatternInstance* patternInstance) = 0;


    /************************************************************************/
    /*getPatternInstanceFromFile    :Method for returning a Pattern Instance from a file on disk

    @param loc						:The location in the file that we start reading for the object of interest
    @return							:returns the object found at the location passed in*/
    /************************************************************************/
    virtual PatternInstance* getPatternInstanceFromFile(long loc) = 0;


    /************************************************************************/
    /*getPatternInstancePairFromFile    :Method for returning an PatternInstance from a file on disk.

    @param loc				 :The location in the file that we start reading for the object of interest
    @return					 :returns the index of the next object(which is not guaranteed to not be the end of the file
    so be careful using this value on subsequent calls) and the object found at the location passed in*/
    /************************************************************************/
    virtual std::pair<long, PatternInstance*>getPatternInstancePairFromFile(long loc) = 0;


    /************************************************************************/
    /*getNumSequentialReads  :Returns the number of sequential bytes read divided by the disk page size
    that was passed into the constructor of this object*/
    /************************************************************************/
    virtual int getNumSequentialReads() = 0;

    /************************************************************************/
    /*getNumRandomReads  :Returns the number of times we called the load method as each time we do
    it can be considered as a random access to the disk*/
    /************************************************************************/
    virtual int getNumRamdomReads() = 0;

    /************************************************************************/
    /* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
    /************************************************************************/
    virtual Iterator begin() = 0;

    /************************************************************************/
    /* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
    /************************************************************************/
    virtual Iterator end() = 0;

};


#endif /* IPATINSTFILEACCESS_HPP_ */
