/*
 * IFileAccess.hpp
 *
 *  Created on: May 27, 2014
 * Author: Dustin
 */

#ifndef IINSTFILEACCESS_HPP_
#define IINSTFILEACCESS_HPP_


#include <list>
#include <utility>

#include "../../../Instance.h"
#include "../../../PatternInstance.h"

class IInstFileAccess {

public:

    /************************************************************************/
    /* Iterator used to iterate through the instance file*/
    /************************************************************************/
class Iterator : public std::iterator<std::forward_iterator_tag, Instance* > {
    private:
        bool pairSet;
        bool accessed;
        std::pair<int, Instance*> currentInst;
        long nextIdx;
        IInstFileAccess* fileAccessPtr;

    public:

        Iterator(IInstFileAccess* fileAccess, long idx) {
            fileAccessPtr = fileAccess;
            nextIdx = idx;
            pairSet = false;
            accessed = true;
        }

        ~Iterator() {
            if (!accessed)
                delete currentInst.second;
        }

        bool operator==(const Iterator& other) {
            return(nextIdx == other.nextIdx);
        }

        bool operator!=(const Iterator& other) {
            return(nextIdx != other.nextIdx);
        }

        Iterator& operator++() {
            if (!pairSet) {
                currentInst = fileAccessPtr->getInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = false;
            } else if (accessed) {
                currentInst = fileAccessPtr->getInstancePairFromFile(nextIdx);
                nextIdx = currentInst.first;
                accessed = false;
            } else if (!accessed) {
                delete currentInst.second;
                currentInst = fileAccessPtr->getInstancePairFromFile(nextIdx);
                nextIdx = currentInst.first;
            }


            return *this;
        }

        Iterator& operator++(int) {
            ++(*this);
            return *this;
        }

        Instance*& operator*() {
            if (!pairSet || accessed) {
                currentInst = fileAccessPtr->getInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = true;
                return currentInst.second;
            } else {
                accessed = true;
                return currentInst.second;
            }
        }

        Instance** operator->() {
            if (!pairSet || accessed) {
                currentInst = fileAccessPtr->getInstancePairFromFile(nextIdx);
                pairSet = true;
                accessed = true;
                return &currentInst.second;
            } else {
                accessed = true;
                return &currentInst.second;
            }
        }
    };

    /************************************************************************/
    /* writeInstanceToFile	:saves an Instance to a file and deletes the object from memory

    @param	instance		:the Instance to save to a file
    @return					:returns the location in the file that the object begins*/
    /************************************************************************/
    virtual long writeInstanceToFile(Instance* instance) = 0;

    /************************************************************************/
    /*getInstanceFromFile    :Method for returning an Instance from a file on disk.

    @param loc				 :The location in the file that we start reading for the object of interest
    @return					 :returns the object found at the location passed in*/
    /************************************************************************/
    virtual Instance* getInstanceFromFile(long loc) = 0;

    /************************************************************************/
    /*getInstancePairFromFile    :Method for returning an Instance from a file on disk.

    @param loc				 :The location in the file that we start reading for the object of interest
    @return					 :returns the index of the next object(which is not guaranteed to not be the end of the file
    so be careful using this value on subsequent calls) and the object found at the location passed in*/
    /************************************************************************/
    virtual std::pair<long, Instance*>getInstancePairFromFile(long loc) = 0;

    /************************************************************************/
    /*getNumSequentialReads  :Returns the number of sequential bytes read divided by the disk page size
    that was passed into the constructor of this object*/
    /************************************************************************/
    virtual int getNumSequentialReads() = 0;

    /************************************************************************/
    /*getNumRandomReads  :Returns the number of times we called the load method as each time we do
    it can be considered as a random access to the disk*/
    /************************************************************************/
    virtual int getNumRamdomReads() = 0;

    /************************************************************************/
    /* begin	:Method for returning an iterator to the beginning of the file
    return		:Returns the iterator that points to the beginning of the file */
    /************************************************************************/
    virtual Iterator begin() = 0;

    /************************************************************************/
    /* end	:Method for returning an iterator that points to the end of the file
    return	:Returns an iterator pointing to the end of the file*/
    /************************************************************************/
    virtual Iterator end() = 0;

};
#endif /* IINSTFILEACCESS_HPP_ */
