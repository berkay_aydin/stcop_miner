/*
* IFileAccess.hpp
*
*  Created on: May 27, 2014
* Author: Dustin
*/

#ifndef PATINSTFILEACCESS_HPP_
#define PATINSTFILEACCESS_HPP_

#ifdef _OPENMP
# include <omp.h>
#endif
#include <set>

#include <iostream>
#include <fstream>
#include <istream>

#include "interface/IPatInstFileAccess.hpp"
//#include "db/io/interface/IPatInstFileAccess.hpp"

#include "./Instance.h"
#include "./PatternInstance.h"

#include <boost/geometry.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

using namespace std;
namespace bg = boost::geometry;

typedef bg::model::point<int, 2, bg::cs::cartesian> point;
typedef bg::model::polygon<point, false, false > Polygon;

class PatInstFileAccess : public IPatInstFileAccess {
private:

	//if we have OpenMP enabled we will have locking on portions of this class
#ifdef _OPENMP

	omp_lock_t lock;
#endif

	struct membuf : std::streambuf {
		membuf(char* begin, char* end) {
			this->setg(begin, begin, end);
		}
	};

	int seqReads;
	int randReads;
	int bytesPerRead;
	string file;
	std::fstream* fileIOStream;


public:
	/************************************************************************/
	/* PatInstFileAccess:		The constructor for the PatInstFileAccess class

	@param fileLocation:	the location that the file will be stored.
	@param fileName:		the name of the file to be used for storing instances in
	@param bytesPerRead:	the number of bytes we are considering as a disk page
	(i.e. the smallest block of data the os will read from the disk)*/
	/************************************************************************/
	PatInstFileAccess(string fileLocation, string fileName, int bytesPerRead) {

		//init the lock
#ifdef _OPENMP
		omp_init_lock(&lock);
#endif

		this->seqReads = 0;
		this->randReads = 0;
		this->bytesPerRead = bytesPerRead;

		this->file = fileLocation + "/" + fileName + ".bin";

		this->fileIOStream = new std::fstream(this->file.c_str(), ios::out | ios::in | ios::trunc | ios::binary);

		if (!this->fileIOStream->is_open()) {

			cout << "failed to open IO file" << endl;
			//	throw "Failed to open";
		}
	}

	/************************************************************************/
	/* ~PatInstFileAccess	:The destructor of this object, it destroys the lock(if it was initialized)
	It also closes the file and removes it.  (If we want the file to last past the
	end of the program we need to remove that portion of the code)*/
	/************************************************************************/
	~PatInstFileAccess() {

		//destroy the lock
#ifdef _OPENMP
		omp_destroy_lock(&lock);
#endif

		fileIOStream->close();
		delete fileIOStream;

		std::remove(this->file.c_str());
	}

	/************************************************************************/
	/* writePatternInstanceToFile	:saves a Pattern Instance to a file and deletes the object from memory

	@param	instance				:the Pattern Instance to save to a file
	@return							:returns the location in the file that the object begins*/
	/************************************************************************/
	long writePatternInstanceToFile(PatternInstance* instance) {
		//cout << instance->unionGeometries.size() << endl;
		//cout << "Union Poly Vect size:: " << instance->unionGeometries.size() << endl;

		stringstream ss;

		//add ids and time period
		ss << instance->getPatternInstanceId() << endl;
		ss << instance->getStartTime() << endl;
		ss << instance->getEndTime() << endl;
		ss << instance->getIntersectionVolume() << endl;
		ss << instance->getUnionVolume() << endl;
		ss << instance->getMaxVolume() << endl;

		list<int> particIDs = instance->participatingInstances;
		for (list<int>::iterator iter = particIDs.begin(); iter != particIDs.end(); iter++) {
			ss << iter.operator *() << endl;
		}
		ss << "endPartIDs" << endl;

		//add intersection multi-polygons
		vector<vector<Polygon> > intersectPolyVects = instance->intersectionGeometries;
		for (unsigned int i = 0; i < intersectPolyVects.size(); i++){
			for (unsigned int j = 0; j < intersectPolyVects.at(i).size(); j++) {
				ss << bg::wkt<Polygon>(intersectPolyVects.at(i).at(j)) << endl;
			}
			ss << "endPolyVect" << endl;
		}

		/*for (vector<vector<Polygon> >::iterator itr = intersectPolyVects.begin(); itr != intersectPolyVects.end(); itr++) {
			vector<Polygon> polys = itr.operator *();
			for (vector<Polygon > ::iterator iter = polys.begin(); iter != polys.end(); iter++) {
			ss << bg::wkt<Polygon>(iter.operator *()) << endl;
			}
			ss << "endPolyVect" << endl;
			}*/
		ss << "endVectVect" << endl;

		//add union multi-polygons


		vector<vector<Polygon> > unionPolyVects = instance->unionGeometries;
		for (unsigned int i = 0; i < unionPolyVects.size(); i++){
			for (unsigned int j = 0; j < unionPolyVects.at(i).size(); j++) {
				//cout << bg::wkt<Polygon>( unionPolyVects.at(i).at(j) ) << endl;
				ss << bg::wkt<Polygon>(unionPolyVects.at(i).at(j)) << endl;
			}
			ss << "endPolyVect" << endl;
		}

		/*        vector<vector<Polygon> > unionPolyVects = instance->unionGeometries;
				for (vector<vector<Polygon> >::iterator itr = unionPolyVects.begin(); itr != unionPolyVects.end(); itr++) {
				vector<Polygon> polys = itr.operator *();
				for (vector<Polygon > ::iterator iter = polys.begin(); iter != polys.end(); iter++) {
				ss << bg::wkt<Polygon>(iter.operator *()) << endl;
				}
				ss << "endPolyVect" << endl;

				}*/
		ss << "endVectVect" << endl;


		ss.seekg(0, ss.end);
		int ssSize = ss.tellg();
		ss.seekg(0, ss.beg);

		char* buffer = new char[ssSize];
		ss.read(buffer, ssSize);

		//lock so only one thread accesses the file at one time
#ifdef _OPENMP

		omp_set_lock(&lock);
#endif

		//test to see file is open and attempt to open if not
		for (int i = 0; i < 3 && !this->fileIOStream->is_open(); i++) {
			this->fileIOStream->open(this->file.c_str(), ios::out | ios::in | ios::trunc | ios::binary);
		}

		if (!this->fileIOStream->is_open()) {
			throw "Failed to open!";
		}

		//seek to the end of the file so we can add to it
		this->fileIOStream->seekp(0, fileIOStream->end);

		//get the location and add our pattern instance object and it's size label first
		long currInstLoc = this->fileIOStream->tellp();
		string objSize = boost::lexical_cast<string>(ssSize);
		objSize.append("\n");
		this->fileIOStream->write(objSize.c_str(), objSize.size());
		this->fileIOStream->write(buffer, ssSize);


		//release the lock
#ifdef _OPENMP

		omp_unset_lock(&lock);
#endif


		//cleanup
		delete[] buffer;
		//delete instance;

		//return the location of the
		return currInstLoc;
	}

	/************************************************************************/
	/*getPatternInstanceFromFile    :Method for returning a Pattern Instance from a file on disk

	@param loc						:The location in the file that we start reading for the object of interest
	@return							:returns the object found at the location passed in*/
	/************************************************************************/
	PatternInstance* getPatternInstanceFromFile(long loc) {

		std::pair<long, PatternInstance*> tmp = this->getPatternInstancePairFromFile(loc);

		return tmp.second;
	}

	/************************************************************************/
	/*getPatternInstancePairFromFile    :Method for returning an PatternInstance from a file on disk.

	@param loc				 :The location in the file that we start reading for the object of interest
	@return					 :returns the index of the next object(which is not guaranteed to not be the end of the file
	so be careful using this value on subsequent calls) and the object found at the location passed in*/
	/************************************************************************/
	virtual std::pair<long, PatternInstance*>getPatternInstancePairFromFile(long loc) {

		//lock so only one thread is accessing the file at one time
#ifdef _OPENMP
		omp_set_lock(&lock);
#endif
		//test to see file is open and attempt to open if not
		for (int i = 0; i < 3 && !this->fileIOStream->is_open(); i++) {
			this->fileIOStream->open(this->file.c_str(), ios::out | ios::in | ios::trunc | ios::binary);
		}

		if (!this->fileIOStream->is_open()) {
			throw "Failed to open!";
		}

		//find direction and size of offset from current read location in stream
		long seekLoc = loc - this->fileIOStream->tellg();
		//cout << "Loc: " << loc << endl;
		//cout << "Loc seek:" << seekLoc << endl;
		//go there
		this->fileIOStream->seekg(seekLoc, fileIOStream->cur);

		//get the first line of chars as this stores the size of the rest of the object
		char* sizeVarArr = new char[10];
		this->fileIOStream->getline(sizeVarArr, 10, '\n');
		string sizeVarString(sizeVarArr);
		int sizeVar = atoi(sizeVarString.c_str());
		delete[] sizeVarArr;
		//cout << "Size string: " << sizeVarString << endl;
		//cout << "Size: " << sizeVar << endl;
		//get the rest of the object from the file
		char* buffer = new char[sizeVar];
		this->fileIOStream->read(buffer, sizeVar);

		//we want to know where we currently are
		seekLoc = this->fileIOStream->tellg();
		//release the lock
#ifdef _OPENMP

		omp_unset_lock(&lock);
#endif

		//now process the rest of the object (here we can be outside of
		membuf sbuf(buffer, buffer + sizeVar);
		std::istream buffReader(&sbuf);

		string val;
		getline(buffReader, val);
		int patInstID = atoi(val.c_str());

		getline(buffReader, val);
		long startTime = atol(val.c_str());

		getline(buffReader, val);
		long endTime = atol(val.c_str());

		getline(buffReader, val);
		double intersectonVol = stod(val.c_str());

		getline(buffReader, val);
		double unionVol = stod(val.c_str());

		getline(buffReader, val);
		double maxVol = stod(val.c_str());

		//get the participating instance IDs
		list<int> partInstIDs;
		while (getline(buffReader, val)) {
			if (val == "endPartIDs")
				break;
			partInstIDs.push_back(atoi(val.c_str()));
		}

		//get the multi-polygon of the intersections
		vector<vector<Polygon> > intersectPolysVect;
		while (getline(buffReader, val)) {
			if (val == "endVectVect")
				break;
			vector<Polygon> polys;
			do {
				if (val == "endPolyVect")
					break;
				Polygon polygon;
				bg::read_wkt(val, polygon);
				bg::correct(polygon);
				polys.push_back(polygon);

			} while (getline(buffReader, val));
			intersectPolysVect.push_back(polys);
		}

		//get the multi-polygon of the unions
		vector<vector<Polygon> > unionPolysVect;
		while (getline(buffReader, val)) {
			if (val == "endVectVect")
				break;
			vector<Polygon> polys;
			do {
				if (val == "endPolyVect")
					break;
				Polygon polygon;
				bg::read_wkt(val, polygon);
				bg::correct(polygon);
				polys.push_back(polygon);

			} while (getline(buffReader, val));
			unionPolysVect.push_back(polys);
		}

		//book keeping updates
		this->randReads++;
		this->seqReads += (sizeVar / this->bytesPerRead + 1);

		//create a new instance
		long location = seekLoc;
		PatternInstance* retVal = new PatternInstance(intersectPolysVect, unionPolysVect, patInstID, startTime, endTime, intersectonVol, unionVol, maxVol);

		std::pair<long, PatternInstance*> tmp = std::make_pair(location, retVal);
		delete[] buffer;


		return tmp;
	}


	/************************************************************************/
	/*getNumSequentialReads  :Returns the number of sequential bytes read divided by the disk page size
		that was passed into the constructor of this object*/
	/************************************************************************/
	int getNumSequentialReads() {
		return this->seqReads;
	}

	/************************************************************************/
	/*getNumRandomReads  :Returns the number of times we called the load method as each time we do
		it can be considered as a random access to the disk*/
	/************************************************************************/
	int getNumRamdomReads() {
		return this->randReads;
	}

	/************************************************************************/
	/* begin	:Method for returning an iterator to the beginning of the file
	return		:Returns the iterator that points to the beginning of the file */
	/************************************************************************/
	Iterator begin() {
		return Iterator(this, 0);
	}

	/************************************************************************/
	/* end	:Method for returning an iterator that points to the end of the file
	return	:Returns an iterator pointing to the end of the file*/
	/************************************************************************/
	Iterator end() {
		//lock so only one thread accesses the file at one time
#ifdef _OPENMP
		omp_set_lock(&lock);
#endif
		//test to see file is open and attempt to open if not
		for (int i = 0; i < 3 && !this->fileIOStream->is_open(); i++) {
			this->fileIOStream->open(this->file.c_str(), ios::out | ios::in | ios::trunc | ios::binary);
		}

		if (!this->fileIOStream->is_open()) {
			throw "Failed to open!";
		}

		//seek to the end of the file so we can add to it
		this->fileIOStream->seekp(0, fileIOStream->end);

		//get the location and add our instance object
		long currEndLoc = this->fileIOStream->tellp();

		//release the lock
#ifdef _OPENMP

		omp_unset_lock(&lock);
#endif

		return Iterator(this, currEndLoc);

	}
};
#endif /* IINSTFILEACCESS_HPP_ */
