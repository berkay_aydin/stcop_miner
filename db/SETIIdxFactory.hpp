/*
* SETIIdxFactory.hpp
*
*  Created on: May 28, 2014
* Author: Dustin Kempton
*/

#ifndef SETIINDEXFACTORY_HPP_
#define SETIINDEXFACTORY_HPP_

#include "db/interface/IIndexFactory.hpp"
#include "db/index/SETIInstIdx.hpp"
#include "db/index/SETIPatInstIndex.hpp"
#include "db/io/PatInstFileAccess.hpp"
#include "db/io/InstFileAccess.hpp"

using namespace std;

class SETIIdxFactory : public IIndexFactory {
    string filesLoc;
    int bytesPerRead;
public:

    SETIIdxFactory(string storageFilesLocation, int bytesPerRead) {
        this->filesLoc = storageFilesLocation;
        this->bytesPerRead = bytesPerRead;
    }

    IInstIndex* getInstIdx(string tableName) {
        //TODO remove those stupid things
        //TODO start
        double dimX = 100000;
        double dimY = 100000;
        int xCell = 4;
        int yCell = 4;
        //TODO end

        IInstFileAccess* fa = new InstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IInstIndex* idx = new SETIInstIndex(fa, dimX, dimY, xCell, yCell);

        return idx;
    }

    IPatInstIndex* getPatInstIdx(string tableName) {
    	//TODO remove those stupid things
    	//TODO start
    	double dimX = 100000;
    	double dimY = 100000;
    	int xCell = 4;
    	int yCell = 4;
    	//TODO end

    	IPatInstFileAccess* fa = new PatInstFileAccess(this->filesLoc, tableName, this->bytesPerRead);
        IPatInstIndex* idx = new SETIPatInstIndex(fa, dimX, dimY, xCell, yCell);

        return idx;
    }

};
#endif /* SETIINDEXFACTORY_HPP_ */
