/*
 * DBAccess.hpp
 *
 *  Created on: May 28, 2014
 * Author: Dustin Kempton
 */

#ifndef DBACCESS_HPP_
#define DBACCESS_HPP_


#include <list>
#include <utility>
#include <boost/bind.hpp>
#include "Instance.h"
#include "PatternInstance.h"
#include "db/interface/IIndexFactory.hpp"
#include "db/index/interface/IInstIndex.hpp"
#include "db/index/interface/IPatInstIndex.hpp"
#include "db/interface/IDBAccess.hpp"

#include <boost/unordered_map.hpp>

using namespace std;

class DBAccess : public IDBAccess {
private:
    boost::unordered_map<string, IInstIndex*>* instIdxMap;
    boost::unordered_map<string, IPatInstIndex*>* patInstIdxMap;
    IIndexFactory* factory;
public:

    DBAccess(IIndexFactory* factory) {
        this->factory = factory;
        this->instIdxMap = new boost::unordered_map<string, IInstIndex*>();
        this->patInstIdxMap = new boost::unordered_map<string, IPatInstIndex*>();
    }

    ~DBAccess() {
		//need to delete all the index items/table indexes
		for (boost::unordered_map<string, IInstIndex*>::iterator itr = this->instIdxMap->begin(); itr != this->instIdxMap->end(); itr++){
			pair<string, IInstIndex*> itm = itr.operator*();
			IInstIndex* idx = itm.second;
			cout << "deleting idx" << endl;
			delete idx;
		}
        delete this->instIdxMap;

		for (boost::unordered_map<string, IPatInstIndex*>::iterator itr = this->patInstIdxMap->begin(); itr != this->patInstIdxMap->end(); itr++){
			pair<string, IPatInstIndex*> itm = itr.operator*();
			delete itm.second;
		}
        delete this->patInstIdxMap;
    }

    JoinTableResults<Instance, IInstIndex>* spatiotemporalJoin(string table1, string table2) {
        boost::unordered_map<string, IInstIndex*>::iterator iter = this->instIdxMap->find(table1);

        //if table1 exists
        if (iter != this->instIdxMap->end()) {
            pair<string, IInstIndex*> idxPair = iter.operator*();
            IInstIndex* idx = idxPair.second;

            boost::function< vector<Instance*>* (Instance*, string) > getMethod;
            getMethod = boost::bind(&DBAccess::getJoinInstance, this, _1, _2);
            JoinTableResults < Instance, IInstIndex >* joinResults = new JoinTableResults< Instance, IInstIndex >(idx, getMethod, table2);
            return joinResults;
		}
		else {
			cout << "Instance spatiotemporalJoin() called with " <<table1<< " and "<< table2<<endl;
            throw logic_error("Table does not exist!");
        }

    }

    JoinTableResults<PatternInstance, IPatInstIndex>* p_spatiotemporalJoin(string table1, string table2) {
        boost::unordered_map<string, IPatInstIndex*>::iterator iter = this->patInstIdxMap->find(table1);

        //if table1 exists
        if (iter != this->patInstIdxMap->end()) {
            pair<string, IPatInstIndex*> idxPair = iter.operator*();
            IPatInstIndex* idx = idxPair.second;
            boost::function< vector<PatternInstance*>* (PatternInstance*, string) > getMethod;
            getMethod = boost::bind(&DBAccess::getJoinPatInst, this, _1, _2);
            JoinTableResults< PatternInstance, IPatInstIndex >* joinResults = new JoinTableResults< PatternInstance, IPatInstIndex >(idx, getMethod, table2);
            return joinResults;
		}
		else {
			cout << "PatInstance p_spatiotemporalJoin() called with " <<table1<< " and "<< table2<<endl;
            throw logic_error("Table does not exist!");
        }

    }


    vector<Instance* >* searchST_Intersection(Instance *i, string table) {
        boost::unordered_map<string, IInstIndex*>::iterator iter = this->instIdxMap->find(table);

        if (iter != this->instIdxMap->end()) {
            //then we found the table and we can search
            IInstIndex* instanceIndex = this->instIdxMap->at(table);
            vector<Instance* >* queryResultList = instanceIndex->searchST_Intersection(i);
            return queryResultList;

		}
		else {
			cout << "Instance searchST_Intersection() called with " <<i->getInstanceId()<< " and "<< table <<endl;
            throw logic_error("Table does not exist!");
        }

    }

    vector<PatternInstance* >* searchST_Intersection(PatternInstance *pinst, string table) {
        boost::unordered_map<string, IPatInstIndex*>::iterator iter = this->patInstIdxMap->find(table);

        if (iter != this->patInstIdxMap->end()) {
            //then we found the table and we can search
            IPatInstIndex* pat_instanceIndex = this->patInstIdxMap->at(table);
            vector<PatternInstance* >* queryResultList = pat_instanceIndex->searchST_Intersection(pinst);
            return queryResultList;

		}
		else {
			cout << "PatInst searchST_Intersection() called with " <<pinst->getPatternInstanceId()<< " and "<< table <<endl;
            throw logic_error("Table does not exist!");
        }
    }

    void insert(Instance* inst, string tableName) {
        boost::unordered_map<string, IInstIndex*>::iterator iter = this->instIdxMap->find(tableName);
        //std::cout << inst->getInstanceId() << " :::: " << tableName << std::endl;
        if (iter == this->instIdxMap->end()) {
        	//inst->printInstance();
        	IInstIndex* idx = this->factory->getInstIdx(tableName);

            idx->insert(inst);
            pair<string, IInstIndex*> toBeInserted;
            toBeInserted = make_pair(tableName, idx);
            this->instIdxMap->insert(toBeInserted);
		}
		else {
            pair<string, IInstIndex*> tmpPair = iter.operator *();
            IInstIndex* idx = tmpPair.second;
            idx->insert(inst);
        }
    }

    void insert(PatternInstance* patInst, string tableName) {
        boost::unordered_map<string, IPatInstIndex*>::iterator iter = this->patInstIdxMap->find(tableName);

        if (iter == this->patInstIdxMap->end()) { // it couldn't find it
            IPatInstIndex* idx = this->factory->getPatInstIdx(tableName);
            idx->insert(patInst);
            pair<string, IPatInstIndex*> toBeInserted;
            toBeInserted = std::make_pair(tableName, idx);
            this->patInstIdxMap->insert(toBeInserted);
		}
		else {
            std::pair<string, IPatInstIndex*> tmpPair = iter.operator *();
            IPatInstIndex* idx = tmpPair.second;
            //cout << "uni geom size in db access:" << patInst->unionGeometries.size() << endl;
            idx->insert(patInst);
        }
    }


    vector<Instance*>* getJoinInstance(Instance* inst, string tableName) {
        boost::unordered_map<string, IInstIndex*>::iterator iter = this->instIdxMap->find(tableName);

        //if the table exists in the map of indexes
        if (iter != this->instIdxMap->end()) {
            pair< string, IInstIndex* > idxPair = iter.operator*();
            IInstIndex* idx = idxPair.second;
            vector<Instance*>* results = idx->searchST_Intersection(inst);
            return results;
		}
		else {
            //the table does not exist
            return new vector<Instance*>();
        }
    }

    vector<PatternInstance*>* getJoinPatInst(PatternInstance* patInst, string tableName) {
        boost::unordered_map<string, IPatInstIndex*>::iterator iter = this->patInstIdxMap->find(tableName);

        //if the table exists in the map of indexes
        if (iter != this->patInstIdxMap->end()) {
            std::pair< string, IPatInstIndex* > tmpPair = iter.operator *();
            IPatInstIndex* idx = tmpPair.second;
            vector<PatternInstance*>* results = idx->searchST_Intersection(patInst);
            return results;
		}
		else {
            //the table does not exist
            return new vector<PatternInstance*>();
        }
    }

};
#endif /* DBACCESS_HPP_ */
