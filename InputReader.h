/*
 * InputReader.h
 *
 *  Created on: Jun 7, 2014
 *      Author: Vijay Akkineni
 */

#ifndef INPUTREADER_H_
#define INPUTREADER_H_

#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <vector>
#include <iostream>
#include "Reader.h"
#include "Feature.h"
#include "db/interface/IDBAccess.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace boost::iostreams;

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

class InputReader : public Reader  {

public:
    InputReader(string dir,IDBAccess* _idb_access);

    vector<Feature> readFeatures();
    void readInstances(vector<Feature> features) ;
    vector<int> getFeatureCounts() ;

private:
    string datasetDirectory;
    IDBAccess* idb_access;
    vector<int> featureCounts;
    int featureIDCounter;
};

#endif /* INPUTREADER_H_ */
