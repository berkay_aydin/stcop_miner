/*
 * Feature.h
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#ifndef FEATURE_H_
#define FEATURE_H_

#include <string>
#include <iostream>

using namespace std;

class Feature {
public:
    int featureId;
    string featureName;

    void printFeature() {
        cout << featureId << featureName << endl;
    }
};

#endif /* FEATURE_H_ */
