/*
 * PatternInstance.cpp
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#include "PatternInstance.h"
#include <iostream>
#include <boost/foreach.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

using namespace std;
using namespace boost::geometry;
namespace bt = boost::posix_time;

int globalPatInstanceId = 0;

PatternInstance::PatternInstance(Instance* i1, Instance* i2) {
	globalPatInstanceId++;
	pi_id = globalPatInstanceId;

	//check if times are disjoint
	long s1 = i1->getStartTime();
	long e1 = i1->getEndTime();
	long s2 = i2->getStartTime();
	long e2 = i2->getEndTime();
	bt::time_period i1Period(bt::from_time_t(i1->getStartTime()), bt::from_time_t(i2->getEndTime()));
	bt::time_period i2Period(bt::from_time_t(i2->getStartTime()), bt::from_time_t(i2->getEndTime()));

	bool isTimeDisjoint = !i1Period.intersects(i2Period);//( (s1 < s2) && (e1 < s2) ) || ( (s1 > s2) && (s1 > e2) ) ;

	if(isTimeDisjoint){
		//we create a dummy pattern instance, that will not return anything at all
		pi_id = -1;

		//so the calculations will show -1 for omax or jaccard
		intersectionVolume = -1;
		unionVolume = 1;
		maxVolume = 1;

	}else {
		//we do regular stuff and check
		if (i1->getStartTime() < i2->getStartTime()) {
			startTime = i1->getStartTime();
		} else {
			startTime = i2->getStartTime();
		}

		if (i1->getEndTime() < i2->getEndTime()) {
			endTime = i2->getEndTime();
		} else {
			endTime = i1->getEndTime();
		}

		participatingInstances.push_back(i1->getInstanceId());
		participatingInstances.push_back(i2->getInstanceId());


		unionVolume = 0;
		intersectionVolume = 0;
		calculateGeometries(i1, i2);
	}

	delete i1;
	delete i2;
}

PatternInstance::PatternInstance(PatternInstance* patI1, PatternInstance* patI2) {

	globalPatInstanceId++;
	pi_id = globalPatInstanceId;

	//check if times are disjoint
	long s1 = patI1->getStartTime();
	long e1 = patI1->getEndTime();
	long s2 = patI2->getStartTime();
	long e2 = patI2->getEndTime();

	bool isTimeDisjoint = ( (s1 < s2) && (e1 < s2) ) || ( (s1 > s2) && (s1 > e2) ) ;

	if(isTimeDisjoint){
		//we create a dummy pattern instance, that will not return anything at all
		pi_id = -1;

		//so the calculations will show -1 for omax or jaccard
		intersectionVolume = -1;
		unionVolume = 1;
		maxVolume = 1;

	}else {
		if (patI1->getStartTime() < patI2->getStartTime()) {
			startTime = patI1->getStartTime();
		} else {
			startTime = patI2->getStartTime();
		}

		if (patI1->getEndTime() < patI2->getEndTime()) {
			endTime = patI2->getEndTime();
		} else {
			endTime = patI1->getEndTime();
		}

		participatingInstances.insert(participatingInstances.end(), patI1->participatingInstances.begin(), patI1->participatingInstances.end());
		participatingInstances.insert(participatingInstances.end(), patI2->participatingInstances.begin(), patI2->participatingInstances.end());


		unionVolume = 0;
		intersectionVolume = 0;
		calculateGeometries(patI1, patI2);

		delete patI1;
		delete patI2;
	}



}

PatternInstance::PatternInstance(vector<vector<Polygon>> intersectionPolys, vector<vector<Polygon>> unionPolys,
                                 int patID, long startTime, long endTime, double intersectVol, double unionVol, double maxVol) {
    this->intersectionGeometries = intersectionPolys;
    this->unionGeometries = unionPolys;
    this->pi_id = patID;
    this->startTime = startTime;
    this->endTime = endTime;
    this->intersectionVolume = intersectVol;
    this->unionVolume = unionVol;
    this->maxVolume = maxVol;
}


void PatternInstance::calculateGeometries(Instance* i1, Instance* i2) {

    Polygon emptyPolygon;
    read_wkt("POLYGON EMPTY", emptyPolygon);
    correct(emptyPolygon);
    for (long i = startTime; i <= endTime; i++) { //for each timestamp from pat. instance's start to end time

        vector<Polygon> uni_i;
        vector<Polygon> int_i;
        Multipolygon mp_int_i;
        int k = 0;
        if (i1->getStartTime() <= i && i1->getEndTime() >= i) { // if we are in time interval of i1
            if (i2->getStartTime() <= i && i2->getEndTime() >= i) { //if we are in time interval of i2

                intersection(i1->getPolygon(i), i2->getPolygon(i), mp_int_i);
                union_(i1->getPolygon(i), i2->getPolygon(i), uni_i);

                //cout << "What is i: " << i << "\t What is int polygon: " << dsv(mp_int_i) << endl;
                if(mp_int_i.size() != 0){
                	int_i.insert(int_i.begin(), mp_int_i.begin(), mp_int_i.end());
                	mp_int_i.clear();
                } else{
                	int_i.push_back(emptyPolygon);
                }

                BOOST_FOREACH(Polygon const& p, int_i) {
                    intersectionVolume += area(p);
                    k++;
                }

            } else { //we only have i1 here
                uni_i.push_back(i1->getPolygon(i));
                int_i.push_back(emptyPolygon);

            }
        } else { // we only have i2 here
            int_i.push_back(emptyPolygon);
            uni_i.push_back(i2->getPolygon(i));
        }
        k = 0;
        BOOST_FOREACH(Polygon const& p, uni_i) {
            unionVolume += area(p);
            k++;
        }
        unionGeometries.push_back(uni_i);
        intersectionGeometries.push_back(int_i);
    }

    double v1 = 0.0, v2 = 0.0;
    for (long i = i1->getStartTime(); i <= i1->getEndTime(); i++) {
        v1 += area(i1->getPolygon(i));
    }
    for (long i = i2->getStartTime(); i <= i2->getEndTime(); i++) {
        v2 += area(i2->getPolygon(i));
    }
    if (v1 > v2) {
        maxVolume = v1;
    } else {
        maxVolume = v2;
    }

}

void PatternInstance::calculateGeometries(PatternInstance* i1, PatternInstance* i2) {

    Polygon emptyPolygon;
    read_wkt("POLYGON EMPTY", emptyPolygon);
    correct(emptyPolygon);
    //cout << "calculating geometries for timestamp::";
    for (long i = startTime; i <= endTime; i++) { //for each timestamp from pat. instance's start to end time
        vector<Polygon> uni_i;
        vector<Polygon> int_i;
        Multipolygon temp_uni1;
        Multipolygon temp_uni2;
        Multipolygon temp_int1;
        Multipolygon temp_int2;
        unsigned int ii = (unsigned int)(i - startTime);
        int k = 0;
        if (i1->getStartTime() <= i && i1->getEndTime() >= i) { // if we are in time interval of i1
            if (i2->getStartTime() <= i && i2->getEndTime() >= i) { //if we are in time interval of i2

                temp_int1.assign(i1->intersectionGeometries.at(ii).begin(), i1->intersectionGeometries.at(ii).end());
                temp_int2.assign(i2->intersectionGeometries.at(ii).begin(), i2->intersectionGeometries.at(ii).end());

                temp_uni1.assign(i1->unionGeometries.at(ii).begin(), i1->unionGeometries.at(ii).end());
                temp_uni2.assign(i2->unionGeometries.at(ii).begin(), i2->unionGeometries.at(ii).end());

                try{
                	intersection(temp_int1, temp_int2, int_i);
                	union_(temp_uni1, temp_uni2, uni_i);
                }catch(std::exception const&  ex)
                {
                	pi_id = -1;
                	//so the calculations will show -1 for omax or jaccard
                	intersectionVolume = -1;
                	unionVolume = 1;
                	maxVolume = 1;
                	break;
                    printf("Intersection or union problem here!!!. %s", ex.what());
                }

                if(int_i.size() == 0){ // handling empty intersection case
                	int_i.push_back(emptyPolygon);
                }

                BOOST_FOREACH(Polygon const& p, int_i) {
                    intersectionVolume += area(p);
                    k++;
                }

            } else { //we only have i1 here
                uni_i.insert(uni_i.begin(), i1->unionGeometries.at(ii).begin(), i1->unionGeometries.at(ii).end());
                int_i.push_back(emptyPolygon);

            }
        } else { // we only have i2 here
            int_i.push_back(emptyPolygon);
            uni_i.insert(uni_i.begin(), i2->unionGeometries.at(ii).begin(), i2->unionGeometries.at(ii).end());
        }
        k = 0;
        BOOST_FOREACH(Polygon const& p, uni_i) {
            unionVolume += area(p);
            k++;
        }
        unionGeometries.push_back(uni_i);
        intersectionGeometries.push_back(int_i);
    }

    if (i1->getMaxVolume() > i2->getMaxVolume()) {
        maxVolume = i1->getMaxVolume();
    } else {
        maxVolume = i2->getMaxVolume();
    }
    /** Deleted part


    	for(long i = startTime; i <= endTime; i++){ //for each timestamp from pat. instance's start to end time



    	if(instances.size() <= 0){
    	cout << "PatternInstance::calculateGeometries there must be at least 1 instance given" << endl;
    	return;
    	}else{ // we have more than one instances

    	list<Instance>::iterator it = instances.begin();

    	vector<Polygon> tempUni;	//union of geometries at 1 single timestamp - i
    	vector<Polygon> tempInt;	//intersection of geometries at 1 single timestamp - i

    	Multipolygon mpUni;
    	Multipolygon mpInt;

    	union_(it->getPolygon(i), it->getPolygon(i), mpUni);
    	union_(it->getPolygon(i), it->getPolygon(i), mpInt );
    	it++;

    	while(it != instances.end()){
    	// clear the temporary polygon vector
    	tempUni.clear();
    	tempInt.clear();

    	//do the union and intersection on multipolygons and current instance's polygon
    	union_(mpUni, it->getPolygon(i), tempUni);
    	//cout<< "Size of vector: " << tempUni.size() << endl;
    	intersection(mpInt, it->getPolygon(i), tempInt);

    	//clear the multipolygons for eradicating the replication
    	mpUni.clear();
    	mpInt.clear();
    	//cout<< "MultiPolygon Uni After clearing:" << dsv(mpUni) << endl;
    	//cout<< "MultiPolygon Int After clearing:" << dsv(mpInt) << endl;

    	//assign the current resulting value of union and intersection
    	mpUni.assign(tempUni.begin(), tempUni.end());
    	mpInt.assign(tempInt.begin(), tempInt.end());
    	//cout<< "MultiPolygon Uni After assigning:" << dsv(mpUni) << endl;
    	//cout<< "MultiPolygon Int After assigning:" << dsv(mpInt) << endl;

    	it++;
    	}


    	double unionV = 0.0;
    	double intersectionV = 0.0;

    	int i = 0;
    	BOOST_FOREACH(Polygon const& p, tempUni)
    	{
    	unionV += boost::geometry::area(p);
    	i++;
    	}
    	i = 0;
    	BOOST_FOREACH(Polygon const& p, tempInt)
    	{
    	intersectionV += boost::geometry::area(p);
    	i++;
    	}

    	unionGeometries.push_back(tempUni);				//push to pattern's union list
    	intersectionGeometries.push_back(tempInt);		//push to pattern's intersection list
    	unionVolume = unionVolume + unionV;
    	intersectionVolume = intersectionVolume + intersectionV;
    	}
    	}*/
}

double PatternInstance::getUnionVolume() {
    return unionVolume;
}

double PatternInstance::getIntersectionVolume() {
    return intersectionVolume;
}

double PatternInstance::getMaxVolume() {
    return maxVolume;
}
long PatternInstance::getStartTime() {
    return startTime;
}
long PatternInstance::getEndTime() {
    return endTime;
}
int PatternInstance::getPatternInstanceId() {
    return pi_id;
}

void PatternInstance::printPatternInstance() {

    cout << "Pattern Instance Id:" << pi_id << " -- Starts:" << startTime << " Ends:" << endTime << endl;

    cout << "Union Volume: " << getUnionVolume() << "\tIntersection Volume:" << getIntersectionVolume() << "\tMax Volume: " << maxVolume << endl;

    cout << "Union Geometries-- " << endl;

    for (unsigned int i = 0; i < unionGeometries.size(); i++) {
        cout << "Time: " << (startTime + i) << "\t Representation " ;
        for (size_t j = 0; j < unionGeometries.at(i).size(); j++) {

            cout << wkt(unionGeometries.at(i).at(j)) << endl;

        }
    }
    cout << "Intersection Geometries-- " << endl;

    for (unsigned int i = 0; i < intersectionGeometries.size(); i++) {
        cout << "Time: " << (startTime + i) << "\tRepresentation ";

        for (size_t j = 0; j < intersectionGeometries.at(i).size(); j++) {
            cout << wkt(intersectionGeometries.at(i).at(j)) << endl;
        }
    }

}

