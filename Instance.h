/*
 * Instance.h
 *
 *  Created on: May 19, 2014
 *      Author: berkay
 */

#ifndef INSTANCE_H_
#define INSTANCE_H_


#include <list>
#include <vector>
#include <string>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

typedef boost::geometry::model::point<int, 2, boost::geometry::cs::cartesian> point;
typedef boost::geometry::model::polygon<point, false, false > Polygon;

typedef boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian> point2d;
typedef boost::geometry::model::polygon<point2d, false, false > Polygon2d;


using namespace std;
using namespace boost::geometry;

class Instance {
private:
    int instanceId;
    int featureId;
    long startTime;
    long endTime;
    double volume;
    vector<Polygon> geometries;
public:
    Instance() {
        this->instanceId = -1;
        this->featureId = -1;
        this->startTime = -1;
        this->endTime = -1;
        this->volume = -1;
    }

    Instance(int instId, int fId, long time_s, long time_e, list<string> wkt) {
        this->instanceId = instId;
        this->featureId = fId;
        this->startTime = time_s;
        this->endTime = time_e;
        volume = 0;

        if ((long)wkt.size() != (1 + endTime - startTime)) {
            cout << "The timestamps do not overlap, there is a problem" << wkt.size() << " vs " << (1 + endTime - startTime) << endl;
        } else {
            Polygon2d poly2d;
            Polygon polygon;

            for (list<string>::iterator it = wkt.begin(); it != wkt.end(); it++) {

                try {
                    read_wkt(*it, poly2d);
                } catch( std::exception const&  ex ) {
                    instId = -1;
                    break;
                }

                correct(poly2d);

                for(unsigned int i = 0; i < poly2d.outer().size(); i++) {
                    float point_x = poly2d.outer().at(i).get<0>();
                    float point_y = poly2d.outer().at(i).get<1>();
                    point point_i( (int)point_x, (int)point_y);
                    polygon.outer().push_back(point_i);
                }
                correct(polygon);
                //cout << "I am trying the integer polygons here:" << dsv(polygon) << std::endl;
                //int stopper = 0;
                //cin >> stopper;

                //cout << area(polygon) << endl;
                volume += area(polygon);
                //cout << volume << endl;
                geometries.push_back(polygon);
                polygon.clear();
                poly2d.clear();

            }
        }
        //volume = calculateVolume();
    }

    Instance(int instId, int fId, long time_s,long time_e,vector<Polygon> _geometries,double volume) {
        this->instanceId = instId;
        this->featureId = fId;
        this->startTime = time_s;
        this->endTime = time_e;
        this->geometries = _geometries;
        this->volume=volume;
    }


    Instance(int instId, int fId, long time_s,long time_e) {
        this->instanceId = instId;
        this->featureId = fId;
        this->startTime = time_s;
        this->endTime = time_e;
        this->volume=0;
    }




    int getInstanceId() {
        if (instanceId < 0) {
            cout << "This instance is not valid" << endl;
            return -1;
        } else {
            return instanceId;
        }
    }

    int getFeatureId() {
        if (featureId < 0) {
            cout << "This instance's feature type is not valid" << endl;
            return -1;
        } else {
            return featureId;
        }
    }

    long getStartTime() {
        if (startTime < 0) {
            cout << "Start timestamp is not correct for Instance: "<<instanceId  <<
            " Feature: " << featureId << endl;
        }
        return this->startTime;
    }

    long getEndTime() {
        if (endTime < 0) {
            cout << "End timestamp is not correct" << endl;
        }
        return this->endTime;
    }

    double getVolume() {
        return volume;
    }

    Polygon getPolygon(long time) {

        if (time >= startTime && time <= endTime) {
            unsigned int timeIndex = (unsigned int)(time - startTime);
            if(timeIndex == geometries.size()) {
                timeIndex--;
            }
            Polygon p = geometries.at(timeIndex);
            correct(p);
            return p;
        } else {
            //cout << "We have a problem here" << endl;
            Polygon empty;
            read_wkt("POLYGON EMPTY", empty);
            correct(empty);
            return empty;
        }
    }


    vector<Polygon> getPolygons() {
        return geometries;
    }



    void printInstance() {

        cout << "Instance Id:" << getInstanceId() << " :: Feature Id: " << getFeatureId()
        << " Total volume: " << getVolume() << " , Start Time:: "
        << getStartTime() << " End Time:: " << getEndTime()
        << " Polygons Size:: " << getPolygons().size()<<endl;

        for (long i = getStartTime(); i <= getEndTime(); i++) {
            cout << "Time: " << i << " Representation: " << dsv(getPolygon(i)) << endl;
        }
    }

private:
    double calculateVolume() {

        double vol = 0;
        for (vector<Polygon>::iterator it = geometries.begin(); it != geometries.end(); it++) {
            vol += area(*it);
        }
        return vol;

    }

};



#endif /* INSTANCE_H_ */
